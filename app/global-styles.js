import { injectGlobal } from 'styled-components';
import ChampLim from 'fonts/Champagne & Limousines.ttf';
import ChampBoldItalic from 'fonts/Champagne & Limousines Bold Italic.ttf';
import ChampItalic from 'fonts/Champagne & Limousines Italic.ttf';
import ChampBold from 'fonts/Champagne & Limousines Bold.ttf';

import BG_WHITEPRINT from 'images/bg-whiteprint.jpg';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  @font-face {
    font-family: 'ChampLim';
    src: url(${ChampLim});
  }

  @font-face {
    font-family: 'ChampBoldItalic';
    src: url(${ChampBoldItalic});
  }

  @font-face {
    font-family: 'ChampItalic';
    src: url(${ChampItalic});
  }

  @font-face {
    font-family: 'ChampBold';
    src: url(${ChampBold});
  }

  html,
  body {
    height: 100%;
    width: 100%;
    color: #464648;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #FFF;
    min-height: 100%;
    min-width: 100%;
  }

  .content-wrapper {
    padding-top: 65px;

    &.with-bg {
      min-height: 100vh;
      background: url(${BG_WHITEPRINT});
    }
  }

  .test-border {
    border: 1px solid;
  }

  .bordered-link {
    font-size: 16px;
    color: #757575 !important;
    text-decoration: none;
    border-bottom: 1px solid #2488E6;
    padding-bottom: 3px;
  }

  .cursor-pointer {
    cursor: pointer;
  }

  .font-champ {
    font-family: ChampLim;
  }

  .font-champ-bolditalic {
    font-family: ChampBoldItalic;
  }

  .font-champ-italic {
    font-family: ChampItalic;
  }

  .font-champ-bold {
    font-family: ChampBold;
  }

  .placeholder-dark {
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
      color: rgb(0,0,0,0.65);
      opacity: 0.6; /* Firefox */
    }

    :-ms-input-placeholder { /* Internet Explorer 10-11 */
      color: rgb(0,0,0,0.65);
    }

    ::-ms-input-placeholder { /* Microsoft Edge */
      color: rgb(0,0,0,0.65);
    }
  }

  .text-center {
    text-align: center;
  }

  .text-right {
    text-align: right;
  }

  .text-white {
    color: #FFF;
  }

  .pull-right {
    float: right;
  }

  .pull-left {
    float: left;
  }

  .clear {
    clear: both;
  }

  .no-padding {
    padding: 0px !important;
  }

  .no-margin {
    margin: 0px !important;
  }

  .social-btn {
    width: 100%;
    margin: 4px 0px;
    color: #FFF;

    &.facebook-btn-color {
      border: 1px solid #3D5894 !important;
      background-color: #3D5894 !important;

      &:hover {
        border: 1px solid #5470b0 !important;
        background-color: #5470b0 !important;
      }

      &:active {
        border: 1px solid #344d86 !important;
        background-color: #344d86 !important;
      }
    }

    &.twitter-btn-color {
      border: 1px solid #2cc6f5 !important;
      background-color: #2cc6f5 !important;

      &:hover {
        border: 1px solid #40d3ff !important;
        background-color: #40d3ff !important;
      }

      &:active {
        border: 1px solid #38b9df !important;
        background-color: #38b9df !important;
      }
    }

    &.google-plus-btn-color {
      border: 1px solid #DD4D45 !important;
      background-color: #DD4D45 !important;

      &:hover {
        border: 1px solid #f4534a !important;
        background-color: #f4534a !important;
      }

      &:active {
        border: 1px solid #c7453e !important;
        background-color: #c7453e !important;
      }
    }

    &.create-account {
      background-color: #484849;
      border-color: #484849;
      color: #fff;
      font-size: 13px;
      text-align: center;
      padding-left: 8px;

      &:hover {
        background-color: #313133;
        border-color: #313133;
        color: #fff;
      }

      &:active, &:focus {
        background-color: #535357;
        border-color: #535357;
        color: #fff;
      }
    }

    .social-icon {
      margin: 4px 0px 0px 3px;
    }
  }

  .ant-form-explain {
    text-align: right;
  }

  .back-button {
    font-size: 16px;
    position: absolute;
    margin-top: 14px;
  }
`;
