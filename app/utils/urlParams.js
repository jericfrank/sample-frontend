import _ from 'lodash';

export default (queryString) => (
    _.chain(queryString)
        .replace('?', '')
        .split('&')
        .map(_.partial(_.split, _, '=', 2))
        .fromPairs()
        .value()
);
