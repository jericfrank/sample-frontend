import axios from 'axios';
import { handleJwtToken, expireJwtToken } from '../jwtToken';

describe('jwtToken', () => {
  const TOKEN = 'Token here';

  beforeEach(() => {
    class LocalStorageMock {
      constructor() {
        this.store = {};
      }

      clear() {
        this.store = {};
      }

      getItem(key) {
        return this.store[key] || null;
      }

      setItem(key, value) {
        this.store[key] = value.toString();
      }

      removeItem(key) {
        delete this.store[key];
      }
    }

    global.localStorage = new (LocalStorageMock)();
  });

  describe('handleJwtToken', () => {
    it('should store token to `localStorage` and `axios` config', () => {
      const setItemSpy = jest.spyOn(global.localStorage, 'setItem');
      handleJwtToken(TOKEN);
      expect(setItemSpy).toHaveBeenCalledTimes(1);
      expect(global.localStorage.getItem('jobportalToken'))
        .toEqual(TOKEN);
      expect(axios.defaults.headers.common.Authorization)
        .toEqual(`Bearer ${TOKEN}`);
      setItemSpy.mockReset();
      setItemSpy.mockRestore();
      expireJwtToken();
    });
  });

  describe('expireJwtToken', () => {
    it('should remove token and user to `localStorage` and `axios`', () => {
      handleJwtToken(TOKEN);
      const removeItemSpy = jest.spyOn(global.localStorage, 'removeItem');
      expireJwtToken();
      expect(removeItemSpy).toHaveBeenCalledTimes(2);
      expect(global.localStorage.getItem('jobportalToken')).toBeNull();
      expect(global.localStorage.getItem('jobportalUser')).toBeNull();
      expect(axios.defaults.headers.common.Authorization).toBeUndefined();
      removeItemSpy.mockReset();
      removeItemSpy.mockRestore();
    });
  });
});
