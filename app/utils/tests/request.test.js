import axios from 'axios';

import {
  getRequest,
  postRequest,
  putRequest,
  deleteRequest,
  setAuthorizationToken,
} from '../request';

jest.mock('axios');

describe('request', () => {
  let error400;

  beforeEach(() => {
    error400 = {
      response: {
        data: 'Error',
        status: 400,
        config: {
          url: '/api/authentications',
        },
      },
    };
  });

  afterEach(() => {
    axios.mockRestore();
  });

  describe('getRequest', () => {
    afterEach(() => {
      axios.get.mockReset();
      axios.get.mockRestore();
    });

    it('should get data successfully', () => {
      const data = [{ name: 'Bob' }];
      const promise = Promise.resolve({ data });
      axios.get.mockReturnValue(promise);
      expect(getRequest('/users')).resolves.toBe(data);
    });

    describe('unsuccessful request', () => {
      it('should return error `data`', () => {
        const promise = Promise.reject(error400);
        axios.get.mockReturnValue(promise);
        expect(getRequest('/users')).rejects.toBe(error400.response.data);
      });

      it('should return error `data`', () => {
        const error = {
          response: {
            data: 'Error',
            status: 401,
            config: {
              url: '/api/users',
            },
          },
        };

        const promise = Promise.reject(error);
        axios.get.mockReturnValue(promise);
        expect(getRequest('/users')).rejects.toBe(error.response.data);
      });
    });
  });

  describe('postRequest', () => {
    afterEach(() => {
      axios.post.mockReset();
      axios.post.mockRestore();
    });

    it('should save data successfully', () => {
      const data = { name: 'Bob' };
      const promise = Promise.resolve({ data });
      axios.post.mockReturnValue(promise);
      expect(postRequest('/users', data)).resolves.toBe(data);
    });

    it('should return error `data` if request is unsuccessfully', () => {
      const promise = Promise.reject(error400);
      axios.post.mockReturnValue(promise);
      expect(postRequest('/users', {})).rejects.toBe(error400.response.data);
    });
  });

  describe('putRequest', () => {
    afterEach(() => {
      axios.put.mockReset();
      axios.put.mockRestore();
    });

    it('should edit data successfully', () => {
      const data = { name: 'Bob' };
      const promise = Promise.resolve({ data });
      axios.put.mockReturnValue(promise);
      expect(putRequest('/users', data)).resolves.toBe(data);
    });

    it('should return error `data` if request is unsuccessfully', () => {
      const promise = Promise.reject(error400);
      axios.put.mockReturnValue(promise);
      expect(putRequest('/users', {})).rejects.toBe(error400.response.data);
    });
  });

  describe('deleteRequest', () => {
    afterEach(() => {
      axios.delete.mockReset();
      axios.delete.mockRestore();
    });

    it('should edit data successfully', () => {
      const data = { name: 'Bob' };
      const promise = Promise.resolve({ data });
      axios.delete.mockReturnValue(promise);
      expect(deleteRequest('/users')).resolves.toBe(data);
    });

    it('should return error `data` if request is unsuccessfully', () => {
      const promise = Promise.reject(error400);
      axios.delete.mockReturnValue(promise);
      expect(deleteRequest('/users')).rejects.toBe(error400.response.data);
    });
  });

  describe('setAuthorizationToken', () => {
    const token = 'Token here';

    it('should set `Authorization` token', () => {
      setAuthorizationToken(token);
      expect(axios.defaults.headers.common.Authorization)
        .toEqual(`Bearer ${token}`);
      setAuthorizationToken();
    });

    it('should remove `Authorization` from `axios` headers', () => {
      setAuthorizationToken(token);
      expect(axios.defaults.headers.common.Authorization)
        .toEqual(`Bearer ${token}`);
      setAuthorizationToken();
      expect(axios.defaults.headers.common.Authorization).toBeUndefined();
    });
  });
});
