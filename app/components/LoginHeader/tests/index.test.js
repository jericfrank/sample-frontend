import React from 'react';
import { shallow } from 'enzyme';
import { Layout } from 'antd';

import LoginHeader from '../index';

describe('<LoginHeader />', () => {
  it('should display `Layout.Header`', () => {
    const wrapper = shallow(<LoginHeader />);
    expect(wrapper.find(Layout.Header)).toHaveLength(1);
  });
});
