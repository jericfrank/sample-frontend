/**
*
* LoginHeader
*
*/

import React from 'react';
import { Link } from 'react-router-dom';
import { Layout } from 'antd';
import styled from 'styled-components';

const Wrapper = styled.div`
  background-color: #022c53;
  position: fixed;
  width: 100%;
  z-index: 4;

  .logo {
    &:hover {
      color: white;
    }

    &:active {
      text-decoration: none;
    }
  }
`;

class LoginHeader extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Wrapper>
        <Layout.Header
          style={{ backgroundColor: '#484849', paddingLeft: '0px' }}
        >
          <h1
            className="text-white text-center font-champ-bold"
            style={{
              backgroundColor: '#2488e6',
              padding: '0px 50px',
              width: '230px',
            }}
          >
            <Link to="/" className="text-white logo">Job Board</Link>
          </h1>
        </Layout.Header>
      </Wrapper>
    );
  }
}

LoginHeader.propTypes = {
};

export default LoginHeader;
