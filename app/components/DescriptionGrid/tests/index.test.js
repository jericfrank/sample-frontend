import React from 'react';
import { mount } from 'enzyme';
import { ParallaxProvider } from 'react-scroll-parallax';
import { List } from 'antd';

import DescriptionGrid from '../index';

describe('<DescriptionGrid />', () => {
  it('should render 2 `List.Item`', () => {
    const wrapper = mount(
      <ParallaxProvider>
        <DescriptionGrid />
      </ParallaxProvider>
    );
    expect(wrapper.find(List.Item)).toHaveLength(2);
  });
});
