/**
*
* DescriptionGrid
*
*/

import React from 'react';
import { ParallaxBanner } from 'react-scroll-parallax';
import { Row, Col, List, Icon } from 'antd';
import styled from 'styled-components';
import BG_IMAGE from 'images/bg-blackprint.jpg';

const Wrapper = styled.div`
  .parallax-banner-layer-0,
  .parallax-banner-layer-1,
  .parallax-banner-layer-2 {
    position: relative;
    background: #2f3030;
    overflow: hidden;

    &:before {
      content: ' ';
      display: block;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      z-index: 1;
      opacity: 0.1;
      background-image: url(${BG_IMAGE});
    }
  }

  .desc-wrapper {
    &.ant-list-item {
      margin-bottom: 0px;
    }

    .ant-row {
      padding: 40px 20px;
      color: #FFF;
    }

    .anticon {
      font-size: 50px;
      margin-top: 4px;
    }

    h2, .anticon {
      color: #5db5ff;
    }

    p {
      font-size: 14px;
    }
  }
`;

const DATA = [{
  icon: 'rocket',
  title: 'About You',
  description: 'Whether you are an entry level candidate looking to start an exciting career or someone with invaluable experience looking for fresh opportunity, we are interested in hearing from you. If you are passionate about your career and strive for Excellence, you may just find that this is the place for you! Even if you don’t see a position listed for the career you want, we would still love to hear from you. Feel free to call, email, or stop by our office to start a conversation.',
}, {
  icon: 'heart-o',
  title: 'About Us',
  description: 'Our company motto is to exhibit Excellence in all that we do. We strive to hire only the best and most dedicated individuals. We work hard to retain a small business culture and value system where team members are not just viewed as employees and co-workers, but rather as friends and family. We are an equal opportunity employer and evaluate candidates based on both their current skills and their future potential.',
}];

function DescriptionGrid() {
  return (
    <Wrapper>
      <ParallaxBanner
        layers={[{
          image: '#414242',
          amount: 0.6,
          slowerScrollRate: false,
        }]}
        style={{
          height: 'inherit',
        }}
      >
        <Row className="text-white">
          <Col xs={1} sm={1} md={1} lg={2} xl={5}></Col>
          <Col xs={22} sm={22} md={22} lg={20} xl={14}>
            <List
              grid={{ column: 2 }}
              dataSource={DATA}
              renderItem={(item) => (
                <List.Item className="desc-wrapper">
                  <Row className="text-white">
                    <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                      <Icon type={item.icon} />
                    </Col>
                    <Col xs={20} sm={20} md={20} lg={20} xl={20}>
                      <h2>
                        {item.title}
                      </h2>
                      <p>
                        {item.description}
                      </p>
                    </Col>
                  </Row>
                </List.Item>
              )}
            />
          </Col>
          <Col xs={1} sm={1} md={1} lg={2} xl={5}></Col>
        </Row>
      </ParallaxBanner>
    </Wrapper>
  );
}

DescriptionGrid.propTypes = {

};

export default DescriptionGrid;
