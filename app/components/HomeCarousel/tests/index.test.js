import React from 'react';
import { Carousel } from 'antd';
import { shallow } from 'enzyme';

import HomeCarousel from '../index';

describe('<HomeCarousel />', () => {
  it('should display `Carousel`', () => {
    const wrapper = shallow(<HomeCarousel />);
    expect(wrapper.find(Carousel)).toHaveLength(1);
  });
});
