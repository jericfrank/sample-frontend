/**
*
* HomeCarousel
*
*/

import React from 'react';
import { Carousel } from 'antd';
import styled from 'styled-components';
import FontAwesome from 'react-fontawesome';

const Wrapper = styled.div`
  .job-nav {
    outline: none;
    width: 50px;
  }

  h3 {
    background: #5f9ea0;
    color: #fff;
    font-size: 36px;
    line-height: 420px;
    height: 400px;
    padding: 2%;
    position: relative;
    text-align: center;
    -webkit-user-select: none; /* Safari */
    -moz-user-select: none; /* Firefox */
    -ms-user-select: none; /* IE10+/Edge */
    user-select: none; /* Standard */
  }

  .slick-dots {
    bottom: 26px;
  }
`;

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  draggable: true,
  adaptiveHeight: true,
  autoplay: true,
};

class HomeCarousel extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Wrapper>
        <Carousel {...settings}>
          <div className="job-nav">
            <h3><FontAwesome name="grav" size="5x" /></h3>
          </div>
          <div className="job-nav">
            <h3><FontAwesome name="paper-plane" size="5x" /></h3>
          </div>
          <div className="job-nav">
            <h3><FontAwesome name="rocket" size="5x" /></h3>
          </div>
          <div className="job-nav">
            <h3><FontAwesome name="snowflake-o" size="5x" /></h3>
          </div>
          <div className="job-nav">
            <h3><FontAwesome name="bomb" size="5x" /></h3>
          </div>
          <div className="job-nav">
            <h3><FontAwesome name="soccer-ball-o" size="5x" /></h3>
          </div>
          <div className="job-nav">
            <h3><FontAwesome name="superpowers" size="5x" /></h3>
          </div>
          <div className="job-nav">
            <h3><FontAwesome name="motorcycle" size="5x" /></h3>
          </div>
        </Carousel>
      </Wrapper>
    );
  }
}

HomeCarousel.propTypes = {

};

export default HomeCarousel;
