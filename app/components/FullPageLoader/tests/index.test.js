import React from 'react';
import { shallow } from 'enzyme';

import FullPageLoader from '../index';

describe('<FullPageLoader />', () => {
  it('should render `dots`', () => {
    const wrapper = shallow(<FullPageLoader />);
    expect(wrapper.find('.dot1')).toHaveLength(1);
    expect(wrapper.find('.dot2')).toHaveLength(1);
  });
});
