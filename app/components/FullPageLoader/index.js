/**
*
* FullPageLoader
*
*/

import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  background: rgb(74, 73, 74, 0.1);
  height: 100vh;

  .loader {
    position: absolute;
    left: 50%;
    top: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
  }
`;

const CirclePrimitive = styled.div`
  margin: 100px auto;
  width: 80px;
  height: 80px;
  position: relative;
  text-align: center;

  -webkit-animation: sk-rotate 2.0s infinite linear;
  animation: sk-rotate 2.0s infinite linear;

  .dot1, .dot2 {
    width: 60%;
    height: 60%;
    display: inline-block;
    position: absolute;
    top: 0;
    background-color: #79D5FD;
    border-radius: 100%;

    -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
    animation: sk-bounce 2.0s infinite ease-in-out;
  }

  .dot2 {
    background-color: #FF7A80;
    top: auto;
    bottom: 0;
    -webkit-animation-delay: -1.0s;
    animation-delay: -1.0s;
  }

  @-webkit-keyframes sk-rotate { 100% { -webkit-transform: rotate(360deg) }}
  @keyframes sk-rotate { 100% { transform: rotate(360deg); -webkit-transform: rotate(360deg) }}

  @-webkit-keyframes sk-bounce {
    0%, 100% { -webkit-transform: scale(0.0) }
    50% { -webkit-transform: scale(1.0) }
  }

  @keyframes sk-bounce {
    0%, 100% {
      transform: scale(0.0);
      -webkit-transform: scale(0.0);
    } 50% {
      transform: scale(1.0);
      -webkit-transform: scale(1.0);
    }
  }
`;

function FullPageLoader() {
  return (
    <Wrapper>
      <div className="loader">
        <CirclePrimitive>
          <div className="dot1"></div>
          <div className="dot2"></div>
        </CirclePrimitive>
      </div>
    </Wrapper>
  );
}

FullPageLoader.propTypes = {

};

export default FullPageLoader;
