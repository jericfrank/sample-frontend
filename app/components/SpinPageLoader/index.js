/**
*
* PageLoader
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Spin, Icon } from 'antd';

const Wrapper = styled.div`
  height: 100vh;
  font-size: 40px;

  .center-this {
    position: relative;
    float: left;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

function SpinPageLoader({ label }) {
  return (
    <Wrapper className="text-center">
      <div className="center-this">
        <Spin
          tip={label}
          style={{ fontSize: '20px' }}
          indicator={(
            <Icon
              type="loading"
              style={{ fontSize: 60, marginBottom: '14px' }}
              spin
            />
          )}
        />
      </div>
    </Wrapper>
  );
}

SpinPageLoader.propTypes = {
  label: PropTypes.string,
};

export default SpinPageLoader;
