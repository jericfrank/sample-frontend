import React from 'react';
import { shallow } from 'enzyme';
import { Spin } from 'antd';

import SpinLoader from '../index';

describe('<SpinLoader />', () => {
  it('should render loader', () => {
    const wrapper = shallow(<SpinLoader />);
    expect(wrapper.find(Spin)).toHaveLength(1);
  });
});
