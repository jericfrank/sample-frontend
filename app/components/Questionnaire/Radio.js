import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Form, Radio as AntdRadio } from 'antd';

const radioStyle = {
  display: 'block',
  height: '30px',
  lineHeight: '30px',
};

function Radio(props) {
  const { getFieldDecorator, initialValue, fieldName, values } = props;
  const { question, options } = values;

  return (
    <Form.Item label={question}>
      {getFieldDecorator(fieldName, {
        initialValue,
        rules: [{ required: true, message: 'This field is required' }],
      })(
        <AntdRadio.Group>
          {_.map(options, ({ label }, key) => (
            <AntdRadio style={radioStyle} key={key} value={label}>{label}</AntdRadio>
          ))}
        </AntdRadio.Group>
      )}
    </Form.Item>
  );
}

Radio.propTypes = {
  getFieldDecorator: PropTypes.func.isRequired,
  initialValue: PropTypes.string,
  fieldName: PropTypes.string.isRequired,
  values: PropTypes.object.isRequired,
};

export default Radio;
