import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Form, Select } from 'antd';

function List(props) {
  const { getFieldDecorator, initialValue, fieldName, values } = props;
  const { question, options } = values;
  const listOptions = _.map(options, 'label');
  return (
    <Form.Item label={question}>
      {getFieldDecorator(fieldName, {
        initialValue,
        rules: [{ required: true, message: 'This field is required' }],
      })(
        <Select showSearch>
          {_.map(listOptions, (value, index) => <Select.Option value={value} key={index}>{value}</Select.Option>)}
        </Select>
      )}
    </Form.Item>
  );
}

List.propTypes = {
  getFieldDecorator: PropTypes.func.isRequired,
  initialValue: PropTypes.string,
  fieldName: PropTypes.string.isRequired,
  values: PropTypes.object.isRequired,
};

export default List;
