import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Form, Radio } from 'antd';

function TrueFalse(props) {
  const { getFieldDecorator, initialValue, fieldName, values } = props;
  const { question, options } = values;

  return (
    <Form.Item label={question}>
      {getFieldDecorator(fieldName, {
        initialValue,
        rules: [{ required: true, message: 'This field is required' }],
      })(
        <Radio.Group>
          {_.map(options, ({ label }, key) => (
            <Radio.Button key={key} value={label}>{label}</Radio.Button>
          ))}
        </Radio.Group>
      )}
    </Form.Item>
  );
}

TrueFalse.propTypes = {
  getFieldDecorator: PropTypes.func.isRequired,
  initialValue: PropTypes.string,
  fieldName: PropTypes.string.isRequired,
  values: PropTypes.object.isRequired,
};

export default TrueFalse;
