import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

function TextArea(props) {
  const { getFieldDecorator, initialValue, fieldName, values } = props;
  const { question } = values;

  return (
    <Form.Item label={question}>
      {getFieldDecorator(fieldName, {
        initialValue,
        rules: [{ required: true, message: 'This field is required' }],
      })(<Input.TextArea autosize={{ minRows: 2, maxRows: 6 }} />)}
    </Form.Item>
  );
}

TextArea.propTypes = {
  getFieldDecorator: PropTypes.func.isRequired,
  initialValue: PropTypes.string,
  fieldName: PropTypes.string.isRequired,
  values: PropTypes.object.isRequired,
};

export default TextArea;
