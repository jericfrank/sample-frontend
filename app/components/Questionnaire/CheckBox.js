import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Form, Checkbox } from 'antd';

function CheckBox(props) {
  const { getFieldDecorator, initialValue, fieldName, values } = props;
  const { question, options } = values;
  const checkboxOptions = _.map(options, 'label');

  return (
    <Form.Item label={question}>
      {getFieldDecorator(fieldName, {
        initialValue,
        rules: [{ required: true, message: 'This field is required' }],
      })(
        <Checkbox.Group options={checkboxOptions} />
      )}
    </Form.Item>
  );
}

CheckBox.propTypes = {
  getFieldDecorator: PropTypes.func.isRequired,
  initialValue: PropTypes.array,
  fieldName: PropTypes.string.isRequired,
  values: PropTypes.object.isRequired,
};

export default CheckBox;
