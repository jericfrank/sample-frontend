import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';

import List from '../List';

describe('List', () => {
  let props;

  beforeEach(() => {
    props = {
      getFieldDecorator: () => jest.fn(),
      initialValue: 'initialValue',
      fieldName: 'fieldName',
      values: {
        question: 'Question?',
        options: [{
          label: 'Yes',
        }, {
          label: 'No',
        }],
      },
    };
  });

  it('should render `List`', () => {
    const wrapper = shallow(<List {...props} />);
    expect(wrapper.find(Form.Item)).toHaveLength(1);
  });
});
