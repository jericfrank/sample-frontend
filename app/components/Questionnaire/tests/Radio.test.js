import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';

import Radio from '../Radio';

describe('Radio', () => {
  let props;

  beforeEach(() => {
    props = {
      getFieldDecorator: () => jest.fn(),
      initialValue: 'initialValue',
      fieldName: 'fieldName',
      values: {
        question: 'Question?',
        options: [{
          label: 'Yes',
        }, {
          label: 'No',
        }],
      },
    };
  });

  it('should render `Radio`', () => {
    const wrapper = shallow(<Radio {...props} />);
    expect(wrapper.find(Form.Item)).toHaveLength(1);
  });
});
