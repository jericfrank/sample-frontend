import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';

import TextArea from '../TextArea';

describe('TextArea', () => {
  let props;

  beforeEach(() => {
    props = {
      getFieldDecorator: () => jest.fn(),
      initialValue: 'initialValue',
      fieldName: 'fieldName',
      values: {
        question: 'Question?',
        options: [{
          label: 'Yes',
        }, {
          label: 'No',
        }],
      },
    };
  });

  it('should render `TextArea`', () => {
    const wrapper = shallow(<TextArea {...props} />);
    expect(wrapper.find(Form.Item)).toHaveLength(1);
  });
});
