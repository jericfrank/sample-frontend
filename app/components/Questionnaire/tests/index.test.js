import React from 'react';
import { Form, Alert, Modal, message } from 'antd';
import { shallow } from 'enzyme';

import { Questionnaire } from '../index';

describe('<Questionnaire />', () => {
  let props;

  beforeEach(() => {
    props = {
      questions: [
        [{
          id: 24,
          job_id: '5',
          question: 'What is the correct ways to greet a person?',
          sort_order: '15000',
          created_by: '1',
          updated_by: '1',
          type: '4',
          type_name: 'checkbox',
          options: [
            {
              id: 33,
              question_id: '24',
              label: 'Bonjour',
              sort_order: '1',
              created_by: null,
              updated_by: null,
            },
            {
              id: 34,
              question_id: '24',
              label: 'Kilaw',
              sort_order: '2',
              created_by: null,
              updated_by: null,
            },
          ],
        },
        {
          id: 21,
          job_id: '5',
          question: 'Isalin mo ito sa bisaya. "Pumasok ka na sa loob"',
          sort_order: '20000',
          created_by: '1',
          updated_by: null,
          type: '3',
          type_name: 'text',
          options: [],
        },
        {
          id: 22,
          job_id: '5',
          question: 'Ang lupa ang ikaapat na planeta mula sa araw',
          sort_order: '30000',
          created_by: '1',
          updated_by: '1',
          type: '2',
          type_name: 'true_false',
          options: [
            {
              id: 42,
              question_id: '22',
              label: 'True',
              sort_order: '1',
              created_by: null,
              updated_by: null,
            },
            {
              id: 43,
              question_id: '22',
              label: 'False',
              sort_order: '2',
              created_by: null,
              updated_by: null,
            },
          ],
        },
        {
          id: 20,
          job_id: '5',
          question: 'Dalawang balon, hindi malingon, ano ito?',
          sort_order: '22500',
          created_by: '1',
          updated_by: '1',
          type: '5',
          type_name: 'list',
          options: [
            {
              id: 39,
              question_id: '20',
              label: 'Tenga',
              sort_order: '1',
              created_by: null,
              updated_by: null,
            },
          ],
        }],
        [{
          id: 2323,
          job_id: '5',
          question: 'xx"',
          sort_order: '20000',
          created_by: '1',
          updated_by: null,
          type: '3',
          type_name: null,
          options: [],
        }],
      ],
      perPage: 3,
      onSubmit: jest.fn(),
      form: {
        getFieldDecorator: () => jest.fn(),
        validateFields: () => jest.fn(),
      },
      loading: false,
      error: {},
      success: {},
      msg: '',
      noDocs: false,
    };
  });

  it('should render component', () => {
    const wrapper = shallow(<Questionnaire {...props} />);
    expect(wrapper.find(Form)).toHaveLength(1);
  });

  it('should call `this.handleNext`', () => {
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.instance().handleNext = jest.fn();
    wrapper.find('.proceed-btn').first().simulate('click');
    expect(wrapper.instance().handleNext).toHaveBeenCalledTimes(1);
  });

  it('should display `Previous` button', () => {
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.setState({ currentPage: 1 });
    expect(wrapper.find('.previous-btn')).toHaveLength(1);
  });

  it('should return to previous page', () => {
    const e = { preventDefault: jest.fn };
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.setState({ currentPage: 1 });
    wrapper.instance().handlePrevious(e);
    expect(wrapper.state('currentPage')).toEqual(0);
  });

  it('should populate `payload` state on `handlePrevious`', () => {
    const e = { preventDefault: jest.fn };
    const payload = { key: 'value' };
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false, payload),
      },
    };
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.setState({ currentPage: 1 });
    wrapper.instance().handlePrevious(e);
    expect(wrapper.state('payload')).toEqual(payload);
  });

  it('should NOT populate `payload` state on `handlePrevious`', () => {
    const e = { preventDefault: jest.fn };
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(true),
      },
    };
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.setState({ currentPage: 1 });
    wrapper.instance().handlePrevious(e);
    expect(wrapper.state('payload')).toEqual({});
  });

  it('should populate `payload` state on `handleNext` and move to the next page', () => {
    const e = { preventDefault: jest.fn() };
    const questions = [1, 2, 3];
    const payload = { key: 'value' };
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false, payload),
      },
    };
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.instance().handleNext(e, questions);
    expect(wrapper.state('payload')).toEqual(payload);
    expect(wrapper.state('currentPage')).toEqual(1);
  });

  it('should populate `payload` state on `handleNext` and call `onSubmit`', () => {
    const e = { preventDefault: jest.fn() };
    const questions = [];
    const payload = {
      24: 'Bonjour',
      21: 'Isalin mo ito sa bisaya. "Pumasok ka na sa loob"',
      22: 'Ang lupa ang ikaapat na planeta mula sa araw',
      20: 'Dalawang balon, hindi malingon, ano ito?',
    };
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false, payload),
      },
    };
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.instance().handleNext(e, questions);
    expect(wrapper.state('payload')).toEqual(payload);
    expect(props.onSubmit).toHaveBeenCalledTimes(1);
  });

  it('should NOT do anything if form has errors on `onSubmit`', () => {
    const e = { preventDefault: jest.fn };
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(true),
      },
    };
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.instance().handleNext(e);
    expect(wrapper.state('payload')).toEqual({});
  });

  it('should NOT do anything if form has errors on `onSubmit`', () => {
    props = { ...props, error: false, success: false };
    const wrapper = shallow(<Questionnaire {...props} />);
    expect(wrapper.find(Alert)).toHaveLength(0);
  });

  it('should show `Modal.confirm`', () => {
    props = { ...props, noDocs: true };
    const spy = jest.spyOn(Modal, 'confirm');
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.instance().showConfirm({});
    expect(spy).toHaveBeenCalledTimes(1);
    Modal.confirm.mockReset();
    Modal.confirm.mockRestore();
  });

  it('should `handleOnOk`', () => {
    const spy = jest.spyOn(message, 'info');
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.instance().handleOnOk();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(props.onSubmit).toHaveBeenCalledTimes(1);
    message.info.mockReset();
    message.info.mockRestore();
  });

  it('should `handleOnCancel`', () => {
    const wrapper = shallow(<Questionnaire {...props} />);
    wrapper.instance().handleOnCancel();
    expect(props.onSubmit).toHaveBeenCalledTimes(1);
  });
});
