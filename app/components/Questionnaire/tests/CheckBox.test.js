import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';

import CheckBox from '../CheckBox';

describe('CheckBox', () => {
  let props;

  beforeEach(() => {
    props = {
      getFieldDecorator: () => jest.fn(),
      initialValue: 'initialValue',
      fieldName: 'fieldName',
      values: {
        question: 'Question?',
        options: [{
          label: 'Yes',
        }, {
          label: 'No',
        }],
      },
    };
  });

  it('should render `CheckBox`', () => {
    const wrapper = shallow(<CheckBox {...props} />);
    expect(wrapper.find(Form.Item)).toHaveLength(1);
  });
});
