import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';

import TrueFalse from '../TrueFalse';

describe('TrueFalse', () => {
  let props;

  beforeEach(() => {
    props = {
      getFieldDecorator: () => jest.fn(),
      initialValue: 'initialValue',
      fieldName: 'fieldName',
      values: {
        question: 'Question?',
        options: [{
          label: 'Yes',
        }, {
          label: 'No',
        }],
      },
    };
  });

  it('should render `TrueFalse`', () => {
    const wrapper = shallow(<TrueFalse {...props} />);
    expect(wrapper.find(Form.Item)).toHaveLength(1);
  });
});
