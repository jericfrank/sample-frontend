/**
*
* Questionnaire
*
*/

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Card, Form, Button, Icon, Alert, Modal, message } from 'antd';

import TextArea from './TextArea';
import CheckBox from './CheckBox';
import TrueFalse from './TrueFalse';
import Radio from './Radio';
import List from './List';

export class Questionnaire extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    currentPage: 0,
    payload: {},
  }

  handleNext = (e, questions) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const payload = {
          ...this.state.payload,
          ...values,
        };

        if (this.state.currentPage >= (questions.length - 1)) {
          this.showConfirm(payload);
        } else {
          this.setState({ currentPage: this.state.currentPage + 1 });
        }

        this.setState({ payload });
      }
    });
  }

  handleOnOk = () => {
    message.info('Please upload your Cover Letter and CV/Resume.', 3);
    this.props.onSubmit(this.state.payload, true);
  }

  handleOnCancel = () => {
    this.props.onSubmit(this.state.payload);
  }

  showConfirm = (payload) => {
    const { onSubmit, noDocs } = this.props;

    if (noDocs) {
      const confirm = Modal.confirm;
      confirm({
        title: 'While a cover letter and resume are optional, we find submitting at least one of them is helpful in evaluating candidates.',
        content: 'Would you like to include one of these document with your application?',
        okText: 'Yes',
        cancelText: 'No',
        onOk: this.handleOnOk,
        onCancel: this.handleOnCancel,
        confirmLoading: true,
      });
    } else {
      onSubmit(payload);
    }
  }

  handlePrevious = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const payload = {
          ...this.state.payload,
          ...values,
        };
        this.setState({ payload });
      }
    });
    this.setState({ currentPage: this.state.currentPage - 1 });
  }

  displayMsg = (type) => (
    <Alert
      style={{ marginBottom: '10px' }}
      message={this.props.msg}
      type={type}
      closable
    />
  );

  renderQuestionType = (values) => {
    const { getFieldDecorator } = this.props.form;
    const { id } = values;

    const props = {
      getFieldDecorator,
      initialValue: this.state.payload[id],
      fieldName: (id).toString(),
      values,
    };

    const components = {
      text: TextArea,
      checkbox: CheckBox,
      true_false: TrueFalse,
      list: List,
      yes_no: Radio,
      radio: Radio,
    };

    const Component = components[_.result(values, 'type_name', 'text')];
    return <Component key={id} {...props} />;
  };

  render() {
    const { questions } = this.props;
    const isDone = this.state.currentPage >= (questions.length - 1);
    const noPrev = this.state.currentPage === 0;

    return (
      <Card
        title={(
          <div>
            Questionnaire
            <span className="pull-right">
              {this.state.currentPage + 1} of {questions.length}
            </span>
          </div>
        )}
        style={{ margin: 'auto', marginBottom: '20px' }}
      >
        <Form layout="vertical">
          {_.map(questions[this.state.currentPage], this.renderQuestionType)}

          {this.props.error ? this.displayMsg('error') : '' }
          {this.props.success ? this.displayMsg('success') : '' }

          <Button
            className="pull-right proceed-btn"
            type="primary"
            htmlType="submit"
            size="large"
            style={{ marginLeft: '10px' }}
            onClick={(e) => this.handleNext(e, questions)}
            loading={this.props.loading}
            icon={isDone ? 'rocket' : null}
          >
            {isDone ? 'Click here to finalize and submit your application' : 'Next'} {isDone ? null : <Icon type="right" />}
          </Button>
          {noPrev ? null : (
            <Button
              className="pull-right previous-btn"
              type="default"
              htmlType="submit"
              size="large"
              onClick={this.handlePrevious}
            >
              <Icon type="left" /> Previous
            </Button>
          )}
        </Form>
      </Card>
    );
  }
}

Questionnaire.propTypes = {
  form: PropTypes.object.isRequired,
  questions: PropTypes.array.isRequired,
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.any.isRequired,
  success: PropTypes.any.isRequired,
  msg: PropTypes.string.isRequired,
  noDocs: PropTypes.bool.isRequired,
};

export default Form.create()(Questionnaire);
