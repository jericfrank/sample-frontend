/**
*
* SpinLoader
*
*/

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export const Wrapper = styled.div`
  border: ${(props) => props.border}px solid transparent; /* Light grey */
  border-top: ${(props) => props.border}px solid #3498db; /* Blue */
  border-radius: 100%;
  width: ${(props) => props.widthHeight}px;
  height: ${(props) => props.widthHeight}px;
  animation: spin 1.3s linear infinite;
  margin: auto;

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
`;

function SpinLoader(props) {
  const sizes = {
    xs: {
      widthHeight: 30,
      border: 2,
    },
    sm: {
      widthHeight: 60,
      border: 4,
    },
    md: {
      widthHeight: 90,
      border: 6,
    },
    lg: {
      widthHeight: 120,
      border: 8,
    },
    xl: {
      widthHeight: 150,
      border: 10,
    },
  };

  const size = sizes[_.result(props, 'size', 'md')];

  return (
    <Wrapper {...size} />
  );
}

SpinLoader.propTypes = {
  size: PropTypes.string,
};

export default SpinLoader;
