import React from 'react';
import { shallow } from 'enzyme';

import SpinLoader from '../index';

describe('<SpinLoader />', () => {
  it('should render loader', () => {
    const wrapper = shallow(<SpinLoader />).dive();
    expect(wrapper).toHaveLength(1);
  });
});
