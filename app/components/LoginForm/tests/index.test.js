import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';

import { LoginForm } from '../index';

describe('<LoginForm />', () => {
  let props;

  beforeEach(() => {
    props = {
      authenticating: false,
      onLogin: jest.fn(),
      onForgotPassClick: jest.fn(),
      form: {
        getFieldDecorator: () => jest.fn(),
        validateFields: () => jest.fn(),
      },
    };
  });

  it('should render `LoginForm`', () => {
    const wrapper = shallow(<LoginForm {...props} />);
    expect(wrapper.find(Form)).toHaveLength(1);
  });

  it('should call `onLogin` if form is valid on `handleSubmit`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false, {}),
      },
    };
    const wrapper = shallow(<LoginForm {...props} />);
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.onLogin).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `onLogin` if form is invalid on `handleSubmit`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(true),
      },
    };
    const wrapper = shallow(<LoginForm {...props} />);
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.onLogin).toHaveBeenCalledTimes(0);
  });
});
