/**
*
* LoginForm
*
*/

import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Form, Icon, Input, Button, Col } from 'antd';

import './styles';

const FormItem = Form.Item;

export class LoginForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onLogin(values);
      }
    });
  }

  render() {
    const { form, queryParams } = this.props;
    const { getFieldDecorator } = form;

    return (
      <div className="login-container">
        <div className="login-content">
          <h1 className="font-champ-bold text-center">Hi there!</h1>
          <h1 className="font-champ-bold text-center">Nice to see you again.</h1>

          <p className="text-center">
            Enter your login credentials to get started.
          </p>

          <Form onSubmit={this.handleSubmit} className="loginpage-form">
            <FormItem>
              {getFieldDecorator('email', {
                rules: [{
                  required: true,
                  message: 'Please input your email',
                }, {
                  type: 'email',
                  message: 'Oops, email address seems invalid. Please try again.',
                }],
              })(
                <Input
                  autoFocus
                  prefix={(
                    <Icon
                      type="mail"
                      style={{ color: 'rgba(0,0,0,.25)' }}
                    />
                  )}
                  placeholder="Email"
                  size="large"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{
                  required: true,
                  message: 'Please input your password',
                }],
              })(
                <Input
                  prefix={(
                    <Icon
                      type="lock"
                      style={{ color: 'rgba(0,0,0,.25)' }}
                    />
                  )}
                  type="password"
                  placeholder="Password"
                  size="large"
                />
              )}
            </FormItem>
            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
              <a
                className="bordered-link"
                onClick={this.props.onForgotPassClick}
              >
                Forgot password
              </a>
            </Col>
            <Col xs={24} sm={12} md={12} lg={12} xl={12} className="text-right">
              <Button
                type="primary"
                htmlType="submit"
                className="loginpage-form-button"
                loading={this.props.authenticating}
                size="large"
              >
                Login
              </Button>
            </Col>

            <Col xs={24} sm={24} md={24} lg={24} xl={24} className="text-center">
              <div className="login-footer text-center">
                New to Job Board?&nbsp;
                <Link to={`/register${queryParams}`}>
                  <Button type="primary" ghost>
                    <span style={{ color: '#1890ff' }}>Create an account.</span>
                  </Button>
                </Link>
              </div>
            </Col>
          </Form>
        </div>
      </div>
    );
  }
}

LoginForm.propTypes = {
  form: PropTypes.object.isRequired,
  onLogin: PropTypes.func.isRequired,
  authenticating: PropTypes.bool.isRequired,
  onForgotPassClick: PropTypes.func.isRequired,
  queryParams: PropTypes.string,
};

export default Form.create()(LoginForm);
