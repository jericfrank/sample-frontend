/**
*
* SignUpForm
*
*/

import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Form, Alert, Input, Col, Button, Icon, Row } from 'antd';
// import styled from 'styled-components';
import Recaptcha from 'react-recaptcha';

import { SUCCESS_MSG, CAPTCHA_KEY } from './constants';

const FormItem = Form.Item;

class SignUpForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    confirmDirty: false,
  };

  setReCaptcha = (e) => (this.recaptcha = e)

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.signUp(values);
      }
      this.recaptcha.reset();
    });
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirmPassword'], { force: true });
    }
    callback();
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    const validLength = value && value.length >= 6;
    if (validLength && value !== form.getFieldValue('password')) {
      callback('Passwords did not matched');
    } else {
      callback();
    }
  }

  displayErrorMsg = (hasError, msgToUser) => (
    hasError
      ? (<Alert
        style={{ marginBottom: '10px' }}
        className="text-center"
        message={msgToUser}
        type="error"
      />)
      : null
  )

  handleCaptcha = (value) => {
    this.props.form.setFields({
      captcha: {
        value,
      },
    });
  };

  render() {
    const { form, saving, isSuccess, hasError, msgToUser, queryParams } = this.props;
    const { getFieldDecorator } = form;

    if (isSuccess) {
      return (
        <div className="padding-10 pad-bottom" style={{ paddingBottom: '20px' }}>
          <Alert
            message={SUCCESS_MSG}
            description={msgToUser}
            type="success"
            showIcon
          />
          <br />
          <Link to="/login" className="padding-10 pad-bottom">
            <Button className="pull-right" type="primary" size="large">
              Login <Icon type="right" />
            </Button>
            <div className="clearfix"></div>
          </Link>
        </div>
      );
    }

    const itemProps = {
      colon: false,
      hasFeedback: true,
    };

    return (
      <Form
        onSubmit={this.handleSubmit}
        layout="horizontal"
        className="loginpage-form placeholder-dark"
      >
        {this.displayErrorMsg(hasError, msgToUser)}

        <Row gutter={16}>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <FormItem {...itemProps}>
              {getFieldDecorator('email', {
                rules: [
                  { type: 'email', message: 'Invalid email address' },
                  { required: true, message: 'Email is required' },
                ],
              })(
                <Input
                  autoFocus
                  className="signupform-email"
                  size="large"
                  prefix={<Icon type="mail" />}
                  placeholder="Email address"
                />
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <FormItem {...itemProps}>
              {getFieldDecorator('password', {
                rules: [
                  { required: true, message: 'Password is required' },
                  { min: 6, message: 'Password should be at least 6 characters' },
                  { validator: this.validateToNextPassword },
                ],
              })(
                <Input
                  className="signupform-password"
                  type="password"
                  size="large"
                  prefix={<Icon type="key" />}
                  placeholder="Password"
                />
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <FormItem {...itemProps}>
              {getFieldDecorator('confirmPassword', {
                rules: [
                  { required: true, message: 'Confirm password is required' },
                  { min: 6, message: 'Password should be at least 6 characters' },
                  { validator: this.compareToFirstPassword },
                ],
              })(
                <Input
                  className="signupform-confirm"
                  type="password"
                  size="large"
                  prefix={<Icon type="key" />}
                  placeholder="Confirm password"
                  onBlur={this.handleConfirmBlur}
                />
              )}
            </FormItem>
          </Col>
          <div className="clear"></div>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            {saving ? (<div>&nbsp;</div>) : (
              <FormItem>
                {getFieldDecorator('captcha', {
                  rules: [{ required: true, message: 'Captcha required!' }],
                })(
                  <div style={{ width: '304px', margin: 'auto' }}>
                    <Recaptcha
                      ref={this.setReCaptcha}
                      verifyCallback={this.handleCaptcha}
                      sitekey={CAPTCHA_KEY}
                    />
                  </div>
                )}
              </FormItem>
            )}
          </Col>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <FormItem className="no-margin text-right">
              <Button
                type="primary"
                htmlType="submit"
                className="loginpage-form-button"
                size="large"
                loading={saving}
              >
                Register
              </Button>
            </FormItem>
          </Col>

          <Col xs={24} sm={24} md={24} lg={24} xl={24} className="text-center">
            <div className="login-footer text-center">
              Already have an account?&nbsp;
              <Link to={`/login${queryParams}`}>
                <Button type="primary" ghost>
                  <span style={{ color: '#1890ff' }}>Sign in here.</span>
                </Button>
              </Link>
            </div>
          </Col>
        </Row>
      </Form>
    );
  }
}

SignUpForm.propTypes = {
  form: PropTypes.object.isRequired,
  signUp: PropTypes.func.isRequired,
  saving: PropTypes.bool.isRequired,
  isSuccess: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  msgToUser: PropTypes.string,
  queryParams: PropTypes.string,
};

export default Form.create()(SignUpForm);
