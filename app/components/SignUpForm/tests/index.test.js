import React from 'react';
import { shallow } from 'enzyme';
import { Form, Alert } from 'antd';

import SignUpForm from '../index';

describe('<SignUpForm />', () => {
  let props;

  beforeEach(() => {
    props = {
      signUp: jest.fn(),
      saving: false,
      isSuccess: false,
      hasError: false,
      msgToUser: '',
      genders: [
        {
          id: 1,
          description: 'Male',
        },
      ],
      form: {
        getFieldDecorator: () => jest.fn(),
        setFieldsValue: jest.fn(),
        validateFields: jest.fn(),
      },
    };
  });

  it('should render component', () => {
    const wrapper = shallow(<SignUpForm {...props} />).shallow();
    expect(wrapper.find(Form)).toHaveLength(1);
  });

  it('should call `signUp` if form is VALID on `handleSubmit`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false),
      },
    };
    const wrapper = shallow(<SignUpForm {...props} />).shallow();
    wrapper.instance().recaptcha = { reset: jest.fn() };
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.signUp).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `signUp` if form is VALID on `handleSubmit`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(true),
      },
    };
    const wrapper = shallow(<SignUpForm {...props} />).shallow();
    wrapper.instance().recaptcha = { reset: jest.fn() };
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.signUp).toHaveBeenCalledTimes(0);
  });

  it('should call `handleConfirmBlur`', () => {
    const wrapper = shallow(<SignUpForm {...props} />).shallow();
    wrapper.instance().handleConfirmBlur({ target: { value: 'test' } });
    expect(wrapper.state().confirmDirty).toBe(true);
  });

  it('should call `validateToNextPassword`', () => {
    const wrapper = shallow(<SignUpForm {...props} />).shallow();
    wrapper.setState({ confirmDirty: 'test' });
    wrapper.instance().validateToNextPassword(null, 'test', jest.fn());
    expect(props.form.validateFields).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `validateToNextPassword`', () => {
    const wrapper = shallow(<SignUpForm {...props} />).shallow();
    wrapper.setState({ confirmDirty: false });
    wrapper.instance().validateToNextPassword(null, 'test', jest.fn());
    expect(props.form.validateFields).toHaveBeenCalledTimes(0);
  });

  it('should call `compareToFirstPassword`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        getFieldValue: jest.fn(),
      },
    };
    const wrapper = shallow(<SignUpForm {...props} />).shallow();
    wrapper.instance().compareToFirstPassword(null, 'test', jest.fn());
    wrapper.instance().compareToFirstPassword(null, 'test password', jest.fn());
    expect(props.form.getFieldValue).toHaveBeenCalledTimes(1);
  });

  it('should display error `Alert` message', () => {
    const errorProps = Object.assign({}, props, { hasError: true });
    const wrapper = shallow(<SignUpForm {...errorProps} />).shallow();
    expect(wrapper.find(Alert)).toHaveLength(1);
  });

  it('should display SignUp Success `Alert` message', () => {
    const successProps = Object.assign({}, props, { isSuccess: true });
    const wrapper = shallow(<SignUpForm {...successProps} />).shallow();
    expect(wrapper.find(Alert)).toHaveLength(1);
  });

  it('should call `setFields` on `handleCaptcha`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        setFields: jest.fn(),
      },
    };
    const wrapper = shallow(<SignUpForm {...props} />).shallow();
    wrapper.instance().handleCaptcha('token');
    expect(props.form.setFields).toHaveBeenCalledTimes(1);
  });

  it('should `setReCaptcha`', () => {
    const recaptcha = 'test';
    const wrapper = shallow(<SignUpForm {...props} />).shallow();
    wrapper.instance().setReCaptcha(recaptcha);
    expect(wrapper.instance().recaptcha).toEqual(recaptcha);
  });

  it('should hide `Recaptcha` when saving', () => {
    props = { ...props, saving: true };
    const wrapper = shallow(<SignUpForm {...props} />).shallow();
    expect(wrapper.find(Form.Item)).toHaveLength(4);
  });
});
