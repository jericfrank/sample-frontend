export const SUCCESS_MSG = 'Registration Successful!';
export const SUCCESS_DESC = `
  Please check your email in a few minutes to verify your account.
  If you have not recieved it, please check your spam folder, then mark it as
  NOT spam.
`;

export const CAPTCHA_KEY = '6LckxFkUAAAAAClme46s4nC4rFa0jiBoGbPHSscc';
