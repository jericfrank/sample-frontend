import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';

import ProfileContactInfo from '../index';

describe('<ProfileContactInfo />', () => {
  let props;

  beforeEach(() => {
    props = {
      user: {},
      getFieldDecorator: () => jest.fn(),
      setFieldsValue: () => jest.fn(),
    };
  });

  it('should render 2 `Form.Item`s', () => {
    const wrapper = shallow(<ProfileContactInfo {...props} />);
    expect(wrapper.find(Form.Item)).toHaveLength(2);
  });

  it('should invoke `props.setFieldsValue` once', () => {
    const spy = jest.spyOn(props, 'setFieldsValue');
    const wrapper = shallow(<ProfileContactInfo {...props} />);
    wrapper.instance().componentDidMount();
    expect(spy).toHaveBeenCalledTimes(1);
    spy.mockReset();
    spy.mockRestore();
  });
});
