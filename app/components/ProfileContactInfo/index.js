/**
*
* ProfileContactInfo
*
*/

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Form, Input, Icon } from 'antd';
// import styled from 'styled-components';


class ProfileContactInfo extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { user, setFieldsValue } = this.props;

    setFieldsValue({
      email: _.result(user, 'email', null),
      mobile_no: _.result(user, 'mobile_no', null),
    });
  }

  render() {
    const { getFieldDecorator } = this.props;
    return (
      <Row gutter={16}>
        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
          <Form.Item hasFeedback className="form-item">
            {getFieldDecorator('email', {
              rules: [
                { type: 'email', message: 'Invalid email address' },
                { required: true, message: 'Email is required' },
              ],
            })(
              <Input
                readOnly
                prefix={<Icon type="mail" />}
                placeholder="Email"
                size="large"
              />
            )}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
          <Form.Item hasFeedback className="form-item">
            {getFieldDecorator('mobile_no', {
              rules: [{
                required: true,
                message: 'Phone number is required',
              }],
            })(
              <Input
                prefix={<Icon type="phone" />}
                placeholder="Phone number"
                size="large"
              />
            )}
          </Form.Item>
        </Col>
      </Row>
    );
  }
}

ProfileContactInfo.propTypes = {
  user: PropTypes.object,
  getFieldDecorator: PropTypes.func.isRequired,
  setFieldsValue: PropTypes.func.isRequired,
};

export default ProfileContactInfo;
