import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';

import ProfileBasicInfo from '../index';

describe('<ProfileBasicInfo />', () => {
  let props;

  beforeEach(() => {
    props = {
      user: {},
      getFieldDecorator: () => jest.fn(),
      setFieldsValue: () => jest.fn(),
    };
  });

  it('should render 3 `Form.Item`s', () => {
    const wrapper = shallow(<ProfileBasicInfo {...props} />);
    expect(wrapper.find(Form.Item)).toHaveLength(3);
  });

  it('should invoke `props.setFieldsValue` once', () => {
    const spy = jest.spyOn(props, 'setFieldsValue');
    const wrapper = shallow(<ProfileBasicInfo {...props} />);
    wrapper.instance().componentDidMount();
    expect(spy).toHaveBeenCalledTimes(1);
    spy.mockReset();
    spy.mockRestore();
  });

  it('should invoke `props.setFieldsValue` twice if location is available', () => {
    props = { ...props, user: { location: 'Location' } };
    const spy = jest.spyOn(props, 'setFieldsValue');
    const wrapper = shallow(<ProfileBasicInfo {...props} />);
    wrapper.instance().componentDidMount();
    expect(spy).toHaveBeenCalledTimes(2);
    spy.mockReset();
    spy.mockRestore();
  });

  it('should filter search options', () => {
    const wrapper = shallow(<ProfileBasicInfo {...props} />);
    const option = {
      props: {
        children: 'Test',
      },
    };
    expect(wrapper.instance().handleSearchFilter('test', option))
      .toEqual(true);
  });
});
