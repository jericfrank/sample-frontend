/**
*
* ProfileBasicInfo
*
*/

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Form, Input, Icon, Select } from 'antd';
// import styled from 'styled-components';

import { COUTRIES } from './constants';

class ProfileBasicInfo extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { user, setFieldsValue } = this.props;
    const location = _.result(user, 'location', null);

    setFieldsValue({
      first_name: _.result(user, 'first_name', null),
      last_name: _.result(user, 'last_name', null),
    });

    if (location) {
      setFieldsValue({ location });
    }
  }

  handleSearchFilter = (input, option) => (
    option.props
      .children.toLowerCase()
      .indexOf(input.toLowerCase()) >= 0
  )

  render() {
    const { getFieldDecorator } = this.props;

    return (
      <Row gutter={16}>
        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
          <Form.Item hasFeedback className="form-item">
            {getFieldDecorator('first_name', {
              rules: [{
                required: true, message: 'First name is required',
              }],
            })(
              <Input
                autoFocus
                placeholder="First name"
                prefix={<Icon type="user" />}
                size="large"
              />
            )}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
          <Form.Item hasFeedback className="form-item">
            {getFieldDecorator('last_name', {
              rules: [{
                required: true, message: 'Last name is required',
              }],
            })(
              <Input
                placeholder="Last name"
                prefix={<Icon type="user" />}
                size="large"
              />
            )}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
          <Form.Item hasFeedback>
            {getFieldDecorator('location', {
              rules: [{
                required: true, message: 'Location is required',
              }],
            })(
              <Select
                size="large"
                showSearch
                style={{ width: '100%' }}
                placeholder="Location"
                optionFilterProp="children"
                filterOption={this.handleSearchFilter}
              >
                {COUTRIES.map((country) => (
                  <Select.Option key={country} value={country}>
                    {country}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
      </Row>
    );
  }
}

ProfileBasicInfo.propTypes = {
  user: PropTypes.object,
  getFieldDecorator: PropTypes.func.isRequired, // antd `Form.getFieldDecorator`
  setFieldsValue: PropTypes.func.isRequired, // antd `Form.setFieldsValue`
};

export default ProfileBasicInfo;
