/**
*
* FilterTags
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Tag, Spin, Icon } from 'antd';
import FontAwesome from 'react-fontawesome';
import styled from 'styled-components';
import _ from 'lodash';

const CheckableTag = Tag.CheckableTag;
const Wrapper = styled.span`
  .custom-checkabletag {
    &.ant-tag-checkable {
      background-color: #fafafa;
      border: 1px solid #c6c6c6;
      margin-top: 5px;
      margin-bottom: 5px;

      &.ant-tag-checkable-checked {
        background-color: #1890ff;
        border-color: transparent;
      }

      &.filtertag-remove {
        background-color: #F5F5F5;
        color: #F5222D;
        border: 1px solid #c6c6c6;

        &:hover {
          background-color: #F5222D;
          color: white;
          border: 1px solid #F5222D;
        }
      }
    }
  }

  .filtertag-more {
    color: #1890ff;
  }
`;

class FilterTags extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    selectedTags: [],
    filterTags: [],
    showMore: false,
  };

  componentWillMount() {
    this.setState({ filterTags: _.slice(this.props.tags, 0, 5) });
  }

  onRemoveAllTags() {
    this.setState({ selectedTags: [] });
    this.props.onRemoveFilters();
  }

  onSelectTag(tag, checked) {
    const { selectedTags } = this.state;
    const nextSelectedTags = checked ?
            [...selectedTags, tag] :
            selectedTags.filter((t) => t !== tag);
    this.setState({ selectedTags: nextSelectedTags });
    this.props.onFilter(nextSelectedTags, 500);
  }

  onMoreClick = () => {
    this.setState({
      showMore: true,
      filterTags: this.props.tags,
    });
  }

  render() {
    const { selectedTags, showMore, filterTags } = this.state;
    const { loading } = this.props;

    if (loading) {
      return (
        <Spin
          indicator={<Icon type="loading" style={{ fontSize: 14 }} spin />}
        />
      );
    }

    return (
      <Wrapper>
        {filterTags.length ? (
          <span>
            {filterTags.map((tag, index) => (
              <CheckableTag
                key={tag}
                className={`custom-checkabletag filtertag-${index}`}
                checked={selectedTags.indexOf(tag) > -1}
                onChange={(checked) => this.onSelectTag(tag, checked)}
              >
                {tag}
              </CheckableTag>
            ))}
          </span>
        ) : (
          <i className="no-filters" style={{ color: 'rgba(0, 0, 0, 0.45)' }}>
            No filters
          </i>
        )}
        {selectedTags.length ? (
          <CheckableTag
            key="remove-tag-1"
            className="custom-checkabletag filtertag-remove"
            checked
            onChange={() => this.onRemoveAllTags()}
          >
            <FontAwesome name="remove" />
            <span style={{ paddingLeft: '5px' }}>Remove filters</span>
          </CheckableTag>
        ) : null}

        {!showMore && filterTags.length ? (
          <CheckableTag
            key="more-tag-1"
            className="filtertag-more"
            onChange={this.onMoreClick}
          >
            <span style={{ paddingLeft: '5px' }}>More</span>
          </CheckableTag>
        ) : null}
      </Wrapper>
    );
  }
}

FilterTags.propTypes = {
  loading: PropTypes.bool.isRequired,
  tags: PropTypes.array.isRequired,
  onFilter: PropTypes.func.isRequired,
  onRemoveFilters: PropTypes.func.isRequired,
};

export default FilterTags;
