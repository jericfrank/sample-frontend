import React from 'react';
import { shallow, mount } from 'enzyme';
import { Tag, Spin } from 'antd';

import FilterTags from '../index';

const CheckableTag = Tag.CheckableTag;

describe.only('<FilterTags />', () => {
  let TAGS;

  beforeEach(() => {
    TAGS = [
      'Detailing',
      'Structural',
      'Information Technology',
      'Human Resources',
    ];
  });

  it('should display 4 tags', () => {
    const wrapper = shallow(
      <FilterTags
        loading={false}
        tags={TAGS}
        onFilter={jest.fn()}
        onRemoveFilters={jest.fn()}
      />
    );
    expect(wrapper.find(CheckableTag)).toHaveLength(5);
  });

  it('should display remove all tags button', () => {
    const wrapper = shallow(
      <FilterTags
        loading={false}
        tags={TAGS}
        onFilter={jest.fn()}
        onRemoveFilters={jest.fn()}
      />
    );
    wrapper.setState({ selectedTags: ['Structural'] });
    expect(wrapper.find('.filtertag-remove')).toHaveLength(1);
  });

  it('should display loading `Spin`', () => {
    const wrapper = shallow(
      <FilterTags
        loading
        tags={[TAGS]}
        onFilter={jest.fn()}
        onRemoveFilters={jest.fn()}
      />
    );
    expect(wrapper.find(Spin)).toHaveLength(1);
  });

  it('should display `No filters`', () => {
    const wrapper = shallow(
      <FilterTags
        loading={false}
        tags={[]}
        onFilter={jest.fn()}
        onRemoveFilters={jest.fn()}
      />
    );
    expect(wrapper.find('.no-filters')).toHaveLength(1);
    expect(wrapper.find('.no-filters').first().text()).toEqual('No filters');
  });

  it('should setState `showMore` to `true`', () => {
    const wrapper = shallow(
      <FilterTags
        loading={false}
        tags={[TAGS]}
        onFilter={jest.fn()}
        onRemoveFilters={jest.fn()}
      />
    );
    wrapper.instance().onMoreClick();
    expect(wrapper.state('showMore')).toEqual(true);
  });

  it('should add `Detailer` to `selectedTags` state', () => {
    const wrapper = shallow(
      <FilterTags
        loading={false}
        tags={[TAGS]}
        onFilter={jest.fn()}
        onRemoveFilters={jest.fn()}
      />
    );
    wrapper.instance().onSelectTag('Detailer', true);
    expect(wrapper.state('selectedTags')).toEqual(['Detailer']);
  });

  it('should remove `Detailer` from `selectedTags` state', () => {
    const wrapper = shallow(
      <FilterTags
        loading={false}
        tags={[TAGS]}
        onFilter={jest.fn()}
        onRemoveFilters={jest.fn()}
      />
    );
    wrapper.setState({ selectedTags: ['Detailer'] });
    wrapper.instance().onSelectTag('Detailer', false);
    expect(wrapper.state('selectedTags')).toEqual([]);
  });

  it('should remove all tags', () => {
    const wrapper = shallow(
      <FilterTags
        loading={false}
        tags={[TAGS]}
        onFilter={jest.fn()}
        onRemoveFilters={jest.fn()}
      />
    );
    wrapper.setState({ selectedTags: ['Detailer'] });
    wrapper.instance().onRemoveAllTags();
    expect(wrapper.state('selectedTags')).toEqual([]);
  });

  it('should call `onSelectTag` when tag is clicked', () => {
    const wrapper = mount(
      <FilterTags
        loading={false}
        tags={[TAGS]}
        onFilter={jest.fn()}
        onRemoveFilters={jest.fn()}
      />
    );
    const instance = wrapper.instance();
    const spy = jest.spyOn(instance, 'onSelectTag');
    wrapper.setState({ selectedTags: ['Detailer'] });
    wrapper.find('.filtertag-0').first().simulate('click');
    expect(spy).toHaveBeenCalledTimes(1);
    instance.onSelectTag.mockRestore();
  });

  it('should call `onRemoveAllTags` when remove tag is clicked', () => {
    const wrapper = mount(
      <FilterTags
        loading={false}
        tags={[TAGS]}
        onFilter={jest.fn()}
        onRemoveFilters={jest.fn()}
      />
    );
    const instance = wrapper.instance();
    const spy = jest.spyOn(instance, 'onRemoveAllTags');
    wrapper.setState({ selectedTags: ['Detailer'] });
    wrapper.find('.filtertag-remove').first().simulate('click');
    expect(spy).toHaveBeenCalledTimes(1);
    instance.onRemoveAllTags.mockRestore();
  });
});
