import React from 'react';
import { shallow } from 'enzyme';
import { Button } from 'antd';

import SocialShare from '../index';

describe('<SocialShare />', () => {
  it('should render 3 `Buttons`', () => {
    const wrapper = shallow(<SocialShare />);
    expect(wrapper.find(Button)).toHaveLength(3);
  });
});
