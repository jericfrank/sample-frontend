/**
*
* SocialShare
*
*/

import React from 'react';
import { Icon, Button, Divider } from 'antd';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 40px;

  .social-share {
    font-size: 16px;
  }
`;

function SocialShare() {
  return (
    <Wrapper className="social-share text-center">
      <p>Sharing is caring. Spread the word!</p>

      <div>
        <Button
          type="primary"
          htmlType="submit"
          className="social-btn facebook-btn-color"
          style={{ width: '50px' }}
        >
          <Icon className="social-icon" type="facebook" />
        </Button>

        <Divider type="vertical" />

        <Button
          type="primary"
          htmlType="submit"
          className="social-btn twitter-btn-color"
          style={{ width: '50px' }}
        >
          <Icon className="social-icon" type="twitter" />
        </Button>

        <Divider type="vertical" />

        <Button
          type="primary"
          htmlType="submit"
          className="social-btn google-plus-btn-color"
          style={{ width: '50px' }}
        >
          <Icon className="social-icon" type="google-plus" />
        </Button>
      </div>
    </Wrapper>
  );
}

SocialShare.propTypes = {

};

export default SocialShare;
