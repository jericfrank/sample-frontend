/**
*
* JobsTable
*
*/

import _ from 'lodash';
import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Table, Icon, Tooltip } from 'antd';
import Wrapper from './Wrapper';

const row = { title: '', location: '', department: '', employment_type: '' };
const mockJobs = [{ key: 1, ...row }, { key: 2, ...row }, { key: 3, ...row }];

class JobsTable extends React.Component { // eslint-disable-line react/prefer-stateless-function
  renderTitle = (text, { id }) => {
    const applied = _.some(this.props.applied, { job_id: id.toString() });
    return (
      <Link to={`/jobs/${id}`}>
        {text} {applied ? (
          <Tooltip title="Applied">
            <Icon type="check-circle" style={{ color: '#52c41a' }} />
          </Tooltip>
        ) : null}
      </Link>
    );
  };

  render() {
    const { loading, jobs } = this.props;
    return (
      <Wrapper>
        <Table
          loading={loading}
          size="middle"
          columns={[
            { title: 'Job Title', dataIndex: 'title', key: 'title', render: this.renderTitle },
            { title: 'Location', dataIndex: 'location', key: 'location' },
            { title: 'Department', dataIndex: 'department', key: 'department' },
            { title: 'Employment', dataIndex: 'employment_type', key: 'employment_type' },
          ]}
          dataSource={loading ? mockJobs : jobs}
          pagination={{ pageSize: 10 }}
        />
      </Wrapper>
    );
  }
}

JobsTable.propTypes = {
  loading: PropTypes.bool.isRequired,
  jobs: PropTypes.array.isRequired,
  applied: PropTypes.array,
};

export default JobsTable;
