import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Table, Tooltip } from 'antd';
import { shallow } from 'enzyme';

import JobsTable from '../index';

describe('<JobsTable />', () => {
  it('should display jobs into `Table`', () => {
    const wrapper = shallow(<JobsTable loading={false} jobs={[]} />);
    expect(wrapper.find(Table)).toHaveLength(1);
  });

  it('should display `mockJobs` into `Table`', () => {
    const wrapper = shallow(<JobsTable loading jobs={[]} />);
    expect(wrapper.find(Table)).toHaveLength(1);
  });

  it('should display `Tooltip` into `Table`', () => {
    const wrapper = shallow(
      <JobsTable
        loading={false}
        jobs={[{
          key: 4,
          id: 1,
          title: 'Accounting Staff',
          department_id: 5,
          location: 'Cebu City',
          description: 'xxxxx',
          employment_type: 'Full-time',
          closed: 0,
          created_by: 1,
          updated_by: null,
          department: 'Human Resources',
        }]}
        applied={[{ job_id: '1' }]}
      />
    );

    const renderTitle = shallow(
      <BrowserRouter>
        {wrapper.instance().renderTitle('Test', { id: 1 })}
      </BrowserRouter>
    ).shallow();

    expect(renderTitle.find(Tooltip)).toHaveLength(1);
  });

  it('should display NOT `Tooltip` into `Table`', () => {
    const wrapper = shallow(
      <JobsTable
        loading={false}
        jobs={[{
          key: 4,
          id: 1,
          title: 'Accounting Staff',
          department_id: 5,
          location: 'Cebu City',
          description: 'xxxxx',
          employment_type: 'Full-time',
          closed: 0,
          created_by: 1,
          updated_by: null,
          department: 'Human Resources',
        }]}
        applied={[]}
      />
    );

    const renderTitle = shallow(
      <BrowserRouter>
        {wrapper.instance().renderTitle('Test', { id: 1 })}
      </BrowserRouter>
    ).shallow();

    expect(renderTitle.find(Tooltip)).toHaveLength(0);
  });
});
