import styled from 'styled-components';

const Wrapper = styled.div`
  .ant-pagination {
    &.ant-table-pagination {
      margin-right: 20px;
    }
  }
`;

export default Wrapper;
