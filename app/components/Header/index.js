/**
*
* Header
*
*/

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button, Layout, Dropdown, Menu, Icon, Tooltip } from 'antd';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: fixed;
  width: 100%;
  z-index: 4;

  .login-btn {
    margin-right: 20px;

    &:hover {
      text-decoration: underline;
      color: #1890ff;
    }
  }

  .create-account {
    width: 180px;
  }
`;

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  renderNotify = () => {
    const isVerified = parseInt(_.result(this.props.user, 'is_verified', 0), 10);
    return isVerified ? null : (
      <Tooltip placement="bottom" title="Account not verified">
        <Icon
          type="exclamation-circle"
          style={{ color: '#FBAD32', fontSize: '16px' }}
        />
      </Tooltip>
    );
  }

  render() {
    const { hideBtns, user, queryParams } = this.props;

    const menu = (
      <Menu>
        <Menu.Item key="0">
          <Link to="/myapplications">My Applications</Link>
        </Menu.Item>
        <Menu.Item key="1">
          <Link to={`/myprofile${queryParams}`}>My Profile</Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to={`/myprofile/change-password${queryParams}`}>
            Change Password
          </Link>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3"><Link to="/logout">Logout</Link></Menu.Item>
      </Menu>
    );

    return (
      <Wrapper>
        <Layout.Header style={{ backgroundColor: '#217BD0' }}>
          {hideBtns ? null : (
            <div className="pull-right">
              <Link to={`/login${queryParams}`}>
                <Button
                  type="default"
                  ghost
                  className="login-btn"
                >
                  SIGN IN
                </Button>
              </Link>
              <Link to={`/register${queryParams}`}>
                <Button
                  type="primary"
                  className="social-btn create-account"
                >
                  CREATE YOUR ACCOUNT
                </Button>
              </Link>
            </div>
          )}

          <div className="pull-right">
            {user ? (
              <Dropdown
                overlay={menu}
                trigger={['click']}
                placement="bottomRight"
              >
                <span
                  className="text-white cursor-pointer username"
                  style={{ paddingBottom: '10px' }}
                >
                  {this.renderNotify()}&nbsp;
                  {user.first_name && user.last_name ? (
                    <span className="applicant-name">
                      {user.first_name} {user.last_name}
                    </span>
                  ) : (
                    <span className="applicant-email">
                      {user.email}
                    </span>
                  )} <Icon type="down" />
                </span>
              </Dropdown>
            ) : null}
          </div>

          <h1 className="text-white font-champ-bold">
            <Link to="/" className="text-white">Job Board</Link>
          </h1>
        </Layout.Header>
      </Wrapper>
    );
  }
}

Header.propTypes = {
  hideBtns: PropTypes.bool,
  user: PropTypes.object,
  queryParams: PropTypes.string,
};

export default Header;
