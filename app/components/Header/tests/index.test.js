import React from 'react';
import { shallow } from 'enzyme';
import { Layout, Button, Dropdown, Tooltip } from 'antd';

import Header from '../index';

describe('<Header />', () => {
  it('should display `Layout.Header`', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.find(Layout.Header)).toHaveLength(1);
    expect(wrapper.find('.login-btn')).toHaveLength(1);
    expect(wrapper.find(Button)).toHaveLength(2);
  });

  it('should display notify `Tooltip`', () => {
    const user = { is_verified: '0' };
    const wrapper = shallow(<Header hideBtns user={user} />);
    expect(wrapper.find(Tooltip)).toHaveLength(1);
  });

  it('should NOT display notify `Tooltip`', () => {
    const user = { is_verified: '1' };
    const wrapper = shallow(<Header hideBtns user={user} />);
    expect(wrapper.find(Tooltip)).toHaveLength(0);
  });

  it('should hide `Login` and `Register` btns', () => {
    const wrapper = shallow(<Header hideBtns />);
    expect(wrapper.find('.login-btn')).toHaveLength(0);
    expect(wrapper.find(Button)).toHaveLength(0);
  });

  it('should display `Dropdown` if logged in', () => {
    const wrapper = shallow(
      <Header hideBtns user={{ first_name: 'Test', last_name: 'Foo' }} />
    );
    expect(wrapper.find('.login-btn')).toHaveLength(0);
    expect(wrapper.find(Button)).toHaveLength(0);
    expect(wrapper.find(Dropdown)).toHaveLength(1);
  });

  it('should display user`s name if it has a first and last name', () => {
    const wrapper = shallow(
      <Header hideBtns user={{ first_name: 'Test', last_name: 'Foo' }} />
    );
    expect(wrapper.find('.username .applicant-name').first().text())
      .toEqual('Test Foo');
  });

  it('should display user`s email if it has no first and last name', () => {
    const wrapper = shallow(<Header hideBtns user={{ email: 'e@mail.com' }} />);
    expect(wrapper.find('.username .applicant-email').first().text())
      .toEqual('e@mail.com');
  });
});
