import React from 'react';
import { shallow } from 'enzyme';
import { Upload, message } from 'antd';

import ProfileFileUpload from '../index';

describe('<ProfileFileUpload />', () => {
  let props;

  beforeEach(() => {
    class LocalStorageMock {
      constructor() {
        this.store = {};
      }

      clear() {
        this.store = {};
      }

      getItem(key) {
        return this.store[key] || null;
      }

      setItem(key, value) {
        this.store[key] = value.toString();
      }

      removeItem(key) {
        delete this.store[key];
      }
    }
    global.localStorage = new (LocalStorageMock)();

    props = {
      formItemName: 'application_letter',
      onFileRemove: jest.fn(),
      onUploadSuccess: jest.fn(),
      onUploadError: jest.fn(),
    };
    message.success = jest.fn();
    message.error = jest.fn();
  });

  afterEach(() => {
    message.success.mockReset();
    message.success.mockRestore();
    message.error.mockReset();
    message.error.mockRestore();
  });

  it('should render `ProfileFileUpload`', () => {
    const wrapper = shallow(<ProfileFileUpload {...props} />);
    expect(wrapper.find(Upload.Dragger)).toHaveLength(1);
  });

  it('should allow file type and size `beforeUpload`', () => {
    const wrapper = shallow(<ProfileFileUpload {...props} />);
    const file = {
      type: 'application/pdf',
      size: 501,
    };
    expect(wrapper.instance().handleBeforeUpload(file))
      .toEqual(true);
  });

  it('should allow file type and size `beforeUpload`', () => {
    const wrapper = shallow(<ProfileFileUpload {...props} />);
    const file = {
      type: 'image/png',
      size: 654065495049849084,
    };
    expect(wrapper.instance().handleBeforeUpload(file))
      .toEqual(false);
  });

  it('should starts uploading if file status is `uploading`', () => {
    const wrapper = shallow(<ProfileFileUpload {...props} />);
    const info = { file: { status: 'uploading' } };

    wrapper.instance().handleChange(info);
    expect(wrapper.state('fileList')).toEqual([info.file]);
    expect(wrapper.state('uploading')).toEqual(true);
  });

  it('should replace the current `file` in `fileList`', () => {
    const wrapper = shallow(<ProfileFileUpload {...props} />);
    wrapper.setState({ fileList: [{ file: {} }, { file: {} }] });

    const info = { file: { status: 'uploading' } };
    wrapper.instance().handleChange(info);
    expect(wrapper.state('fileList')).toEqual([info.file]);
    expect(wrapper.state('uploading')).toEqual(true);
  });

  it('should invoke `onUploadSuccess` and `message.success` if file status is `done`', () => {
    const wrapper = shallow(<ProfileFileUpload {...props} />);
    wrapper.setState({ uploading: true });

    const info = { file: { status: 'done' } };
    wrapper.instance().handleChange(info);
    expect(props.onUploadSuccess).toHaveBeenCalledTimes(1);
    expect(message.success).toHaveBeenCalledTimes(1);
    expect(wrapper.state('uploading')).toEqual(false);
  });

  it('should invoke `onUploadError` and `message.success` if file status is `error`', () => {
    const wrapper = shallow(<ProfileFileUpload {...props} />);
    wrapper.setState({ uploading: true });

    const info = { file: { status: 'error' } };
    wrapper.instance().handleChange(info);
    expect(props.onUploadError).toHaveBeenCalledTimes(1);
    expect(message.error).toHaveBeenCalledTimes(1);
    expect(wrapper.state('uploading')).toEqual(false);
  });

  it('should change `uploading` state if file status is `pending`', () => {
    const wrapper = shallow(<ProfileFileUpload {...props} />);
    wrapper.setState({ uploading: true });

    const info = { file: { status: 'pending' } };
    wrapper.instance().handleChange(info);
    expect(wrapper.state('uploading')).toEqual(false);
  });

  it('should invoke `onFileRemove` when `handleRemove` is triggered', () => {
    const wrapper = shallow(<ProfileFileUpload {...props} />);
    wrapper.instance().handleRemove(null);
    expect(props.onFileRemove).toHaveBeenCalledTimes(1);
  });
});
