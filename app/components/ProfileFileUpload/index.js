/**
*
* ProfileFileUpload
*
*/

import _ from 'lodash';
import axios from 'axios';
import React from 'react';
import PropTypes from 'prop-types';
import { Upload, Button, Icon, message, Spin } from 'antd';

import { FILE_TYPES } from './constants';

class ProfileFileUpload extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    fileList: [],
    fileTypes: FILE_TYPES,
    uploading: false,
    uploadProps: {
      name: 'document',
      accept: _.join(FILE_TYPES, ','),
      action: `${axios.defaults.baseURL}/applicants/upload`,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('jobportalToken')}`,
        'X-Requested-With': null,
      },
    },
  }

  handleBeforeUpload = (file) => {
    const { fileTypes } = this.state;
    const isFileTypeAllowed = _.some(fileTypes, (type) => file.type === type);
    if (!isFileTypeAllowed) {
      message.error('You can only upload PDF and MSWord files!');
    }

    const isLt4MB = file.size / 1024 / 1024 < 4;
    if (!isLt4MB) {
      message.error('File must be smaller than 4MB!');
    }
    return isLt4MB && isFileTypeAllowed;
  }

  handleChange = (info) => {
    if (this.state.fileList.length > 1) {
      this.setState({ fileList: [] });
    }
    this.setState({ fileList: [info.file] });

    if (info.file.status === 'uploading') {
      this.setState({ uploading: true });
      return;
    }

    if (info.file.status === 'done') {
      this.props.onUploadSuccess(info.file, this.props.formItemName, false);
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      this.props.onUploadError(info.file, this.props.formItemName, true);
      message.error(`${info.file.name} file upload failed. Please try again.`);
    }

    this.setState({ uploading: false });
  }

  handleRemove = (file) => {
    this.setState({ fileList: [] });
    this.props.onFileRemove(file, this.props.formItemName, false);
  }

  render() {
    const fileList = this.state.fileList.length ? (
      this.state.fileList
    ) : this.props.defaultFileList;

    return (
      <Spin spinning={this.state.uploading}>
        <Upload.Dragger
          {...this.state.uploadProps}
          multiple={false}
          beforeUpload={this.handleBeforeUpload}
          onChange={this.handleChange}
          onRemove={this.handleRemove}
          fileList={fileList}
        >
          <p className="ant-upload-drag-icon no-margin">
            <Icon type="cloud-upload-o" />
          </p>
          <p className="ant-upload-text no-margin">
            Drop your file here
          </p>
          <p
            className="ant-upload-hint"
            style={{ marginBottom: '4px' }}
          >
            or
          </p>
          <Button ghost type="primary" style={{ marginBottom: '10px' }}>
            Choose a file
          </Button>
        </Upload.Dragger>
      </Spin>
    );
  }
}

ProfileFileUpload.propTypes = {
  formItemName: PropTypes.string.isRequired,
  onFileRemove: PropTypes.func,
  onUploadSuccess: PropTypes.func.isRequired,
  onUploadError: PropTypes.func,
  defaultFileList: PropTypes.array,
};

export default ProfileFileUpload;
