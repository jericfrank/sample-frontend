import React from 'react';
import { shallow } from 'enzyme';
import { ParallaxBanner } from 'react-scroll-parallax';

import Banner from '../index';

describe('<Banner />', () => {
  let props;

  beforeEach(() => {
    props = {
      requestProviderUrl: jest.fn(),
      hideSignUpBtns: false,
    };
  });

  it('should display `ParallaxBanner`', () => {
    const wrapper = shallow(<Banner {...props} />);
    expect(wrapper.find(ParallaxBanner)).toHaveLength(1);
  });
});
