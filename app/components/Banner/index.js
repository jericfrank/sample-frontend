/**
*
* Banner
*
*/

import React from 'react';
import { ParallaxBanner } from 'react-scroll-parallax';
import { Icon } from 'antd';
import { Link } from 'react-scroll';
import styled from 'styled-components';
import BG_IMAGE from 'images/bg-blackprint.jpg';

const Wrapper = styled.div`
  .parallax-banner-layer-0,
  .parallax-banner-layer-1,
  .parallax-banner-layer-2 {
    position: relative;
    background: #2488e6;
    overflow: hidden;

    &:before {
      content: ' ';
      display: block;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      z-index: 1;
      opacity: 0.1;
      background-image: url(${BG_IMAGE});
      background-repeat: repeat-y !important;
    }
  }

  .banner-container {
    position: relative,
    z-index: 2;
    height: 100%;
    border: 1px solid orange;

    .banner-wrapper {
      position: relative;
      float: left;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);

      .banner-header {
        font-size: 60px;
        margin-bottom: 10px;
        line-height: 60px;

        &:first-child {
          font-size: 36px;
          line-height: 30px;
        }
      }

      .banner-desc {
        font-size: 24px;
      }

      .social-btn {
        width: 160px;
        margin: 5px;
      }
    }
  }

  .btnscroll-wrapper {
    position: relative;

    .btnscroll-text-container {
      font-size: 18px;
      position: absolute;
      bottom: 0;
      left: 0;
      width: 100%;
      margin: 15px 0px;

      a:hover, a:active, a:focus {
        color: white;
        text-decoration: none;
      }

      .anticon-down {
        font-size: 25px;
      }
    }
  }
`;

function Banner() {
  return (
    <Wrapper>
      <ParallaxBanner
        layers={[{
          image: '#2488e6',
          amount: 0.6,
          slowerScrollRate: false,
        }]}
        style={{
          height: '100vh',
        }}
      >
        <div className="banner-container">
          <div className="banner-wrapper text-center">
            <div className="banner-header text-white font-champ-bold">
              Join our Team
            </div>
            <h1 className="banner-header text-white font-champ-bold">
              We&apos;re Hiring!
            </h1>
            <h2 className="banner-desc text-white font-champ-bold">
              Excellent opportunity for you to be part of
              a growing company
            </h2>
          </div>
        </div>
        <div className="btnscroll-wrapper text-white text-center font-champ-bold">
          <div className="btnscroll-text-container">
            <Link
              activeClass="active"
              to="job-openings"
              spy
              smooth
              duration={1000}
            >
              <span className="text-white cursor-pointer">
                Job Openings
                <div>
                  <Icon type="down" />
                </div>
              </span>
            </Link>
          </div>
        </div>
      </ParallaxBanner>
    </Wrapper>
  );
}

Banner.propTypes = {
};

export default Banner;
