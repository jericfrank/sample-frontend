import React from 'react';
import { shallow } from 'enzyme';
import { Modal } from 'antd';

import PrivacyPolicy from '../index';

describe('<PrivacyPolicy />', () => {
  it('should render component', () => {
    const wrapper = shallow(<PrivacyPolicy />);
    expect(wrapper.find(Modal)).toHaveLength(1);
  });

  it('should setState `visible` to false on `handleCancel`', () => {
    const wrapper = shallow(<PrivacyPolicy />);
    wrapper.instance().handleCancel({ preventDefault: jest.fn() });
    expect(wrapper.state().visible).toEqual(false);
  });

  it('should setState `visible` to true on `showModal`', () => {
    const wrapper = shallow(<PrivacyPolicy />);
    wrapper.instance().showModal();
    expect(wrapper.state().visible).toEqual(true);
  });
});
