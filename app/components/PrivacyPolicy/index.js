/**
*
* PrivacyPolicy
*
*/

import _ from 'lodash';
import React from 'react';
import { Link } from 'react-router-dom';
import { Modal } from 'antd';

import { POLICY_DESCRIPTION } from './constants';

class PrivacyPolicy extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = { visible: false }

  showModal = () => {
    this.setState({
      visible: true,
    });
  }

  handleCancel = (e) => {
    e.preventDefault();
    this.setState({
      visible: false,
    });
  }

  render() {
    return (
      <div>
        <div className="pull-left">
          By clicking {'"Register"'} I agree to <Link onClick={this.showModal} to="#" className="bordered-link">Tritontek Inc. Privacy Policy</Link>
        </div>
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          width={920}
          footer={null}
        >
          <div dangerouslySetInnerHTML={{ __html: _.unescape(POLICY_DESCRIPTION) }} />
        </Modal>
      </div>
    );
  }
}

PrivacyPolicy.propTypes = {

};

export default PrivacyPolicy;
