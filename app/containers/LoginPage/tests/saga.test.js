/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { take, put, takeLatest, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';

import { CHECK_USER_AUTH } from 'containers/App/constants';
import { setUserAuth, userAuthErr } from 'containers/App/actions';

import watchUserLogin, { checkUserCreds } from '../saga';

/* eslint-disable redux-saga/yield-effects */
describe('checkUserCreds', () => {
  let checkUserCredsGenerator;
  let creds;
  let response;

  beforeEach(() => {
    class LocalStorageMock {
      constructor() {
        this.store = {};
      }

      clear() {
        this.store = {};
      }

      getItem(key) {
        return this.store[key] || null;
      }

      setItem(key, value) {
        this.store[key] = value.toString();
      }

      removeItem(key) {
        delete this.store[key];
      }
    }
    global.localStorage = new (LocalStorageMock)();

    creds = {
      email: 'email@email.com',
      password: 'myPassword',
    };
    response = {
      applicant: {
        token: 123,
        details: {
          id: 1,
          active: 1,
        },
      },
    };
    checkUserCredsGenerator = checkUserCreds(creds);

    const callDescriptor = checkUserCredsGenerator.next(response).value;
    expect(callDescriptor).toMatchSnapshot();

    const jwtTokenCallDescriptor = checkUserCredsGenerator.next(response).value;
    expect(jwtTokenCallDescriptor).toMatchSnapshot();

    const callReqDescriptor = checkUserCredsGenerator.next(response).value;
    expect(callReqDescriptor).toMatchSnapshot();

    const encryptCallDescriptor = checkUserCredsGenerator.next(response).value;
    expect(encryptCallDescriptor).toMatchSnapshot();
  });

  it('should encrypt user details using token as CryptoJS-token', () => {
    const putDescriptor = checkUserCredsGenerator.next(response).value;
    const applications = {
      applicant: {
        details: {
          id: 1,
          active: 1,
        },
        token: 123,
      },
    };
    expect(putDescriptor).toEqual(put(setUserAuth({ ...response.applicant.details, applications })));
  });

  it('should call `message.error` to infom the user of the error', () => {
    const err = { error: 'Something went wrong.' };
    const callDescriptor = checkUserCredsGenerator.throw(err).value;
    expect(callDescriptor).toMatchSnapshot();
  });

  it('should call the userAuthErr action if the response errors', () => {
    const err = { error: 'Something went wrong.' };
    checkUserCredsGenerator.throw(err);
    const putDescriptor = checkUserCredsGenerator.next().value;
    expect(putDescriptor).toEqual(put(userAuthErr()));
  });
});

describe('checkUserCreds', () => {
  let checkUserCredsGenerator;
  let creds;
  let response;

  beforeEach(() => {
    class LocalStorageMock {
      constructor() {
        this.store = {};
      }

      clear() {
        this.store = {};
      }

      getItem(key) {
        return this.store[key] || null;
      }

      setItem(key, value) {
        this.store[key] = value.toString();
      }

      removeItem(key) {
        delete this.store[key];
      }
    }
    global.localStorage = new (LocalStorageMock)();

    creds = {
      email: 'email@email.com',
      password: 'myPassword',
    };
    response = {
      applicant: {
        token: 123,
        details: {
          id: 1,
          active: 0,
        },
      },
    };
    checkUserCredsGenerator = checkUserCreds(creds);

    const callDescriptor = checkUserCredsGenerator.next(response).value;
    expect(callDescriptor).toMatchSnapshot();

    const jwtTokenCallDescriptor = checkUserCredsGenerator.next(response).value;
    expect(jwtTokenCallDescriptor).toMatchSnapshot();

    const callReqDescriptor = checkUserCredsGenerator.next(response).value;
    expect(callReqDescriptor).toMatchSnapshot();

    const encryptCallDescriptor = checkUserCredsGenerator.next(response).value;
    expect(encryptCallDescriptor).toMatchSnapshot();

    const messageCallDescriptor = checkUserCredsGenerator.next(response).value;
    expect(messageCallDescriptor).toMatchSnapshot();
  });

  it('should encrypt user details using token as CryptoJS-token', () => {
    const putDescriptor = checkUserCredsGenerator.next(response).value;
    const applications = {
      applicant: {
        token: 123,
        details: {
          id: 1,
          active: 0,
        },
      },
    };
    expect(putDescriptor).toEqual(put(setUserAuth({ ...response.applicant.details, applications })));
  });
});

describe('watchUserLogin Saga', () => {
  const generator = watchUserLogin();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const expectedYield = takeLatest(CHECK_USER_AUTH, checkUserCreds);
    expect(generator.next().value).toEqual(expectedYield);
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
