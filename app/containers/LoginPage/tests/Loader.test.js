import React from 'react';
import { shallow } from 'enzyme';

import SpinLoader from 'components/SpinLoader';
import Loader from '../Loader';

describe('Loader', () => {
  it('should render `SpinLoader`', () => {
    const wrapper = shallow(<Loader />);
    expect(wrapper.find(SpinLoader)).toHaveLength(1);
  });
});
