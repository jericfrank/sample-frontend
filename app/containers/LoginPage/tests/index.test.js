import React from 'react';
import { shallow } from 'enzyme';
import { Redirect } from 'react-router-dom';
import { Flipper, Front, Back } from 'react-flipper';

import { checkUserAuth } from 'containers/App/actions';
import LoginForm from 'components/LoginForm';
import ForgotPassForm from 'containers/ForgotPassForm';
import { LoginPage, mapDispatchToProps } from '../index';

describe('<LoginPage />', () => {
  let props;

  beforeEach(() => {
    props = {
      handleLogin: jest.fn(),
      authenticating: false,
      authenticated: false,
      location: {
        search: '',
      },
    };
  });

  it('should render `LoginPage`', () => {
    const wrapper = shallow(<LoginPage {...props} />);
    expect(wrapper.find(Flipper)).toHaveLength(1);
    expect(wrapper.find(Front)).toHaveLength(1);
    expect(wrapper.find(Back)).toHaveLength(1);
    expect(wrapper.find(LoginForm)).toHaveLength(1);
    expect(wrapper.find(ForgotPassForm)).toHaveLength(0);
  });

  it('should change `isFlipped` state value', () => {
    const wrapper = shallow(<LoginPage {...props} />);
    wrapper.instance().flip();
    expect(wrapper.state('isFlipped')).toBeTruthy();
  });

  it('should render `react-router-dom` `Redirect`', () => {
    props = { ...props, authenticated: true };
    const wrapper = shallow(<LoginPage {...props} />);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

  describe('mapDispatchToProps', () => {
    describe('handleLogin', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.handleLogin).toBeDefined();
      });

      it('should dispatch checkUserAuth when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const creds = {
          email: 'email@email.com',
          password: 'myPassword',
        };
        result.handleLogin(creds);
        expect(dispatch).toHaveBeenCalledWith(checkUserAuth(creds));
      });
    });
  });
});
