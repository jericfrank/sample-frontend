/**
 *
 * LoginPage
 *
 */

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Flipper, Front, Back } from 'react-flipper';

import injectSaga from 'utils/injectSaga';
import urlParams from 'utils/urlParams';

import {
  makeSelectAuthenticated,
  makeSelectAuthenticating,
} from 'containers/App/selectors';
import { checkUserAuth } from 'containers/App/actions';
import ForgotPassForm from 'containers/ForgotPassForm';
import LoginForm from 'components/LoginForm';

import saga from './saga';

export class LoginPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    isFlipped: false,
  }

  flip = () => {
    this.setState({ isFlipped: !this.state.isFlipped });
  }

  render() {
    const { handleLogin, authenticating } = this.props;
    const urlParamsObj = urlParams(this.props.location.search);
    const redirectRoute = _.result(urlParamsObj, 'redirect', '/');

    if (this.props.authenticated) {
      return <Redirect to={decodeURIComponent(redirectRoute)} />;
    }

    return (
      <div>
        <Helmet>
          <title>Login</title>
          <meta name="description" content="Description of Login" />
        </Helmet>

        <Flipper
          isFlipped={this.state.isFlipped}
          orientation="horizontal"
          style={{ width: '100%' }}
        >
          <Front>
            <LoginForm
              onLogin={handleLogin}
              authenticating={authenticating}
              onForgotPassClick={this.flip}
              queryParams={this.props.location.search}
            />
          </Front>

          <Back>
            {this.state.isFlipped ? (
              <ForgotPassForm
                onLogin={handleLogin}
                authenticating={authenticating}
                onRedirectToLogin={this.flip}
              />
            ) : <div>&nbsp;</div>}
          </Back>
        </Flipper>
      </div>
    );
  }
}

LoginPage.propTypes = {
  location: PropTypes.object.isRequired,
  handleLogin: PropTypes.func.isRequired,
  authenticating: PropTypes.bool.isRequired,
  authenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  authenticating: makeSelectAuthenticating(),
  authenticated: makeSelectAuthenticated(),
});

export function mapDispatchToProps(dispatch) {
  return {
    handleLogin: (creds) => dispatch(checkUserAuth(creds)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({ key: 'loginPage', saga });

export default compose(
  withSaga,
  withConnect,
)(LoginPage);
