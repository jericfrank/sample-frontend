import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  .flipper-container {
    width: 100%;
  }

  .login-container {
    color: #464648;

    .login-content {
      position: absolute;
      width: 100%;
      padding: 0px 50px;
      margin-top: 200px;
      border-radius: 5px;

      h1 {
        font-size: 50px;
        line-height: 40px;
        color: #464648;
      }

      p {
        margin: 50px 0px 30px 0px;
        font-size: 16px;
        color: #464648;
      }

      .flipper-btn {
        color: #464648;
      }

      .social-btns-label {
        font-size: 14px;
        margin: 15px 0px;

        span {
          color: rgba(0, 0, 0, 0.85);
        }
      }

      .loginpage-form {
        .social-btn {
          width: 150px;
          margin: 5px 10px;
        }
      }
    }
  }
`;
