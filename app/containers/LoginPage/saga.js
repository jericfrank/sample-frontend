import _ from 'lodash';
import CryptoJS from 'crypto-js';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { postRequest, getRequest } from 'utils/request';
import { handleJwtToken } from 'utils/jwtToken';
import { message } from 'antd';

import { CHECK_USER_AUTH } from 'containers/App/constants';
import { setUserAuth, userAuthErr } from 'containers/App/actions';

export function* checkUserCreds({ email, password }) {
  try {
    const { applicant } = yield call(postRequest, '/hr/applicants/login', {
      email,
      password,
    });

    const { details, token } = applicant;

    yield call(handleJwtToken, token);
    const applications = yield call(getRequest, '/hr/applicants/job_applications');

    const userToken = { ...details, applications };

    const encrypt = yield call(CryptoJS.AES.encrypt, JSON.stringify(userToken), token);
    localStorage.setItem('jobportalUser', encrypt);

    if (!parseInt(userToken.active, 10)) {
      yield call(message.success, 'Your account has been automatically activated.');
    }
    yield put(setUserAuth(userToken));
  } catch (err) {
    yield call(message.error, _.result(err, 'error', 'Something went wrong. Please try again later.'));
    yield put(userAuthErr());
  }
}

// Individual exports for testing
export default function* watchUserLogin() {
  const watcher = yield takeLatest(CHECK_USER_AUTH, checkUserCreds);

  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
