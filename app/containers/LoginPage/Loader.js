import React from 'react';
import styled from 'styled-components';

import SpinLoader from 'components/SpinLoader';

const Wrapper = styled.div`
  .loader {
    position: absolute;
    left: 50%;
    top: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
  }
`;

function Loader() {
  return (
    <Wrapper>
      <div className="loader">
        <SpinLoader />
      </div>
    </Wrapper>
  );
}

export default Loader;
