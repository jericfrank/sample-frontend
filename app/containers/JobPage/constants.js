/*
 *
 * JobPage constants
 *
 */

export const FETCH_JOB_DETAILS = 'app/JobPage/FETCH_JOB_DETAILS';
export const JOB_DETAILS_FETCHED = 'app/JobPage/JOB_DETAILS_FETCHED';
export const JOB_DETAILS_FETCH_ERR = 'app/JobPage/JOB_DETAILS_FETCH_ERR';
