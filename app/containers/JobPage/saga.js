import _ from 'lodash';
import { take, call, put, takeLatest, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { getRequest } from 'utils/request';

import { FETCH_JOB_DETAILS } from './constants';
import { fetchedJobDetails, displayFetchErr } from './actions';

export function* fetchJobDetails({ id }) {
  try {
    const response = yield call(getRequest, `/hr/applicants/job_openings/${id}`);
    let questions = _.chunk(_.remove(response.job_question, (q, index) => index !== 0), 5);
    const defaultQuestion = _.pullAt(response.job_question, 0);
    questions = [...questions, defaultQuestion];
    yield put(fetchedJobDetails({ ...response, job_question: questions }));
  } catch (err) {
    yield put(displayFetchErr(err));
  }
}

export default function* watchFetchJobOpenings() {
  const watcher = yield takeLatest(FETCH_JOB_DETAILS, fetchJobDetails);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
