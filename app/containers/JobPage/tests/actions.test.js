
import {
  fetchJobDetails,
  fetchedJobDetails,
  displayFetchErr,
} from '../actions';
import {
  FETCH_JOB_DETAILS,
  JOB_DETAILS_FETCHED,
  JOB_DETAILS_FETCH_ERR,
} from '../constants';

describe('JobPage actions', () => {
  describe('fetchJobDetails Action', () => {
    it('has a type of FETCH_JOB_DETAILS', () => {
      const expected = {
        type: FETCH_JOB_DETAILS,
        id: 1,
      };
      expect(fetchJobDetails(1)).toEqual(expected);
    });
  });

  describe('fetchedJobDetails Action', () => {
    it('has a type of JOB_DETAILS_FETCHED', () => {
      const expected = {
        type: JOB_DETAILS_FETCHED,
        jobDetails: {},
      };
      expect(fetchedJobDetails({})).toEqual(expected);
    });
  });

  describe('displayFetchErr Action', () => {
    it('has a type of JOB_DETAILS_FETCH_ERR', () => {
      const expected = {
        type: JOB_DETAILS_FETCH_ERR,
        errMsg: 'error',
      };
      expect(displayFetchErr('error')).toEqual(expected);
    });
  });
});
