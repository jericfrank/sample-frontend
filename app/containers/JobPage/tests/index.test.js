import React from 'react';
import { shallow } from 'enzyme';
import { Button } from 'antd';

import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { JobPage, mapDispatchToProps } from '../index';
import { fetchJobDetails } from '../actions';

describe('<JobPage />', () => {
  let props;
  beforeEach(() => {
    props = {
      match: {
        params: {
          id: 1,
        },
      },
      location: {
        search: '',
      },
      getJobDetails: jest.fn(),
      jobpage: {
        fetchErr: false,
        fetched: true,
        jobDetails: {
          id: 1,
          title: 'test',
          description: '&lt;p&gt;&lt;b&gt;TRITONTEK &lt;/b&gt;',
          location: 'Cebu City',
          employment_type: 'Full-time',
          division: {
            description: 'Human Resources',
          },
        },
      },
      history: {
        goBack: jest.fn(),
      },
      currentUser: {
        applications: [],
      },
    };
  });

  it('should contain `NotFoundPage` component', () => {
    props.jobpage.fetchErr = true;
    const wrapper = shallow(<JobPage {...props} />);
    expect(wrapper.find(NotFoundPage)).toHaveLength(1);
  });

  it('should contain `renderApplied` component', () => {
    props = {
      ...props,
      currentUser: {
        applications: [{
          job_id: '1',
        }],
      },
    };
    const wrapper = shallow(<JobPage {...props} />);
    expect(wrapper.find(Button)).toHaveLength(1);
  });

  it('should contain `loading` component', () => {
    const wrapper = shallow(<JobPage {...props} />);
    wrapper.setProps({ jobpage: { ...props.jobpage, fetched: false } });
    expect(wrapper.instance().props.jobpage.fetched).toEqual(false);
  });

  it('should handle `handleBack`', () => {
    const wrapper = shallow(<JobPage {...props} />);
    wrapper.instance().handleBack();
    expect(props.history.goBack).toHaveBeenCalledTimes(1);
  });

  it('should handle `componentDidMount`', () => {
    const wrapper = shallow(<JobPage {...props} />);
    wrapper.instance().componentDidMount();
    expect(wrapper.instance().props.jobpage.jobDetails).toEqual(props.jobpage.jobDetails);
  });

  it('should render `Apply` button', () => {
    props = {
      ...props,
      currentUser: undefined,
    };
    const wrapper = shallow(<JobPage {...props} />);
    expect(wrapper.find('.apply-btn')).toHaveLength(1);
  });

  it('should `renderBackButton` btn that links to `/myapplications`', () => {
    props = {
      ...props,
      location: {
        search: '?redirect=%2Fmyapplications',
      },
    };
    const wrapper = shallow(<JobPage {...props} />);
    expect(wrapper.find('[to="/myapplications"]')).toHaveLength(1);
  });

  describe('mapDispatchToProps', () => {
    describe('getJobDetails', () => {
      it('should dispatch fetchJobDetails when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.getJobDetails(1);
        expect(dispatch).toHaveBeenCalledWith(fetchJobDetails(1));
      });
    });
  });
});
