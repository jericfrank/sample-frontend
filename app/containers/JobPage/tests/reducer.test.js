import { fromJS } from 'immutable';
import jobPageReducer from '../reducer';

import {
  fetchJobDetails,
  fetchedJobDetails,
  displayFetchErr,
} from '../actions';

describe('jobPageReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({
      jobDetails: {},
      fetching: false,
      fetched: false,
      fetchErr: false,
      errMsg: '',
    });
  });

  it('returns the initial state', () => {
    expect(jobPageReducer(undefined, {})).toEqual(state);
  });

  it('should handle the fetchJobDetails action correctly', () => {
    const expectedResult = state
        .set('jobDetails', {})
        .set('fetching', true)
        .set('fetched', false)
        .set('errMsg', '')
        .set('fetchErr', false);
    expect(jobPageReducer(state, fetchJobDetails({}))).toEqual(expectedResult);
  });

  it('should handle the fetchedJobDetails action correctly', () => {
    const expectedResult = state
        .set('jobDetails', {})
        .set('fetching', false)
        .set('fetched', true)
        .set('errMsg', '')
        .set('fetchErr', false);
    expect(jobPageReducer(state, fetchedJobDetails({}))).toEqual(expectedResult);
  });

  it('should handle the displayFetchErr action correctly', () => {
    const expectedResult = state
        .set('jobDetails', {})
        .set('fetching', false)
        .set('fetched', false)
        .set('errMsg', 'error')
        .set('fetchErr', true);
    expect(jobPageReducer(state, displayFetchErr('error'))).toEqual(expectedResult);
  });
});
