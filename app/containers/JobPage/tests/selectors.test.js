import { fromJS } from 'immutable';
import { selectJobPageDomain, makeSelectJobPage } from '../selectors';

describe('selectJobPageDomain', () => {
  it('should select `jobPage` state', () => {
    const jobPage = fromJS({});
    const mockedState = fromJS({
      jobPage,
    });
    expect(selectJobPageDomain(mockedState))
      .toEqual(jobPage);
  });
});

describe('makeSelectJobPage', () => {
  it('should select `jobPage` state', () => {
    const state = {};
    const jobPage = fromJS(state);
    const mockedState = fromJS({
      jobPage,
    });
    expect(makeSelectJobPage()(mockedState))
      .toEqual(state);
  });
});
