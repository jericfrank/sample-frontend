import { put, take, takeLatest, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';
import { FETCH_JOB_DETAILS } from '../constants';
import watchFetchJobOpenings, { fetchJobDetails } from '../saga';
import { fetchedJobDetails, displayFetchErr } from '../actions';

/* eslint-disable redux-saga/yield-effects */
describe('fetchJobDetails Saga', () => {
  let createGenerator;

  const question = { value: 'test' };
  const action = {
    id: 1,
    job_question: [question, question],
  };

  beforeEach(() => {
    createGenerator = fetchJobDetails(action);
    const requestDescriptor = createGenerator.next().value;
    expect(requestDescriptor).toMatchSnapshot();
  });

  it('should call getItemsSucceeded action if requests successfully', () => {
    const response = { job_question: [question, question] };
    const descriptor = createGenerator.next(response).value;
    expect(descriptor).toEqual(put(fetchedJobDetails({
      ...response,
      job_question: [[question], [question]],
    })));
  });

  it('should call the displayFetchErr action if the response errors', () => {
    const response = new Error('Some error');
    const descriptor = createGenerator.throw(response).value;
    expect(descriptor).toEqual(put(displayFetchErr(response)));
  });
});

describe('watchFetchJobOpenings Saga', () => {
  const generator = watchFetchJobOpenings();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const expectedYield = takeLatest(FETCH_JOB_DETAILS, fetchJobDetails);
    expect(generator.next().value).toEqual(expectedYield);
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
