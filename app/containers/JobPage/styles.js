import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
    .subtitle {
        font-size: 16px;
        color: #9e9e9e;
        margin-bottom: 0;
    }
`;
