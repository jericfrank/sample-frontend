/*
 *
 * JobPage actions
 *
 */

import {
  FETCH_JOB_DETAILS,
  JOB_DETAILS_FETCHED,
  JOB_DETAILS_FETCH_ERR,
} from './constants';

export function fetchJobDetails(id) {
  return {
    type: FETCH_JOB_DETAILS,
    id,
  };
}

export function fetchedJobDetails(jobDetails) {
  return {
    type: JOB_DETAILS_FETCHED,
    jobDetails,
  };
}

export function displayFetchErr(errMsg) {
  return {
    type: JOB_DETAILS_FETCH_ERR,
    errMsg,
  };
}
