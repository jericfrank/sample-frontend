/*
 *
 * JobPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  FETCH_JOB_DETAILS,
  JOB_DETAILS_FETCHED,
  JOB_DETAILS_FETCH_ERR,
} from './constants';

const initialState = fromJS({
  jobDetails: {},
  fetching: false,
  fetched: false,
  fetchErr: false,
  errMsg: '',
});

function jobPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_JOB_DETAILS:
      return state
        .set('jobDetails', {})
        .set('fetching', true)
        .set('fetched', false)
        .set('errMsg', '')
        .set('fetchErr', false);

    case JOB_DETAILS_FETCHED:
      return state
        .set('jobDetails', action.jobDetails)
        .set('fetching', false)
        .set('fetched', true)
        .set('errMsg', '')
        .set('fetchErr', false);

    case JOB_DETAILS_FETCH_ERR:
      return state
        .set('jobDetails', {})
        .set('fetching', false)
        .set('fetched', false)
        .set('errMsg', action.errMsg)
        .set('fetchErr', true);

    default:
      return state;
  }
}

export default jobPageReducer;
