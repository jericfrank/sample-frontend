/**
 *
 * JobPage
 *
 */

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Row, Col, Card, Divider, Button } from 'antd';
import sanitizeHtml from 'sanitize-html';
import FontAwesome from 'react-fontawesome';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import urlParams from 'utils/urlParams';
import { makeSelectUser } from 'containers/App/selectors';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import makeSelectJobPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import { fetchJobDetails } from './actions';
import './styles';

export class JobPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.getJobDetails(this.props.match.params.id);
  }

  handleBack = () => {
    this.props.history.goBack();
  };

  renderApply = () => {
    const route = `/jobs/${this.props.match.params.id}/apply`;
    return (
      <Link to={route}>
        <Button
          style={{ marginTop: '10px' }}
          type="primary"
          size="large"
          className="apply-btn"
        >
          Apply now
        </Button>
      </Link>
    );
  };

  renderApplied() {
    return (
      <Button
        style={{ marginTop: '10px', color: '#52c41a' }}
        size="large"
        type="dashed"
        icon="check-circle"
        className="applied-btn"
      >
        Applied
      </Button>
    );
  }

  renderBackButton = () => {
    const urlParamsObj = urlParams(this.props.location.search);
    const redirectRoute = _.result(urlParamsObj, 'redirect', '/');

    return (
      <Link to={decodeURIComponent(redirectRoute)} className="back-button">
        <FontAwesome name="angle-left" /> Back
      </Link>
    );
  }

  render() {
    const { id } = this.props.match.params;
    const { fetched, fetchErr, jobDetails } = this.props.jobpage;

    const applied = _.some(this.props.currentUser ? this.props.currentUser.applications : [], { job_id: id.toString() });

    if (fetchErr) {
      return <NotFoundPage />;
    }

    const html = sanitizeHtml(_.unescape(jobDetails.description), {
      allowedTags: ['b', 'i', 'em', 'strong', 'a', 'p', 'ul', 'li', 'div'],
      allowedAttributes: {
        a: ['href'],
      },
    });

    return (
      <div className="content-wrapper with-bg">
        <Helmet>
          <title>{jobDetails.title}</title>
          <meta name="jobtitle" content={jobDetails.title} />
        </Helmet>
        <Row>
          <Col xs={1} sm={1} md={1} lg={2} xl={6}></Col>
          <Col xs={22} sm={22} md={22} lg={20} xl={12} style={{ margin: 'auto', marginTop: '20px' }}>

            <Row>
              <Col span={2}>
                {this.renderBackButton()}
              </Col>
              <Col span={20}>
                <h1 className="text-center font-champ-bold dark-navy-blue">
                  Job Description
                </h1>
              </Col>
              <Col span={2}></Col>
            </Row>

            {fetched ? (
              <div>
                <Card
                  title={(
                    <div className="text-center">
                      <h3>{jobDetails.title}</h3>
                      <span className="subtitle">
                        <FontAwesome name="building" /> {jobDetails.division.description} <Divider type="vertical" />
                        <FontAwesome name="map-pin" /> {jobDetails.location} <Divider type="vertical" />
                        <FontAwesome name="clock-o" /> {jobDetails.employment_type}
                      </span>
                      <br />
                      {applied ? this.renderApplied() : this.renderApply()}
                    </div>
                  )}
                  style={{ marginBottom: '20px' }}
                >
                  <div dangerouslySetInnerHTML={{ __html: html }} />
                  <div className="pull-right">{applied ? this.renderApplied() : this.renderApply()}</div>
                  <div className="clear"></div>
                </Card>
              </div>
            ) : (
              <Card loading />
            )}

          </Col>
          <Col xs={1} sm={1} md={1} lg={2} xl={6}></Col>
        </Row>
      </div>
    );
  }
}

JobPage.propTypes = {
  match: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  getJobDetails: PropTypes.func.isRequired,
  jobpage: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  currentUser: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  jobpage: makeSelectJobPage(),
  currentUser: makeSelectUser(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getJobDetails: (id) => dispatch(fetchJobDetails(id)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'jobPage', reducer });
const withSaga = injectSaga({ key: 'jobPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(JobPage);
