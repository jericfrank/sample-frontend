/**
 * Tests for SignUpPage sagas
 */

import { put, takeLatest, take, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';

import { setUserAuth } from 'containers/App/actions';
import { SIGNUP_USER } from '../constants';
import watchSignupUser, { signupUser } from '../saga';
import { signUpUserSuccess, signUpUserError } from '../actions';

/* eslint-disable redux-saga/yield-effects */
describe('signupUser Saga', () => {
  let signupUserGenerator;
  let payload;
  let response;
  let encrypt;

  beforeEach(() => {
    class LocalStorageMock {
      constructor() {
        this.store = {};
      }

      clear() {
        this.store = {};
      }

      getItem(key) {
        return this.store[key] || null;
      }

      setItem(key, value) {
        this.store[key] = value.toString();
      }

      removeItem(key) {
        delete this.store[key];
      }
    }
    global.localStorage = new (LocalStorageMock)();

    payload = {
      user: {
        email: 'email@email.com',
        password: 'myPassword',
        confirmPassword: 'myPassword',
      },
    };
    response = {
      applicant: {
        details: {},
        token: 'Token goes here',
      },
    };
    encrypt = 'Encrypted the flipping out of the user details';

    signupUserGenerator = signupUser(payload);

    const registerDescriptor = signupUserGenerator.next(response).value;
    expect(registerDescriptor).toMatchSnapshot();

    const jwtCallDescriptor = signupUserGenerator.next(response).value;
    expect(jwtCallDescriptor).toMatchSnapshot();

    const messageDescriptor = signupUserGenerator.next().value;
    expect(messageDescriptor).toMatchSnapshot();

    const encryptDescriptor = signupUserGenerator.next(encrypt).value;
    expect(encryptDescriptor).toMatchSnapshot();
  });

  it('should dispatch the setUserAuth action if it requests the data successfully', () => {
    const putDescriptor = signupUserGenerator.next(response.applicant.details).value;
    expect(putDescriptor).toEqual(put(setUserAuth(response.applicant.details)));
  });

  it('should dispatch the signUpUserSuccess action if it requests the data successfully', () => {
    signupUserGenerator.next(response.applicant.details);
    const putDescriptor = signupUserGenerator.next('').value;
    expect(putDescriptor).toEqual(put(signUpUserSuccess('')));
  });

  it('should call the signUpUserError action if the response errors', () => {
    const putDescriptor = signupUserGenerator.throw('Error').value;
    expect(putDescriptor).toEqual(put(signUpUserError('Error')));
  });
});

describe('watchSignupUser Saga', () => {
  const generator = watchSignupUser();
  const mockTask = createMockTask();

  it('waits for start action SIGNUP_USER', () => {
    const expectedYield = takeLatest(SIGNUP_USER, signupUser);
    expect(generator.next().value).toEqual(expectedYield);
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
