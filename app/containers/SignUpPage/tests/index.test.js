import React from 'react';
import { shallow } from 'enzyme';
import { shallowWithIntl } from 'helpers/intl-enzyme-test-helper';
import { Redirect } from 'react-router-dom';

import SignUpForm from 'components/SignUpForm';
import { signUpUser, resetForm } from '../actions';
import { SignUpPage, mapDispatchToProps } from '../index';

describe('<SignUpPage />', () => {
  let props;
  beforeEach(() => {
    props = {
      handleSignUp: jest.fn(),
      resetSignUpForm: jest.fn(),
      signuppage: {
        signingUp: false,
        signUpSuccess: false,
        signUpError: false,
        messageToUser: '',
      },
      getGenders: jest.fn(),
      location: {
        search: '',
      },
    };
  });

  it('should display `Redirect` if logged in', () => {
    const wrapper = shallow(<SignUpPage {...props} user={{}} />);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

  it('should contain `SignUpForm` component', () => {
    const wrapper = shallowWithIntl(<SignUpPage {...props} />);
    const {
      signingUp,
      signUpSuccess,
      signUpError,
      messageToUser,
    } = props.signuppage;
    expect(wrapper.contains(
      <SignUpForm
        signUp={props.handleSignUp}
        saving={signingUp}
        isSuccess={signUpSuccess}
        hasError={signUpError}
        msgToUser={messageToUser.error}
        queryParams={props.location.search}
      />
    )).toBe(true);
  });

  it('should reset form on `componentWillUnmount`', () => {
    const wrapper = shallow(<SignUpPage {...props} user={{}} />);
    wrapper.instance().componentWillUnmount();
    expect(props.resetSignUpForm).toHaveBeenCalledTimes(1);
  });

  describe('mapDispatchToProps', () => {
    describe('handleSignUp', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.handleSignUp).toBeDefined();
      });

      it('should dispatch signUpUser when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const creds = {
          email: 'email@email.com',
          password: 'myPassword',
        };
        result.handleSignUp(creds);
        expect(dispatch).toHaveBeenCalledWith(signUpUser(creds));
      });
    });

    describe('resetSignUpForm', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.resetSignUpForm).toBeDefined();
      });

      it('should dispatch resetForm when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.resetSignUpForm();
        expect(dispatch).toHaveBeenCalledWith(resetForm());
      });
    });
  });
});
