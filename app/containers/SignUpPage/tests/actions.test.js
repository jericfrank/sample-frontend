
import {
  signUpUser,
  signUpUserSuccess,
  signUpUserError,
  resetForm,
} from '../actions';
import {
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_ERROR,
  RESET_SIGNUP_FORM,
} from '../constants';

describe('SignUpPage actions', () => {
  describe('signUpUser', () => {
    it('has a type of SIGNUP_USER', () => {
      const user = {};
      const expected = {
        type: SIGNUP_USER,
        user,
      };
      expect(signUpUser(user)).toEqual(expected);
    });
  });

  describe('signUpUserSuccess', () => {
    it('has a type of SIGNUP_USER_SUCCESS', () => {
      const expected = {
        type: SIGNUP_USER_SUCCESS,
      };
      expect(signUpUserSuccess()).toEqual(expected);
    });
  });

  describe('signUpUserError', () => {
    it('has a type of SIGNUP_USER_ERROR', () => {
      const errorMsg = 'Something went wrong.';
      const expected = {
        type: SIGNUP_USER_ERROR,
        errorMsg,
      };
      expect(signUpUserError(errorMsg)).toEqual(expected);
    });
  });

  describe('resetForm', () => {
    it('has a type of RESET_SIGNUP_FORM', () => {
      const expected = {
        type: RESET_SIGNUP_FORM,
      };
      expect(resetForm()).toEqual(expected);
    });
  });
});
