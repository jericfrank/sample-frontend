/*
 *
 * SignUpPage constants
 *
 */

export const SIGNUP_USER = 'app/SignUpPage/SIGNUP_USER';
export const SIGNUP_USER_SUCCESS = 'app/SignUpPage/SIGNUP_USER_SUCCESS';
export const SIGNUP_USER_ERROR = 'app/SignUpPage/SIGNUP_USER_ERROR';
export const RESET_SIGNUP_FORM = 'app/SignUpPage/RESET_SIGNUP_FORM';
