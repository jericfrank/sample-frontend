/**
 *
 * SignUpPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import SignUpForm from 'components/SignUpForm';
import { makeSelectUser } from 'containers/App/selectors';

import '../LoginPage/styles';
import { signUpUser, resetForm } from './actions';
import makeSelectSignUpPage from './selectors';
import reducer from './reducer';
import saga from './saga';

export class SignUpPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentWillUnmount() {
    this.props.resetSignUpForm();
  }

  render() {
    const { handleSignUp, signuppage, user } = this.props;
    const {
      signingUp,
      signUpSuccess,
      signUpError,
      messageToUser,
    } = signuppage;

    if (user) {
      return <Redirect to={`/myprofile${this.props.location.search}`} />;
    }

    return (
      <div className="login-container">
        <Helmet>
          <title>Register</title>
          <meta name="description" content="Register of Login" />
        </Helmet>

        <div className="login-content">
          <h1 className="font-champ-bold text-center">Welcome to Job Board!</h1>
          <h1 className="font-champ-bold text-center">Lets get you started.</h1>

          <p className="text-center">Create an account so that you can track your application’s status</p>

          <SignUpForm
            signUp={handleSignUp}
            saving={signingUp}
            isSuccess={signUpSuccess}
            hasError={signUpError}
            msgToUser={messageToUser.error}
            queryParams={this.props.location.search}
          />
        </div>
      </div>
    );
  }
}

SignUpPage.propTypes = {
  signuppage: PropTypes.object.isRequired,
  handleSignUp: PropTypes.func.isRequired,
  resetSignUpForm: PropTypes.func.isRequired,
  user: PropTypes.object,
  location: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  signuppage: makeSelectSignUpPage(),
  user: makeSelectUser(),
});

export function mapDispatchToProps(dispatch) {
  return {
    handleSignUp: (creds) => dispatch(signUpUser(creds)),
    resetSignUpForm: () => dispatch(resetForm()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'signUpPage', reducer });
const withSaga = injectSaga({ key: 'signUpPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(SignUpPage);
