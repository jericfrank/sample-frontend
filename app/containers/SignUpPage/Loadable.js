/**
 *
 * Asynchronously loads the component for SignUpPage
 *
 */

import Loadable from 'react-loadable';
import FullPageLoader from 'components/FullPageLoader';

export default Loadable({
  loader: () => import('./index'),
  loading: FullPageLoader,
  delay: 300,
});
