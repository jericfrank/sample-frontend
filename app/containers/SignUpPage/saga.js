import _ from 'lodash';
import CryptoJS from 'crypto-js';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { postRequest } from 'utils/request';
import { handleJwtToken } from 'utils/jwtToken';
import { message } from 'antd';

import { setUserAuth } from 'containers/App/actions';
import { signUpUserSuccess, signUpUserError } from './actions';
import { SIGNUP_USER } from './constants';

export function* signupUser({ user }) {
  try {
    const payload = _.omit(user, ['confirmPassword']);
    const { applicant } = yield call(postRequest, '/hr/applicants/register', payload);
    const { details, token } = applicant;

    yield call(handleJwtToken, token);
    yield call(message.success, 'Thanks for signing up! Please check your email to get verified.');

    const encrypt = yield call(CryptoJS.AES.encrypt, JSON.stringify({ ...details }), token);
    localStorage.setItem('jobportalUser', encrypt);

    yield put(setUserAuth(details));
    yield put(signUpUserSuccess(''));
  } catch (err) {
    yield put(signUpUserError(err));
  }
}

export default function* watchSignupUser() {
  const watcher = yield takeLatest(SIGNUP_USER, signupUser);

  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
