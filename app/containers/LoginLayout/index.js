/**
 *
 * LoginLayout
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Row, Col } from 'antd';
import styled from 'styled-components';
import BG_WHITEPRINT from 'images/bg-whiteprint.jpg';

import LoginHeader from 'components/LoginHeader';

const Wrapper = styled.div`
  color: #464648;

  .login-bgprint {
    height: 100vh;
    background: url(${BG_WHITEPRINT});
    background-size: 400%;
  }

  .login-bgwhite {
    height: 100vh;
    background-color: white;
  }

  .login-content {
    position: absolute;
    width: 100%;
    padding: 0px 50px;
    border-radius: 5px;

    h1 {
      font-size: 50px;
      line-height: 40px;
      color: #464648;
    }

    p {
      margin: 50px 0px 30px 0px;
      font-size: 16px;
    }

    .loginpage-form {
      .social-btn {
        width: 150px;
        margin: 5px 10px;
      }
    }
  }
`;

export class LoginLayout extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Wrapper className="login-container">
        <LoginHeader />

        <Row>
          <Col xs={0} sm={0} md={0} lg={4} xl={6} className="login-bgprint">
          </Col>
          <Col xs={24} sm={24} md={24} lg={16} xl={12} className="login-bgwhite">
            {this.props.children}
          </Col>
          <Col xs={0} sm={0} md={0} lg={4} xl={6} className="login-bgprint">
          </Col>
        </Row>
      </Wrapper>
    );
  }
}

LoginLayout.propTypes = {
  children: PropTypes.object.isRequired,
};

const withConnect = connect(null, null);

export default compose(
  withConnect,
)(LoginLayout);
