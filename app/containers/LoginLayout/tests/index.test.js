import React from 'react';
import { shallow } from 'enzyme';

import LoginHeader from 'components/LoginHeader';
import { LoginLayout } from '../index';

describe('<LoginLayout />', () => {
  it('should display `LoginHeader` & `Component` inside `PublicLayout`', () => {
    const Component = <div>Component</div>;
    const wrapper = shallow(
      <LoginLayout>
        <Component match={{ path: '/' }} />
      </LoginLayout>
    );
    expect(wrapper.find(LoginHeader)).toHaveLength(1);
    expect(wrapper.contains(<Component match={{ path: '/' }} />)).toEqual(true);
  });
});
