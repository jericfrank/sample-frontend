/**
 *
 * ChangePasswordPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Form, Input, Icon, Button } from 'antd';
import { Redirect } from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectChangePasswordPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import { changePaswordSubmit } from './actions';

const FormItem = Form.Item;

export class ChangePasswordPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    confirmDirty: false,
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onChangePaswordSubmit(values, this.props.location.search);
      }
    });
  };

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['new-confirm-password'], { force: true });
    }
    callback();
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    const validLength = value && value.length >= 6;
    if (validLength && value !== form.getFieldValue('new-password')) {
      callback('Passwords did not matched');
    } else {
      callback();
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { submitting, submitted } = this.props.changepasswordpage;

    if (submitted) {
      return <Redirect to="/login" />;
    }

    return (
      <div className="login-container">
        <Helmet>
          <title>ChangePasswordPage</title>
          <meta name="description" content="Description of ChangePasswordPage" />
        </Helmet>
        <div className="login-content">
          <h1 className="font-champ-bold">Forgot Password?</h1>
          <h1 className="font-champ-bold">Change your password now</h1>

          <p>Enter your new password.</p>

          <Form onSubmit={this.handleSubmit} className="loginpage-form">
            <FormItem>
              {getFieldDecorator('new-password', {
                rules: [
                  { required: true, message: 'Please input your password' },
                  { min: 6, message: 'Password should be at least 6 characters' },
                  { validator: this.validateToNextPassword },
                ],
              })(
                <Input
                  prefix={(
                    <Icon
                      type="lock"
                      style={{ color: 'rgba(0,0,0,.25)' }}
                    />
                  )}
                  type="password"
                  placeholder="Password"
                  size="large"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('new-confirm-password', {
                rules: [
                  { required: true, message: 'Please input your confirm password' },
                  { min: 6, message: 'Password should be at least 6 characters' },
                  { validator: this.compareToFirstPassword },
                ],
              })(
                <Input
                  prefix={(
                    <Icon
                      type="lock"
                      style={{ color: 'rgba(0,0,0,.25)' }}
                    />
                  )}
                  type="password"
                  placeholder="Confirm Password"
                  size="large"
                  onBlur={this.handleConfirmBlur}
                />
              )}
            </FormItem>
            <FormItem className="no-margin">
              <Button
                type="primary"
                htmlType="submit"
                className="loginpage-form-button pull-right"
                size="large"
                loading={submitting}
              >
                Change Password
              </Button>
            </FormItem>
          </Form>
        </div>
      </div>
    );
  }
}

ChangePasswordPage.propTypes = {
  form: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  onChangePaswordSubmit: PropTypes.func.isRequired,
  changepasswordpage: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  changepasswordpage: makeSelectChangePasswordPage(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangePaswordSubmit: (payload, queryString) => dispatch(changePaswordSubmit(payload, queryString)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'changePasswordPage', reducer });
const withSaga = injectSaga({ key: 'changePasswordPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(
  Form.create()(ChangePasswordPage)
);
