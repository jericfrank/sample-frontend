/*
 *
 * ChangePasswordPage actions
 *
 */

import {
  CHANGE_PASSWORD_SUBMIT,
  CHANGE_PASSWORD_SUBMITTED,
  CHANGE_PASSWORD_ERR,
} from './constants';

export function changePaswordSubmit(payload, queryString) {
  return {
    type: CHANGE_PASSWORD_SUBMIT,
    payload,
    queryString,
  };
}

export function changePaswordSubmitted() {
  return {
    type: CHANGE_PASSWORD_SUBMITTED,
  };
}

export function changePaswordErr(error) {
  return {
    type: CHANGE_PASSWORD_ERR,
    error,
  };
}
