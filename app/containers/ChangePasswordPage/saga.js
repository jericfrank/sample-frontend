import _ from 'lodash';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { postRequest } from 'utils/request';
import { message } from 'antd';

import { changePaswordErr, changePaswordSubmitted } from './actions';
import { CHANGE_PASSWORD_SUBMIT } from './constants';

export function* changePassword({ payload, queryString }) {
  try {
    const { code } = _.chain(queryString)
      .replace('?', '')
      .split('&')
      .map(_.partial(_.split, _, '=', 2))
      .fromPairs()
      .value();

    _.assign(payload, { token: code });

    const response = yield call(postRequest, '/hr/applicants/change_password', payload);
    yield call(message.success, response.message);
    yield put(changePaswordSubmitted());
  } catch (err) {
    yield call(message.error, err.error);
    yield put(changePaswordErr(err.error));
  }
}

// Individual exports for testing
export default function* watchChangePasswordSubmit() {
  const watcher = yield takeLatest(CHANGE_PASSWORD_SUBMIT, changePassword);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
