import { fromJS } from 'immutable';

import makeSelectChangePasswordPage, { selectChangePasswordPageDomain } from '../selectors';

describe('selectChangePasswordPageDomain', () => {
  it('should select `changePasswordPage`', () => {
    const state = fromJS({
      changePasswordPage: {
        submitting: false,
        submitted: false,
        submitErr: false,
      },
    });
    expect(selectChangePasswordPageDomain(state))
      .toEqual(state.get('changePasswordPage'));
  });
});

describe('makeSelectChangePasswordPage', () => {
  it('should select `changePasswordPage`', () => {
    const changePasswordPage = fromJS({
      test: 'test',
    });
    const mockedState = fromJS({
      changePasswordPage,
    });
    expect(makeSelectChangePasswordPage()(mockedState))
      .toEqual(changePasswordPage.toJS());
  });
});
