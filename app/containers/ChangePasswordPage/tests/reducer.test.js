import { fromJS } from 'immutable';
import {
  changePaswordSubmit,
  changePaswordSubmitted,
  changePaswordErr,
} from '../actions';
import changePasswordPageReducer from '../reducer';

describe('changePasswordPageReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({
      submitting: false,
      submitted: false,
      submitErr: false,
    });
  });

  it('returns the initial state', () => {
    expect(changePasswordPageReducer(undefined, {})).toEqual(state);
  });

  it('should handle the changePaswordSubmit action correctly', () => {
    const expectedResult = state
      .set('submitting', true)
      .set('submitted', false)
      .set('submitErr', false);
    const payload = { password: 'test' };
    const queryString = '?code=xxxx';
    expect(changePasswordPageReducer(state, changePaswordSubmit(payload, queryString)))
      .toEqual(expectedResult);
  });

  it('should handle the changePaswordSubmitted action correctly', () => {
    const expectedResult = state
      .set('submitting', false)
      .set('submitted', true)
      .set('submitErr', false);
    expect(changePasswordPageReducer(state, changePaswordSubmitted()))
      .toEqual(expectedResult);
  });

  it('should handle the changePaswordErr action correctly', () => {
    const expectedResult = state
      .set('submitting', false)
      .set('submitted', false)
      .set('submitErr', 'error');
    expect(changePasswordPageReducer(state, changePaswordErr('error')))
      .toEqual(expectedResult);
  });
});
