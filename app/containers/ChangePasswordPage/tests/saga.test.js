/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { take, takeLatest, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';

import { changePaswordSubmitted, changePaswordErr } from '../actions';
import watchChangePasswordSubmit, { changePassword } from '../saga';
import { CHANGE_PASSWORD_SUBMIT } from '../constants';

describe('changePassword Saga', () => {
  let createGenerator;
  let response;

  const action = {
    payload: {},
    queryString: '?code=xxxx',
  };

  beforeEach(() => {
    response = {
      message: 'Sample message.',
    };

    createGenerator = changePassword(action);

    const callDescriptor = createGenerator.next(response).value;
    expect(callDescriptor).toMatchSnapshot();

    const requestDescriptor = createGenerator.next(response).value;
    expect(requestDescriptor).toMatchSnapshot();
  });

  it('should call emailSubmitted action if requests successfully', () => {
    const descriptor = createGenerator.next(action).value;
    expect(descriptor).toEqual(put(changePaswordSubmitted()));
  });

  describe('catch', () => {
    beforeEach(() => {
      const callDescriptor = createGenerator.throw({ error: 'Error' }).value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the changePaswordErr action if the response errors', () => {
      const descriptor = createGenerator.next().value;
      expect(descriptor).toEqual(put(changePaswordErr('Error')));
    });
  });
});

describe('watchChangePasswordSubmit Saga', () => {
  const generator = watchChangePasswordSubmit();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const expectedYield = takeLatest(CHANGE_PASSWORD_SUBMIT, changePassword);
    expect(generator.next().value).toEqual(expectedYield);
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
