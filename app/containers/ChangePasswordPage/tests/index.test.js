import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';
import { Redirect } from 'react-router-dom';

import { changePaswordSubmit } from '../actions';
import { ChangePasswordPage, mapDispatchToProps } from '../index';

describe('<ForgotPassForm />', () => {
  let props;

  beforeEach(() => {
    props = {
      onChangePaswordSubmit: jest.fn(),
      changepasswordpage: {},
      form: {
        getFieldDecorator: () => jest.fn(),
        setFieldsValue: jest.fn(),
        validateFields: jest.fn(),
      },
      location: {
        search: '?code=xxxxx',
      },
    };
  });

  it('should render `ChangePasswordPage`', () => {
    const wrapper = shallow(<ChangePasswordPage {...props} />);
    expect(wrapper.find(Form)).toHaveLength(1);
  });

  it('should call `onChangePaswordSubmit` if form is valid on `handleSubmit`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false, {}),
      },
    };
    const wrapper = shallow(<ChangePasswordPage {...props} />);
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.onChangePaswordSubmit).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `onChangePaswordSubmit` if form is invalid on `handleSubmit`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(true),
      },
    };
    const wrapper = shallow(<ChangePasswordPage {...props} />);
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.onChangePaswordSubmit).toHaveBeenCalledTimes(0);
  });

  it('should call `handleConfirmBlur`', () => {
    const wrapper = shallow(<ChangePasswordPage {...props} />);
    wrapper.instance().handleConfirmBlur({ target: { value: 'test' } });
    expect(wrapper.state().confirmDirty).toBe(true);
  });

  it('should call `validateToNextPassword`', () => {
    const wrapper = shallow(<ChangePasswordPage {...props} />);
    wrapper.setState({ confirmDirty: 'test' });
    wrapper.instance().validateToNextPassword(null, 'test', jest.fn());
    expect(props.form.validateFields).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `validateToNextPassword`', () => {
    const wrapper = shallow(<ChangePasswordPage {...props} />);
    wrapper.setState({ confirmDirty: false });
    wrapper.instance().validateToNextPassword(null, 'test', jest.fn());
    expect(props.form.validateFields).toHaveBeenCalledTimes(0);
  });

  it('should call `compareToFirstPassword`', () => {
    const newProps = {
      ...props,
      form: {
        ...props.form,
        getFieldValue: jest.fn(),
      },
    };
    const wrapper = shallow(<ChangePasswordPage {...newProps} />);
    wrapper.instance().compareToFirstPassword(null, 'test', jest.fn());
    wrapper.instance().compareToFirstPassword(null, 'test password', jest.fn());
    expect(newProps.form.getFieldValue).toHaveBeenCalledTimes(1);
  });

  it('should render `react-router-dom` `Redirect`', () => {
    const newProps = { ...props, changepasswordpage: { submitted: true } };
    const wrapper = shallow(<ChangePasswordPage {...newProps} />);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

  describe('mapDispatchToProps', () => {
    describe('onChangePaswordSubmit', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onChangePaswordSubmit).toBeDefined();
      });

      it('should dispatch changePaswordSubmit when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const payload = {
          password: 'xD',
        };
        const queryString = '?code=xxxx';
        result.onChangePaswordSubmit(payload, queryString);
        expect(dispatch).toHaveBeenCalledWith(changePaswordSubmit(payload, queryString));
      });
    });
  });
});
