
import {
  changePaswordSubmit,
  changePaswordSubmitted,
  changePaswordErr,
} from '../actions';
import {
  CHANGE_PASSWORD_SUBMIT,
  CHANGE_PASSWORD_SUBMITTED,
  CHANGE_PASSWORD_ERR,
} from '../constants';

describe('ForgotPassForm actions', () => {
  describe('changePaswordSubmit', () => {
    it('has a type of CHANGE_PASSWORD_SUBMIT', () => {
      const payload = { password: 'test' };
      const queryString = '?code=xxxxx';
      const expected = {
        type: CHANGE_PASSWORD_SUBMIT,
        payload,
        queryString,
      };
      expect(changePaswordSubmit(payload, queryString)).toEqual(expected);
    });
  });

  describe('changePaswordSubmitted', () => {
    it('has a type of CHANGE_PASSWORD_SUBMITTED', () => {
      const expected = {
        type: CHANGE_PASSWORD_SUBMITTED,
      };
      expect(changePaswordSubmitted()).toEqual(expected);
    });
  });

  describe('changePaswordErr', () => {
    it('has a type of CHANGE_PASSWORD_ERR', () => {
      const expected = {
        type: CHANGE_PASSWORD_ERR,
        error: 'error',
      };
      expect(changePaswordErr('error')).toEqual(expected);
    });
  });
});
