/*
 *
 * ChangePasswordPage constants
 *
 */

export const CHANGE_PASSWORD_SUBMIT = 'app/ChangePasswordPage/CHANGE_PASSWORD_SUBMIT';
export const CHANGE_PASSWORD_SUBMITTED = 'app/ChangePasswordPage/CHANGE_PASSWORD_SUBMITTED';
export const CHANGE_PASSWORD_ERR = 'app/ChangePasswordPage/CHANGE_PASSWORD_ERR';
