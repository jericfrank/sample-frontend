/*
 *
 * ChangePasswordPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CHANGE_PASSWORD_SUBMIT,
  CHANGE_PASSWORD_SUBMITTED,
  CHANGE_PASSWORD_ERR,
} from './constants';

const initialState = fromJS({
  submitting: false,
  submitted: false,
  submitErr: false,
});

function changePasswordPageReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_PASSWORD_SUBMIT:
      return state
        .set('submitting', true)
        .set('submitted', false)
        .set('submitErr', false);

    case CHANGE_PASSWORD_SUBMITTED:
      return state
        .set('submitting', false)
        .set('submitted', true)
        .set('submitErr', false);

    case CHANGE_PASSWORD_ERR:
      return state
        .set('submitting', false)
        .set('submitted', false)
        .set('submitErr', action.error);

    default:
      return state;
  }
}

export default changePasswordPageReducer;
