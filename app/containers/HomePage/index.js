/**
 *
 * HomePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { ParallaxProvider } from 'react-scroll-parallax';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Element } from 'react-scroll';

import Banner from 'components/Banner';
import JobOpenings from 'containers/JobOpenings';
import DescriptionGrid from 'components/DescriptionGrid';
import { makeSelectUser } from 'containers/App/selectors';

export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { user } = this.props;

    return (
      <div>
        <Helmet>
          <title>Job Board</title>
          <meta name="description" content="Description of Job Board" />
        </Helmet>

        <ParallaxProvider>
          <main>
            <Banner hideSignUpBtns={user !== null} />
            <Element name="job-openings" className="element">
              <JobOpenings />
            </Element>
            <DescriptionGrid />
          </main>
        </ParallaxProvider>
      </div>
    );
  }
}

HomePage.propTypes = {
  user: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(
  withConnect,
)(HomePage);
