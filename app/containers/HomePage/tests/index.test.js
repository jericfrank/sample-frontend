import React from 'react';
import { shallow } from 'enzyme';
import { ParallaxProvider } from 'react-scroll-parallax';

import Banner from 'components/Banner';

import { HomePage } from '../index';

describe('<HomePage />', () => {
  it('should render `Banner`, `JobOpenings` inside `ParallaxProvider`', () => {
    const wrapper = shallow(<HomePage />);
    expect(wrapper.find(ParallaxProvider).find(Banner)).toHaveLength(1);
  });
});
