/**
 *
 * Asynchronously loads the component for HomePage
 *
 */

import Loadable from 'react-loadable';
import FullPageLoader from 'components/FullPageLoader';

export default Loadable({
  loader: () => import('./index'),
  loading: FullPageLoader,
});
