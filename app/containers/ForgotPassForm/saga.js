import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { postRequest } from 'utils/request';
import { message } from 'antd';

import { submitEmailErr, emailSubmitted } from './actions';
import { SUBMIT_EMAIL } from './constants';

export function* forgotPassRequest({ forgotEmail, captcha }) {
  try {
    const response = yield call(postRequest, '/hr/applicants/request_change_password', {
      email: forgotEmail,
      captcha,
    });
    yield call(message.success, response.message);
    yield put(emailSubmitted());
  } catch (err) {
    yield call(message.error, err.error);
    yield put(submitEmailErr(err.error));
  }
}

// Individual exports for testing
export default function* watchForgotPassRequest() {
  const watcher = yield takeLatest(SUBMIT_EMAIL, forgotPassRequest);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
