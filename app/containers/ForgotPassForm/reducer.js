/*
 *
 * ForgotPassForm reducer
 *
 */

import { fromJS } from 'immutable';
import {
  SUBMIT_EMAIL,
  EMAIL_SUBMITTED,
  SUBMIT_EMAIL_ERR,
} from './constants';

const initialState = fromJS({
  submitting: false,
  submitted: false,
  submitErr: false,
});

function forgotPassFormReducer(state = initialState, action) {
  switch (action.type) {
    case SUBMIT_EMAIL:
      return state
        .set('submitting', true)
        .set('submitted', false)
        .set('submitErr', false);

    case EMAIL_SUBMITTED:
      return state
        .set('submitting', false)
        .set('submitted', true)
        .set('submitErr', false);

    case SUBMIT_EMAIL_ERR:
      return state
        .set('submitting', false)
        .set('submitted', false)
        .set('submitErr', true);

    default:
      return state;
  }
}

export default forgotPassFormReducer;
