import { createSelector } from 'reselect';

/**
 * Direct selector to the forgotPassForm state domain
 */
const selectForgotPassFormDomain = (state) => state.get('forgotPassForm');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ForgotPassForm
 */

const makeSelectForgotPassForm = () => createSelector(
  selectForgotPassFormDomain,
  (substate) => substate.toJS()
);

export default makeSelectForgotPassForm;
export {
  selectForgotPassFormDomain,
};
