
import { fromJS } from 'immutable';
import {
  submitEmail,
  emailSubmitted,
  submitEmailErr,
} from '../actions';
import forgotPassFormReducer from '../reducer';

describe('forgotPassFormReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({
      submitting: false,
      submitted: false,
      submitErr: false,
    });
  });

  it('returns the initial state', () => {
    expect(forgotPassFormReducer(undefined, {})).toEqual(state);
  });

  it('should handle the submitEmail action correctly', () => {
    const expectedResult = state
      .set('submitting', true)
      .set('submitted', false)
      .set('submitErr', false);
    expect(forgotPassFormReducer(state, submitEmail('email@email.com')))
      .toEqual(expectedResult);
  });

  it('should handle the emailSubmitted action correctly', () => {
    const expectedResult = state
      .set('submitting', false)
      .set('submitted', true)
      .set('submitErr', false);
    expect(forgotPassFormReducer(state, emailSubmitted()))
      .toEqual(expectedResult);
  });

  it('should handle the submitEmailErr action correctly', () => {
    const expectedResult = state
      .set('submitting', false)
      .set('submitted', false)
      .set('submitErr', true);
    expect(forgotPassFormReducer(state, submitEmailErr()))
      .toEqual(expectedResult);
  });
});
