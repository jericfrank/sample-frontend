
import {
  submitEmail,
  emailSubmitted,
  submitEmailErr,
} from '../actions';
import {
  SUBMIT_EMAIL,
  EMAIL_SUBMITTED,
  SUBMIT_EMAIL_ERR,
} from '../constants';

describe('ForgotPassForm actions', () => {
  describe('submitEmail', () => {
    it('has a type of SUBMIT_EMAIL', () => {
      const forgotEmail = 'email@email.com';
      const captcha = 'captchaToken';
      const expected = {
        type: SUBMIT_EMAIL,
        forgotEmail,
        captcha,
      };
      expect(submitEmail({ forgotEmail, captcha })).toEqual(expected);
    });
  });

  describe('emailSubmitted', () => {
    it('has a type of EMAIL_SUBMITTED', () => {
      const expected = {
        type: EMAIL_SUBMITTED,
      };
      expect(emailSubmitted()).toEqual(expected);
    });
  });

  describe('submitEmailErr', () => {
    it('has a type of SUBMIT_EMAIL_ERR', () => {
      const expected = {
        type: SUBMIT_EMAIL_ERR,
      };
      expect(submitEmailErr()).toEqual(expected);
    });
  });
});
