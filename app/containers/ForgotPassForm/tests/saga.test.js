/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { take, takeLatest, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';
import { emailSubmitted, submitEmailErr } from '../actions';
import watchForgotPassRequest, { forgotPassRequest } from '../saga';
import { SUBMIT_EMAIL } from '../constants';

describe('forgotPassRequest Saga', () => {
  let createGenerator;
  let response;

  const action = {
    forgotEmail: 'samp@gmail.com',
    captcha: 'captchaToken',
  };

  beforeEach(() => {
    response = {
      message: 'Sample message.',
    };

    createGenerator = forgotPassRequest(action);

    const callDescriptor = createGenerator.next(response).value;
    expect(callDescriptor).toMatchSnapshot();

    const requestDescriptor = createGenerator.next(response).value;
    expect(requestDescriptor).toMatchSnapshot();
  });

  it('should call emailSubmitted action if requests successfully', () => {
    const descriptor = createGenerator.next(action).value;
    expect(descriptor).toEqual(put(emailSubmitted()));
  });

  describe('catch', () => {
    beforeEach(() => {
      const callDescriptor = createGenerator.throw(response).value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the applyJobError action if the response errors', () => {
      response = new Error('Some error');
      const descriptor = createGenerator.next().value;
      expect(descriptor).toEqual(put(submitEmailErr(response)));
    });
  });
});

describe('watchForgotPassRequest Saga', () => {
  const generator = watchForgotPassRequest();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const expectedYield = takeLatest(SUBMIT_EMAIL, forgotPassRequest);
    expect(generator.next().value).toEqual(expectedYield);
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
