import { fromJS } from 'immutable';

import makeSelectForgotPassForm, { selectForgotPassFormDomain } from '../selectors';

describe('selectForgotPassFormDomain', () => {
  it('should select `forgotPassForm`', () => {
    const state = fromJS({
      forgotPassForm: {
        submitting: false,
        submitted: false,
        submitErr: false,
      },
    });
    expect(selectForgotPassFormDomain(state))
      .toEqual(state.get('forgotPassForm'));
  });
});

describe('makeSelectForgotPassForm', () => {
  it('should select `forgotPassForm`', () => {
    const forgotPassForm = fromJS({
      test: 'test',
    });
    const mockedState = fromJS({
      forgotPassForm,
    });
    expect(makeSelectForgotPassForm()(mockedState))
      .toEqual(forgotPassForm.toJS());
  });
});
