import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';

import { submitEmail } from '../actions';
import { ForgotPassForm, mapDispatchToProps } from '../index';

describe('<ForgotPassForm />', () => {
  let props;

  beforeEach(() => {
    props = {
      onRedirectToLogin: jest.fn(),
      onSubmitEmail: jest.fn(),
      forgotpassform: {
        submitting: false,
      },
      form: {
        getFieldDecorator: () => jest.fn(),
        validateFields: () => jest.fn(),
      },
    };
  });

  it('should render `ForgotPassForm`', () => {
    const wrapper = shallow(<ForgotPassForm {...props} />);
    expect(wrapper.find(Form)).toHaveLength(1);
  });

  it('should call `onSubmitEmail` if form is valid on `handleSubmit`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false, {}),
      },
    };
    const wrapper = shallow(<ForgotPassForm {...props} />);
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.onSubmitEmail).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `onSubmitEmail` if form is invalid on `handleSubmit`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(true),
      },
    };
    const wrapper = shallow(<ForgotPassForm {...props} />);
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.onSubmitEmail).toHaveBeenCalledTimes(0);
  });

  it('should call `form.setFields` if captcha is called `handleCaptcha`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        setFields: jest.fn(),
      },
    };
    const wrapper = shallow(<ForgotPassForm {...props} />);
    wrapper.instance().handleCaptcha('captchaToken');
    expect(props.form.setFields).toHaveBeenCalledTimes(1);
  });

  describe('mapDispatchToProps', () => {
    describe('onSubmitEmail', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onSubmitEmail).toBeDefined();
      });

      it('should dispatch submitEmail when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const email = 'email@email.com';
        result.onSubmitEmail(email);
        expect(dispatch).toHaveBeenCalledWith(submitEmail(email));
      });
    });
  });
});
