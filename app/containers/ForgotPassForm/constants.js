/*
 *
 * ForgotPassForm constants
 *
 */

export const SUBMIT_EMAIL = 'app/ForgotPassForm/SUBMIT_EMAIL';
export const EMAIL_SUBMITTED = 'app/ForgotPassForm/EMAIL_SUBMITTED';
export const SUBMIT_EMAIL_ERR = 'app/ForgotPassForm/SUBMIT_EMAIL_ERR';
