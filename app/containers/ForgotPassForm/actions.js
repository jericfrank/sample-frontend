/*
 *
 * ForgotPassForm actions
 *
 */

import {
  SUBMIT_EMAIL,
  EMAIL_SUBMITTED,
  SUBMIT_EMAIL_ERR,
} from './constants';

export function submitEmail({ forgotEmail, captcha }) {
  return {
    type: SUBMIT_EMAIL,
    forgotEmail,
    captcha,
  };
}

export function emailSubmitted() {
  return {
    type: EMAIL_SUBMITTED,
  };
}

export function submitEmailErr() {
  return {
    type: SUBMIT_EMAIL_ERR,
  };
}
