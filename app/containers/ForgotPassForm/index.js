/**
 *
 * ForgotPassForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Form, Icon, Input, Button } from 'antd';
import Recaptcha from 'react-recaptcha';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { CAPTCHA_KEY } from 'components/SignUpForm/constants';
import { submitEmail } from './actions';
import makeSelectForgotPassForm from './selectors';
import reducer from './reducer';
import saga from './saga';

const FormItem = Form.Item;

export class ForgotPassForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmitEmail(values);
      }
    });
  }

  handleCaptcha = (value) => {
    this.props.form.setFields({
      captcha: {
        value,
      },
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { submitting } = this.props.forgotpassform;

    return (
      <div className="login-container">
        <div className="login-content">
          <h1 className="font-champ-bold text-center">Forgot your password?</h1>
          <h1 className="font-champ-bold text-center">No worries!</h1>

          <p className="text-center">
            Enter your email address below and we&#39;ll send you some reset instructions.
          </p>

          <Form onSubmit={this.handleSubmit} className="loginpage-form">
            <FormItem>
              {getFieldDecorator('forgotEmail', {
                rules: [{ required: true,
                  message: 'Please input your email',
                }, {
                  type: 'email',
                  message: 'Oops, email address seems invalid. Please try again.',
                }],
              })(
                <Input
                  prefix={(
                    <Icon
                      type="mail"
                      style={{ color: 'rgba(0,0,0,.25)' }}
                    />
                  )}
                  placeholder="Email"
                  size="large"
                />
              )}
            </FormItem>
            <div style={{ width: '305px', margin: 'auto', paddingBottom: '15px' }}>
              <FormItem>
                {getFieldDecorator('captcha', {
                  rules: [
                    { required: true, message: 'Captcha required!' },
                  ],
                })(
                  <Recaptcha verifyCallback={this.handleCaptcha} sitekey={CAPTCHA_KEY} />
                )}
              </FormItem>
            </div>
            <FormItem className="no-margin">
              <span className="flipper-btn">
                Remeber it now? Back to&nbsp;
                <span
                  className="bordered-link cursor-pointer"
                  onClick={this.props.onRedirectToLogin}
                >
                  log in.
                </span>
              </span>
              <Button
                type="primary"
                htmlType="submit"
                className="loginpage-form-button pull-right"
                loading={submitting}
                size="large"
              >
                Submit
              </Button>
            </FormItem>
          </Form>
        </div>
      </div>
    );
  }
}

ForgotPassForm.propTypes = {
  form: PropTypes.object.isRequired,
  forgotpassform: PropTypes.object.isRequired,
  onRedirectToLogin: PropTypes.func.isRequired,
  onSubmitEmail: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  forgotpassform: makeSelectForgotPassForm(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onSubmitEmail: (payload) => dispatch(submitEmail(payload)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'forgotPassForm', reducer });
const withSaga = injectSaga({ key: 'forgotPassForm', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Form.create()(ForgotPassForm));
