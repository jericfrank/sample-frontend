/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const DEFAULT_LOCALE = 'en';
export const CHECK_USER_AUTH = 'app/App/CHECK_USER_AUTH';
export const SET_USER_AUTH = 'app/App/SET_USER_AUTH';
export const USER_AUTH_ERR = 'app/App/USER_AUTH_ERR';
export const SHOW_MSG_TO_USER = 'app/App/SHOW_MSG_TO_USER';
export const UNSET_USER_AUTH = 'app/App/UNSET_USER_AUTH';
