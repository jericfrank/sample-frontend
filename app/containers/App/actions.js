/*
 *
 * LoginPage actions
 *
 */

import {
  CHECK_USER_AUTH,
  SET_USER_AUTH,
  USER_AUTH_ERR,
  SHOW_MSG_TO_USER,
  UNSET_USER_AUTH,
} from './constants';

export function checkUserAuth({ email, password }) {
  return {
    type: CHECK_USER_AUTH,
    email,
    password,
  };
}

export function setUserAuth(user) {
  return {
    type: SET_USER_AUTH,
    user,
  };
}

export function userAuthErr() {
  return {
    type: USER_AUTH_ERR,
  };
}

export function showMsgToUser(msgType, msg) {
  return {
    type: SHOW_MSG_TO_USER,
    msgType,
    msg,
  };
}

export function unsetUserAuth() {
  return {
    type: UNSET_USER_AUTH,
  };
}
