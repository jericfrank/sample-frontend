import React from 'react';
import PropTypes from 'prop-types';
import { shallow, mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import { fromJS } from 'immutable';
import { message } from 'antd';

import { App, AppRoute, authenticatedSelector } from '../index';

describe('<App />', () => {
  it('should render some routes', () => {
    const renderedComponent = shallow(<App />);
    expect(renderedComponent.find(AppRoute).length).not.toBe(0);
  });

  it('should trigger `antd.message`', () => {
    const props = {
      showMsgToUser: true,
      msgToUser: {
        msgType: 'info',
        msg: 'This is a message.',
      },
    };
    const spy = jest.spyOn(message, 'info');
    const renderedComponent = shallow(<App {...props} />);
    renderedComponent.instance().componentDidUpdate();
    expect(spy).toHaveBeenCalledTimes(1);
    message.info.mockReset();
    message.info.mockRestore();
  });

  it('should NOT trigger `antd.message`', () => {
    const spy = jest.spyOn(message, 'info');
    const renderedComponent = shallow(<App />);
    renderedComponent.instance().componentDidUpdate();
    expect(spy).toHaveBeenCalledTimes(0);
    message.info.mockReset();
    message.info.mockRestore();
  });

  it('should render `AppRoute`\'s `Component` inside `Layout`', () => {
    const Layout = (props) => <div className="layout">{props.children}</div>;
    const Component = () => <div className="component">Component</div>;
    Layout.propTypes = { children: PropTypes.object };
    const renderedComponent = mount(
      <Router>
        <AppRoute layout={Layout} component={Component} />
      </Router>
    );
    expect(renderedComponent.find('.component')).toHaveLength(1);
  });

  it('should populate `app` user state', () => {
    const app = { user: {} };
    expect(authenticatedSelector(fromJS({ app }))).toBeTruthy();
  });
});
