import { fromJS } from 'immutable';

import {
  makeSelectLocation,
  makeSelectAuthenticating,
  makeSelectAuthenticated,
  makeSelectShowMsgToUser,
  makeSelectMsgToUser,
  makeSelectUser,
} from '../selectors';

describe('makeSelectLocation', () => {
  it('should select the location', () => {
    const route = fromJS({
      location: { pathname: '/foo' },
    });
    const mockedState = fromJS({
      route,
    });
    expect(makeSelectLocation()(mockedState)).toEqual(route.get('location').toJS());
  });
});

describe('makeSelectAuthenticating', () => {
  it('should select the app/authenticating', () => {
    const app = fromJS({
      authenticating: false,
    });
    const mockedState = fromJS({
      app,
    });
    expect(makeSelectAuthenticating()(mockedState)).toEqual(app.get('authenticating'));
  });
});

describe('makeSelectAuthenticated', () => {
  it('should select the app/authenticated', () => {
    const app = fromJS({
      authenticated: false,
    });
    const mockedState = fromJS({
      app,
    });
    expect(makeSelectAuthenticated()(mockedState)).toEqual(app.get('authenticated'));
  });
});

describe('makeSelectShowMsgToUser', () => {
  it('should select the app/showMsgToUser', () => {
    const app = fromJS({
      showMsgToUser: false,
    });
    const mockedState = fromJS({
      app,
    });
    expect(makeSelectShowMsgToUser()(mockedState)).toEqual(app.get('showMsgToUser'));
  });
});

describe('makeSelectMsgToUser', () => {
  it('should select the app/msgToUser', () => {
    const app = fromJS({
      msgToUser: {
        msgType: 'info',
        msg: 'This is a message',
      },
    });
    const mockedState = fromJS({
      app,
    });
    expect(makeSelectMsgToUser()(mockedState)).toEqual(app.get('msgToUser').toJS());
  });
});

describe('makeSelectUser', () => {
  it('should select the app/user', () => {
    const app = fromJS({
      user: {},
    });
    const mockedState = fromJS({
      app,
    });
    expect(makeSelectUser()(mockedState)).toEqual(app.get('user'));
  });
});
