import { fromJS } from 'immutable';

import appReducer, { defaultMsgToUser } from '../reducer';
import {
  checkUserAuth,
  setUserAuth,
  userAuthErr,
  showMsgToUser,
  unsetUserAuth,
} from '../actions';

describe('appReducer', () => {
  let state;
  beforeEach(() => {
    state = fromJS({
      authenticating: false,
      authenticated: false,
      authenticateErr: false,
      showMsgToUser: false,
      msgToUser: defaultMsgToUser,
      user: null,
    });

    class LocalStorageMock {
      constructor() {
        this.store = {};
      }

      clear() {
        this.store = {};
      }

      getItem(key) {
        return this.store[key] || null;
      }

      setItem(key, value) {
        this.store[key] = value.toString();
      }

      removeItem(key) {
        delete this.store[key];
      }
    }

    global.localStorage = new (LocalStorageMock)();
  });

  it('should return the initial state', () => {
    const expectedResult = state;
    expect(appReducer(undefined, {})).toEqual(expectedResult);
  });

  it('should handle the checkUserAuth action correctly', () => {
    const creds = {
      username: 'admin',
      password: 'admin',
    };
    const expectedResult = state
      .set('authenticating', true)
      .set('authenticated', false);
    expect(appReducer(state, checkUserAuth(creds))).toEqual(expectedResult);
  });

  it('should handle the setUserAuth action correctly', () => {
    const user = {
      firstName: 'James',
      lastName: 'Brennan',
    };
    const expectedResult = state
      .set('authenticated', true)
      .set('user', user);
    expect(appReducer(state, setUserAuth(user))).toEqual(expectedResult);
  });

  it('should handle the userAuthErr action correctly', () => {
    const expectedResult = state
      .set('authenticateErr', true);
    expect(appReducer(state, userAuthErr())).toEqual(expectedResult);
  });

  it('should handle the showMsgToUser action correctly', () => {
    const msgType = 'info';
    const msg = 'This is a message.';
    const expectedResult = state
      .setIn(['msgToUser', 'msgType'], msgType)
      .setIn(['msgToUser', 'msg'], msg)
      .set('showMsgToUser', true)
      .set('authenticating', false)
      .set('authenticated', false);
    expect(appReducer(state, showMsgToUser(msgType, msg)))
      .toEqual(expectedResult);
  });

  it('should handle the unsetUserAuth action correctly', () => {
    const expectedResult = state
      .set('user', null)
      .setIn(['msgToUser', 'msgType'], defaultMsgToUser.msgType)
      .setIn(['msgToUser', 'msg'], defaultMsgToUser.msg)
      .set('authenticating', false)
      .set('authenticated', false);
    expect(appReducer(state, unsetUserAuth())).toEqual(expectedResult);
  });
});
