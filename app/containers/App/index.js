/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect';
import { message } from 'antd';

import PublicLayout from 'containers/PublicLayout/Loadable';
import LoginLayout from 'containers/LoginLayout/Loadable';

import LoginPage from 'containers/LoginPage';
import LogoutPage from 'containers/LogoutPage';
import SignUpPage from 'containers/SignUpPage';
import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import JobOpenings from 'containers/JobOpenings/Loadable';
import JobPage from 'containers/JobPage/Loadable';
import ApplyPage from 'containers/ApplyPage/Loadable';
import ProfilePage from 'containers/ProfilePage/Loadable';
import ChangePasswordPage from 'containers/ChangePasswordPage/Loadable';
import ChangeUserPassPage from 'containers/ChangeUserPassPage/Loadable';
import VerifyPage from 'containers/VerifyPage/Loadable';
import MyApplicationPage from 'containers/MyApplicationPage/Loadable';

import { makeSelectShowMsgToUser, makeSelectMsgToUser } from './selectors';

export const authenticatedSelector = (state) =>
  state.get('app').get('user') !== null;

const userIsAuthenticated = connectedRouterRedirect({
   // The url to redirect user to if they fail
  redirectPath: '/register',
   // If selector is true, wrapper will not redirect
   // For example let's check that state contains user data
  authenticatedSelector,
  // A nice display name for this check
  wrapperDisplayName: 'UserIsAuthenticated',
  allowRedirectBack: true,
});

export const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (
      <Layout>
        <Component {...props} />
      </Layout>
    )}
  />
);

AppRoute.propTypes = {
  component: PropTypes.func.isRequired,
  layout: PropTypes.func.isRequired,
};

export class App extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidUpdate() {
    if (this.props.showMsgToUser) {
      const { msgType, msg } = this.props.msgToUser;
      message[msgType](msg);
    }
  }

  render() {
    return (
      <Router>
        <div>
          <Switch>
            {/* public routes */}
            <AppRoute exact path="/" layout={PublicLayout} component={HomePage} />
            <AppRoute exact path="/login" layout={LoginLayout} component={LoginPage} />
            <AppRoute exact path="/login/verify" layout={LoginLayout} component={VerifyPage} />
            <AppRoute exact path="/register" layout={LoginLayout} component={SignUpPage} />
            <AppRoute exact path="/change-password" layout={LoginLayout} component={ChangePasswordPage} />
            <Route exact path="/logout" component={LogoutPage} />
            <AppRoute exact path="/jobs/:id/apply" layout={PublicLayout} component={userIsAuthenticated(ApplyPage)} />
            <AppRoute exact path="/jobs/:id" layout={PublicLayout} component={JobPage} />
            <AppRoute exact path="/jobs" layout={PublicLayout} component={JobOpenings} />
            <AppRoute exact path="/myprofile" layout={PublicLayout} component={ProfilePage} />
            <AppRoute exact path="/myprofile/change-password" layout={PublicLayout} component={userIsAuthenticated(ChangeUserPassPage)} />
            <AppRoute exact path="/myapplications" layout={PublicLayout} component={userIsAuthenticated(MyApplicationPage)} />
            <AppRoute layout={PublicLayout} component={NotFoundPage} />
          </Switch>
        </div>
      </Router>
    );
  }
}

App.propTypes = {
  showMsgToUser: PropTypes.bool,
  msgToUser: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  showMsgToUser: makeSelectShowMsgToUser(),
  msgToUser: makeSelectMsgToUser(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(
  withConnect,
)(App);
