/*
 *
 * App reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CHECK_USER_AUTH,
  SET_USER_AUTH,
  USER_AUTH_ERR,
  SHOW_MSG_TO_USER,
  UNSET_USER_AUTH,
} from './constants';

export const defaultMsgToUser = {
  msgType: 'info',
  msg: '',
};

const initialState = fromJS({
  authenticating: false,
  authenticated: false,
  authenticateErr: false,
  showMsgToUser: false,
  msgToUser: defaultMsgToUser,
  user: null,
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case CHECK_USER_AUTH:
      return state
        .set('authenticated', false)
        .set('authenticating', true)
        .set('authenticateErr', false)
        .set('showMsgToUser', false)
        .setIn(['msgToUser', 'msgType'], defaultMsgToUser.msgType)
        .setIn(['msgToUser', 'msg'], defaultMsgToUser.msg)
        .set('user', null);

    case SET_USER_AUTH: {
      return state
        .set('authenticated', true)
        .set('authenticating', false)
        .set('authenticateErr', false)
        .set('showMsgToUser', false)
        .setIn(['msgToUser', 'msgType'], defaultMsgToUser.msgType)
        .setIn(['msgToUser', 'msg'], defaultMsgToUser.msg)
        .set('user', action.user);
    }

    case USER_AUTH_ERR: {
      return state
        .set('authenticated', false)
        .set('authenticating', false)
        .set('authenticateErr', true)
        .set('showMsgToUser', false)
        .set('user', null);
    }

    case SHOW_MSG_TO_USER:
      return state
        .set('showMsgToUser', true)
        .setIn(['msgToUser', 'msgType'], action.msgType)
        .setIn(['msgToUser', 'msg'], action.msg);

    case UNSET_USER_AUTH: {
      localStorage.removeItem('jobportalUser');
      return state
        .set('authenticating', false)
        .set('authenticated', false)
        .set('showMsgToUser', false)
        .setIn(['msgToUser', 'msgType'], 'info')
        .setIn(['msgToUser', 'msg'], '')
        .set('user', null);
    }

    default:
      return state;
  }
}

export default appReducer;
