import { createSelector } from 'reselect';

const selectRoute = (state) => state.get('route');

const makeSelectLocation = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('location').toJS(),
);

const selectApp = (state) => state.get('app');

const makeSelectAuthenticating = () => createSelector(
  selectApp,
  (routeState) => routeState.get('authenticating'),
);

const makeSelectAuthenticated = () => createSelector(
  selectApp,
  (routeState) => routeState.get('authenticated'),
);

const makeSelectShowMsgToUser = () => createSelector(
  selectApp,
  (routeState) => routeState.get('showMsgToUser'),
);

const makeSelectMsgToUser = () => createSelector(
  selectApp,
  (routeState) => routeState.get('msgToUser').toJS(),
);

const makeSelectUser = () => createSelector(
  selectApp,
  (routeState) => routeState.get('user'),
);

export {
  makeSelectLocation,
  makeSelectAuthenticating,
  makeSelectAuthenticated,
  makeSelectShowMsgToUser,
  makeSelectMsgToUser,
  makeSelectUser,
};
