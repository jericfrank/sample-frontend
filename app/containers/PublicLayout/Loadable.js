/**
 *
 * Asynchronously loads the component for PublicLayout
 *
 */

import Loadable from 'react-loadable';
import FullPageLoader from 'components/FullPageLoader';

export default Loadable({
  loader: () => import('./index'),
  loading: FullPageLoader,
  delay: 300,
});
