import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  .publiclayout {
    height: 100vh;

    .publiclayout-header {
      color: #FFF;
    }

    .publiclayout-content {
      background-color: #FFF;
    }

    .publiclayout-footer {
      text-align: center;
      background-color: #217BD0;
      color: #FFF;
    }
  }
`;
