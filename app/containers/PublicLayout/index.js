/**
 *
 * PublicLayout
 *
 */

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Layout } from 'antd';

import Header from 'components/Header';
import { makeSelectUser } from 'containers/App/selectors';

import './styles';

const { Content, Footer } = Layout;

export class PublicLayout extends React.PureComponent {
  render() {
    const { children, currentUser } = this.props;
    const { path } = children.props.match;
    const userNotEmpty = !_.isEmpty(currentUser);
    const queryParams = `?redirect=${encodeURIComponent(children.props.match.url)}`;

    return (
      <Layout className="publiclayout">
        <Header
          hideBtns={path === '/login' || path === '/register' || userNotEmpty}
          user={currentUser}
          queryParams={queryParams}
        />

        <Content className="publiclayout-content">
          {children}
        </Content>

        <div className="clear"></div>

        <Footer className="publiclayout-footer">
          &copy; 2018 Job Board
        </Footer>
      </Layout>
    );
  }
}

PublicLayout.propTypes = {
  children: PropTypes.object.isRequired,
  currentUser: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectUser(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(
  withConnect,
)(PublicLayout);
