import React from 'react';
import { shallow } from 'enzyme';

import { PublicLayout } from '../index';

describe('<PublicLayout />', () => {
  it('should display `Component` inside `PublicLayout`', () => {
    const Component = <div>Component</div>;
    const wrapper = shallow(
      <PublicLayout>
        <Component match={{ path: '/' }} />
      </PublicLayout>
    );
    expect(wrapper.contains(<Component match={{ path: '/' }} />)).toEqual(true);
  });
});
