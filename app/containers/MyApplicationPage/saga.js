import _ from 'lodash';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { getRequest } from 'utils/request';
import { message } from 'antd';

import { applicationsFetched, fetchApplicationsErr } from './actions';
import { FETCH_APPLICATIONS } from './constants';

export function* fetchApplications() {
  try {
    const applications = yield call(getRequest, '/hr/applicants/job_applications');
    const divisions = yield call(getRequest, '/hr/applicants/divisions');

    const userApplications = _.map(applications, (application) => ({
      key: application.job.id,
      id: application.job.id,
      title: application.job.title,
      location: application.job.location,
      employment_type: application.job.employment_type,
      department: _.result(
        _.find(divisions, { id: parseInt(application.job.department_id, 10) }),
        'description',
        '------',
      ),
      status: application.status.label,
    }));

    yield put(applicationsFetched(_.orderBy(userApplications, ['title'])));
  } catch (err) {
    yield call(message.error, 'Something went wrong when fetching applications. Please try again.');
    yield put(fetchApplicationsErr());
  }
}

export default function* watchMyApplications() {
  const watcher = yield takeLatest(FETCH_APPLICATIONS, fetchApplications);

  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
