/*
 *
 * MyApplicationPage constants
 *
 */

export const FETCH_APPLICATIONS = 'app/MyApplicationPage/FETCH_APPLICATIONS';
export const APPLICATIONS_FETCHED = 'app/MyApplicationPage/APPLICATIONS_FETCHED';
export const FETCH_APPLICATIONS_ERR = 'app/MyApplicationPage/FETCH_APPLICATIONS_ERR';
