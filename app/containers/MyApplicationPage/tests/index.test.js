import React from 'react';
import { shallow } from 'enzyme';
import { BrowserRouter as Router, Redirect } from 'react-router-dom';
import { Table } from 'antd';

import { fetchApplications } from '../actions';
import { MyApplicationPage, mapDispatchToProps } from '../index';

describe('<MyApplicationPage />', () => {
  let props;

  beforeEach(() => {
    props = {
      getApplications: jest.fn(),
      myapplicationpage: {
        fetching: false,
        applications: [],
      },
      match: {
        url: '',
      },
    };
  });

  it('should render `MyApplicationPage`', () => {
    const wrapper = shallow(<MyApplicationPage {...props} />);
    expect(wrapper.find(Table)).toHaveLength(1);
  });

  it('should invoke `getApplications` on mount', () => {
    const wrapper = shallow(<MyApplicationPage {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.getApplications).toHaveBeenCalledTimes(1);
  });

  it('should invoke `getApplications` on mount', () => {
    const job = { id: 1 };
    props = {
      ...props,
      match: {
        url: '/myapplications',
      },
    };
    const wrapper = shallow(<MyApplicationPage {...props} />);
    const renderTitle = shallow(
      <Router>
        {wrapper.instance().renderTitle('Job', job)}
      </Router>
    );
    expect(
      renderTitle.find('[to="/jobs/1?redirect=%2Fmyapplications"]')
    ).toHaveLength(1);
  });

  it('should `Redirect` the user to the previous page', () => {
    props = {
      ...props,
      myapplicationpage: { ...props.myapplicationpage, fetchErr: true },
    };
    const wrapper = shallow(<MyApplicationPage {...props} />);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

  describe('mapDispatchToProps', () => {
    describe('getApplications', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.getApplications).toBeDefined();
      });

      it('should dispatch fetchApplications when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.getApplications();
        expect(dispatch).toHaveBeenCalledWith(fetchApplications());
      });
    });
  });
});
