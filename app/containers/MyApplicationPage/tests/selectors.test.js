import { fromJS } from 'immutable';

import makeSelectMyApplicationPage, { selectMyApplicationPageDomain } from '../selectors';

describe('selectMyApplicationPageDomain', () => {
  it('should select `myApplicationPage`', () => {
    const state = fromJS({
      myApplicationPage: {
        test: 'test',
      },
    });
    expect(selectMyApplicationPageDomain(state))
      .toEqual(state.get('myApplicationPage'));
  });
});

describe('makeSelectMyApplicationPage', () => {
  it('should select `myApplicationPage`', () => {
    const myApplicationPage = fromJS({
      test: 'test',
    });
    const mockedState = fromJS({
      myApplicationPage,
    });
    expect(makeSelectMyApplicationPage()(mockedState))
      .toEqual(myApplicationPage.toJS());
  });
});
