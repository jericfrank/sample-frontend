
import { fromJS } from 'immutable';
import {
  fetchApplications, applicationsFetched, fetchApplicationsErr,
} from '../actions';

import myApplicationPageReducer from '../reducer';

describe('myApplicationPageReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({
      fetching: false,
      fetched: false,
      fetchErr: false,
      applications: [],
    });
  });

  it('returns the initial state', () => {
    expect(myApplicationPageReducer(undefined, {})).toEqual(state);
  });

  it('should handle the fetchApplications action correctly', () => {
    const expectedResult = state
      .set('fetching', true)
      .set('fetched', false)
      .set('fetchErr', false)
      .set('applications', []);
    expect(myApplicationPageReducer(state, fetchApplications())).toEqual(expectedResult);
  });

  it('should handle the applicationsFetched action correctly', () => {
    const applications = fromJS([]);
    const expectedResult = state
      .set('fetching', false)
      .set('fetched', true)
      .set('fetchErr', false)
      .set('applications', applications);
    expect(myApplicationPageReducer(state, applicationsFetched(applications))).toEqual(expectedResult);
  });

  it('should handle the fetchApplicationsErr action correctly', () => {
    const expectedResult = state
      .set('fetching', false)
      .set('fetched', false)
      .set('fetchErr', true)
      .set('applications', []);
    expect(myApplicationPageReducer(state, fetchApplicationsErr())).toEqual(expectedResult);
  });
});
