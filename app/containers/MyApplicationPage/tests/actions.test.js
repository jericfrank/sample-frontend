
import {
  fetchApplications, applicationsFetched, fetchApplicationsErr,
} from '../actions';
import {
  FETCH_APPLICATIONS,
  APPLICATIONS_FETCHED,
  FETCH_APPLICATIONS_ERR,
} from '../constants';

describe('MyApplicationPage actions', () => {
  describe('fetchApplications', () => {
    it('has a type of FETCH_APPLICATIONS', () => {
      const expected = {
        type: FETCH_APPLICATIONS,
      };
      expect(fetchApplications()).toEqual(expected);
    });
  });

  describe('applicationsFetched', () => {
    it('has a type of APPLICATIONS_FETCHED', () => {
      const applications = [];
      const expected = {
        type: APPLICATIONS_FETCHED,
        applications,
      };
      expect(applicationsFetched(applications)).toEqual(expected);
    });
  });

  describe('fetchApplicationsErr', () => {
    it('has a type of FETCH_APPLICATIONS_ERR', () => {
      const expected = {
        type: FETCH_APPLICATIONS_ERR,
      };
      expect(fetchApplicationsErr()).toEqual(expected);
    });
  });
});
