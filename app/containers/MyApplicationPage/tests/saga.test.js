/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { take, put, takeLatest, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';

import { applicationsFetched, fetchApplicationsErr } from '../actions';
import watchMyApplications, { fetchApplications } from '../saga';
import { FETCH_APPLICATIONS } from '../constants';

describe('fetchApplications Saga', () => {
  let fetchApplicationsCredsGenerator;
  let application;
  let applications;
  let division;
  let divisions;

  beforeEach(() => {
    fetchApplicationsCredsGenerator = fetchApplications();
    application = {
      job: {
        id: 1,
        title: 'Title',
        location: 'Location',
        employment_type: 'Employment Type',
        department_id: 1,
      },
      status: {
        label: 'Recieved',
      },
    };
    applications = [application];
    division = { id: 1, description: 'Department' };
    divisions = [division];

    const divisionsDescriptor = fetchApplicationsCredsGenerator.next().value;
    expect(divisionsDescriptor).toMatchSnapshot();

    const applicationsDescriptor = fetchApplicationsCredsGenerator.next(applications).value;
    expect(applicationsDescriptor).toMatchSnapshot();
  });

  it('should dispatch the applicationsFetched action if it requests the data successfully', () => {
    const userApplications = [{
      key: application.job.id,
      id: application.job.id,
      title: application.job.title,
      location: application.job.location,
      employment_type: application.job.employment_type,
      department: division.description,
      status: application.status.label,
    }];
    const putDescriptor = fetchApplicationsCredsGenerator.next(divisions).value;
    expect(putDescriptor).toEqual(put(applicationsFetched(userApplications)));
  });

  describe('catch', () => {
    beforeEach(() => {
      const callDescriptor = fetchApplicationsCredsGenerator.throw().value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the fetchApplicationsErr action if the response errors', () => {
      const putDescriptor = fetchApplicationsCredsGenerator.next().value;
      expect(putDescriptor).toEqual(put(fetchApplicationsErr()));
    });
  });
});

describe('watchMyApplications Saga', () => {
  const generator = watchMyApplications();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const effect = generator.next().value;
    expect(effect).toEqual(takeLatest(FETCH_APPLICATIONS, fetchApplications));
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
