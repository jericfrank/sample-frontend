/**
 *
 * MyApplicationPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link, Redirect } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Row, Col, Card, Table, Tooltip, Icon } from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import Wrapper from 'containers/ProfilePage/Wrapper';

import { fetchApplications } from './actions';
import makeSelectMyApplicationPage from './selectors';
import reducer from './reducer';
import saga from './saga';

export class MyApplicationPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.getApplications();
  }

  renderTitle = (text, { id }) => {
    const queryParams = `?redirect=${encodeURIComponent(this.props.match.url)}`;
    return (
      <Link to={`/jobs/${id}${queryParams}`}>
        {text} {(
          <Tooltip title="Applied">
            <Icon type="check-circle" style={{ color: '#52c41a' }} />
          </Tooltip>
        )}
      </Link>
    );
  };

  render() {
    const { myapplicationpage } = this.props;
    const { fetching, applications, fetchErr } = myapplicationpage;

    if (fetchErr) {
      return <Redirect to="/logout" />;
    }

    return (
      <div className="content-wrapper with-bg">
        <Helmet>
          <title>My Applications</title>
          <meta name="description" content="Description of My Applications" />
        </Helmet>

        <Row style={{ color: '#484859 !important' }}>
          <Col xs={2} sm={2} md={1} lg={1} xl={5}></Col>
          <Col xs={20} sm={20} md={22} lg={22} xl={14}>
            <h1
              className="text-center font-champ-bold dark-navy-blue"
              style={{ marginTop: '20px' }}
            >
              My Applications
            </h1>

            <Wrapper>
              <Card
                className="placeholder-dark card-table"
                loading={fetching}
              >
                <Table
                  loading={fetching}
                  size="middle"
                  columns={[
                    {
                      title: 'Job Title',
                      dataIndex: 'title',
                      key: 'title',
                      render: this.renderTitle,
                    },
                    {
                      title: 'Location',
                      dataIndex: 'location',
                      key: 'location',
                    },
                    {
                      title: 'Department',
                      dataIndex: 'department',
                      key: 'department',
                    },
                    {
                      align: 'center',
                      title: 'Employment',
                      dataIndex: 'employment_type',
                      key: 'employment_type',
                    },
                    {
                      align: 'center',
                      title: 'Status',
                      dataIndex: 'status',
                      key: 'status',
                    },
                  ]}
                  dataSource={applications}
                  pagination={{ pageSize: 10 }}
                />
              </Card>
            </Wrapper>
          </Col>
          <Col xs={2} sm={2} md={1} lg={1} xl={5}></Col>
        </Row>
      </div>
    );
  }
}

MyApplicationPage.propTypes = {
  myapplicationpage: PropTypes.object.isRequired,
  getApplications: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  myapplicationpage: makeSelectMyApplicationPage(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getApplications: () => dispatch(fetchApplications()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'myApplicationPage', reducer });
const withSaga = injectSaga({ key: 'myApplicationPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(MyApplicationPage);
