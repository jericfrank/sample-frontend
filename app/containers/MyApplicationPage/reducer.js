/*
 *
 * MyApplicationPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  FETCH_APPLICATIONS,
  APPLICATIONS_FETCHED,
  FETCH_APPLICATIONS_ERR,
} from './constants';

const initialState = fromJS({
  fetching: false,
  fetched: false,
  fetchErr: false,
  applications: [],
});

function myApplicationPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_APPLICATIONS:
      return state
        .set('fetching', true)
        .set('fetched', false)
        .set('fetchErr', false)
        .set('applications', []);

    case APPLICATIONS_FETCHED:
      return state
        .set('fetching', false)
        .set('fetched', true)
        .set('fetchErr', false)
        .set('applications', action.applications);

    case FETCH_APPLICATIONS_ERR:
      return state
        .set('fetching', false)
        .set('fetched', false)
        .set('fetchErr', true)
        .set('applications', []);

    default:
      return state;
  }
}

export default myApplicationPageReducer;
