import { createSelector } from 'reselect';

/**
 * Direct selector to the myApplicationPage state domain
 */
const selectMyApplicationPageDomain = (state) => state.get('myApplicationPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by MyApplicationPage
 */

const makeSelectMyApplicationPage = () => createSelector(
  selectMyApplicationPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectMyApplicationPage;
export {
  selectMyApplicationPageDomain,
};
