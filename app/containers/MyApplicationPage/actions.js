/*
 *
 * MyApplicationPage actions
 *
 */

import {
  FETCH_APPLICATIONS,
  APPLICATIONS_FETCHED,
  FETCH_APPLICATIONS_ERR,
} from './constants';

export function fetchApplications() {
  return {
    type: FETCH_APPLICATIONS,
  };
}

export function applicationsFetched(applications) {
  return {
    type: APPLICATIONS_FETCHED,
    applications,
  };
}

export function fetchApplicationsErr() {
  return {
    type: FETCH_APPLICATIONS_ERR,
  };
}
