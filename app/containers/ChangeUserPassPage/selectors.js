import { createSelector } from 'reselect';

/**
 * Direct selector to the changeUserPassPage state domain
 */
const selectChangeUserPassPageDomain = (state) => state.get('changeUserPassPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ChangeUserPassPage
 */

const makeSelectChangeUserPassPage = () => createSelector(
  selectChangeUserPassPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectChangeUserPassPage;
export {
  selectChangeUserPassPageDomain,
};
