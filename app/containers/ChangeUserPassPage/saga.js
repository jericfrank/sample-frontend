import _ from 'lodash';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { postRequest } from 'utils/request';
import { message } from 'antd';

import { CHANGE_USER_PASS } from './constants';
import { userPassChanged, changeUserPassErr } from './actions';

export function* changeUserPassword({ passwords }) {
  try {
    const res = yield call(postRequest, '/hr/applicants/changePassword', passwords);
    yield call(message.success, res.message);
    yield put(userPassChanged());
  } catch (err) {
    const error = _.result(err, 'error.new-confirm-password', err.error);
    const errorMsg = Array.isArray(error) ? _.join(error, ', ') : error;
    const msgToUser = _.chain(errorMsg)
      .replace('new-confirm-password', 'new confirm password')
      .replace('new-password', 'new password')
      .value();

    yield call(message.error, msgToUser);
    yield put(changeUserPassErr());
  }
}

// Individual exports for testing
export default function* watchChangeUserPass() {
  const watcher = yield takeLatest(CHANGE_USER_PASS, changeUserPassword);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
