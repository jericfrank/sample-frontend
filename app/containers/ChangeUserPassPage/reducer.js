/*
 *
 * ChangeUserPassPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CHANGE_USER_PASS,
  USER_PASS_CHANGED,
  CHANGE_USER_PASS_ERR,
} from './constants';

const initialState = fromJS({
  saving: false,
  saved: false,
  saveError: false,
});

function changeUserPassPageReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_USER_PASS:
      return state
        .set('saving', true)
        .set('saved', false)
        .set('saveError', false);

    case USER_PASS_CHANGED:
      return state
        .set('saving', false)
        .set('saved', true)
        .set('saveError', false);

    case CHANGE_USER_PASS_ERR:
      return state
        .set('saving', false)
        .set('saved', false)
        .set('saveError', true);

    default:
      return state;
  }
}

export default changeUserPassPageReducer;
