/*
 *
 * ChangeUserPassPage actions
 *
 */

import {
  CHANGE_USER_PASS,
  USER_PASS_CHANGED,
  CHANGE_USER_PASS_ERR,
} from './constants';

export function changeUserPass(passwords) {
  return {
    type: CHANGE_USER_PASS,
    passwords,
  };
}

export function userPassChanged() {
  return {
    type: USER_PASS_CHANGED,
  };
}

export function changeUserPassErr() {
  return {
    type: CHANGE_USER_PASS_ERR,
  };
}
