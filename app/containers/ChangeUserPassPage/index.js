/**
 *
 * ChangeUserPassPage
 *
 */

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Redirect } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Row, Col, Card, Form, Input, Icon, Divider, Button } from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import urlParams from 'utils/urlParams';
import { changeUserPass } from './actions';
import makeSelectChangeUserPassPage from './selectors';
import reducer from './reducer';
import saga from './saga';

const formItemLayout = { labelCol: { span: 6 }, wrapperCol: { span: 18 } };
const inputProps = {
  size: 'large',
  type: 'password',
  prefix: <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />,
  placeholder: '******',
};

export class ChangeUserPassPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    confirmDirty: false,
  };

  componentDidUpdate() {
    const { changeuserpasspage, form } = this.props;
    const { saved } = changeuserpasspage;

    if (saved) {
      form.resetFields();
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.changePassword(values);
      }
    });
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['new-confirm-password'], { force: true });
    }
    callback();
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    const validLength = value && value.length >= 6;
    if (validLength && value !== form.getFieldValue('new-password')) {
      callback('New Passwords did not matched');
    } else {
      callback();
    }
  }

  render() {
    const { form, changeuserpasspage } = this.props;
    const { getFieldDecorator } = form;
    const { saving, saved } = changeuserpasspage;

    const urlParamsObj = urlParams(this.props.location.search);
    const redirectRoute = _.result(urlParamsObj, 'redirect', '/');

    if (saved) {
      return <Redirect to={decodeURIComponent(redirectRoute)} />;
    }

    return (
      <div className="content-wrapper with-bg">
        <Helmet>
          <title>Change Password</title>
          <meta name="description" content="Description of Change Password" />
        </Helmet>

        <Row style={{ color: '#484859 !important' }}>
          <Col xs={1} sm={1} md={1} lg={2} xl={6}></Col>
          <Col xs={22} sm={22} md={22} lg={20} xl={12}>
            <h1
              className="text-center font-champ-bold dark-navy-blue"
              style={{ marginTop: '20px' }}
            >
              Change Password
            </h1>

            <Card>
              <Form onSubmit={this.handleSubmit} layout="horizontal">
                <Form.Item label="Current Password" {...formItemLayout}>
                  {getFieldDecorator('current-password', {
                    rules: [{
                      required: true,
                      message: 'Please input your old password',
                    }],
                  })(<Input {...inputProps} />)}
                </Form.Item>

                <Divider />

                <Form.Item label="New Password" {...formItemLayout}>
                  {getFieldDecorator('new-password', {
                    rules: [{
                      required: true, message: 'Please input your new password',
                    }, {
                      min: 6,
                      message: 'Password should be at least 6 characters',
                    }, {
                      validator: this.validateToNextPassword,
                    }],
                  })(<Input {...inputProps} />)}
                </Form.Item>
                <Form.Item label="Confirm New Password" {...formItemLayout}>
                  {getFieldDecorator('new-confirm-password', {
                    rules: [{
                      required: true,
                      message: 'Please input your confirm new password',
                    }, {
                      min: 6,
                      message: 'Password should be at least 6 characters',
                    }, {
                      validator: this.compareToFirstPassword,
                    }],
                  })(<Input {...inputProps} />)}
                </Form.Item>

                <Button
                  htmlType="submit"
                  type="primary"
                  size="large"
                  className="pull-right"
                  loading={saving}
                >
                  Save
                </Button>
                <div className="clear"></div>
              </Form>
            </Card>
          </Col>
          <Col xs={1} sm={1} md={1} lg={2} xl={6}></Col>
        </Row>
      </div>
    );
  }
}

ChangeUserPassPage.propTypes = {
  form: PropTypes.object.isRequired,
  changePassword: PropTypes.func.isRequired,
  changeuserpasspage: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  changeuserpasspage: makeSelectChangeUserPassPage(),
});

export function mapDispatchToProps(dispatch) {
  return {
    changePassword: (passwords) => dispatch(changeUserPass(passwords)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'changeUserPassPage', reducer });
const withSaga = injectSaga({ key: 'changeUserPassPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Form.create()(ChangeUserPassPage));
