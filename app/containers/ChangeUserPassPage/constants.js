/*
 *
 * ChangeUserPassPage constants
 *
 */

export const CHANGE_USER_PASS = 'app/ChangeUserPassPage/CHANGE_USER_PASS';
export const USER_PASS_CHANGED = 'app/ChangeUserPassPage/USER_PASS_CHANGED';
export const CHANGE_USER_PASS_ERR = 'app/ChangeUserPassPage/CHANGE_USER_PASS_ERR';
