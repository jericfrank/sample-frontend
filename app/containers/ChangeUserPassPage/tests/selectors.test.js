import { fromJS } from 'immutable';

import makeSelectChangeUserPassPage, { selectChangeUserPassPageDomain } from '../selectors';

describe('selectChangeUserPassPageDomain', () => {
  it('should select `changeUserPassPage`', () => {
    const state = fromJS({
      changeUserPassPage: {
        test: 'test',
      },
    });
    expect(selectChangeUserPassPageDomain(state))
      .toEqual(state.get('changeUserPassPage'));
  });
});

describe('makeSelectChangeUserPassPage', () => {
  it('should select `changeUserPassPage`', () => {
    const changeUserPassPage = fromJS({
      test: 'test',
    });
    const mockedState = fromJS({
      changeUserPassPage,
    });
    expect(makeSelectChangeUserPassPage()(mockedState))
      .toEqual(changeUserPassPage.toJS());
  });
});
