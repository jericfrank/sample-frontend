import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'antd';

import { changeUserPass } from '../actions';
import { ChangeUserPassPage, mapDispatchToProps } from '../index';

describe('<ChangeUserPassPage />', () => {
  let props;

  beforeEach(() => {
    props = {
      changePassword: jest.fn(),
      form: {
        getFieldDecorator: () => jest.fn(),
        validateFields: jest.fn(),
        resetFields: jest.fn(),
      },
      changeuserpasspage: {
        saving: false,
        saved: false,
      },
      location: {
        search: 'SEARCH',
      },
    };
  });

  it('should display `ChangeUserPassPage`', () => {
    const wrapper = shallow(<ChangeUserPassPage {...props} />);
    expect(wrapper.find(Form)).toHaveLength(1);
    expect(wrapper.find(Form.Item)).toHaveLength(3);
  });

  it('should `resetFields` on update if form is saved', () => {
    props = {
      ...props,
      changeuserpasspage: {
        ...props.changeuserpasspage,
        saved: true,
      },
    };
    const wrapper = shallow(<ChangeUserPassPage {...props} />);
    wrapper.instance().componentDidUpdate();
    expect(props.form.resetFields).toHaveBeenCalledTimes(1);
  });

  it('should call `changePassword` if form is VALID on `handleSubmit`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false),
      },
    };
    const wrapper = shallow(<ChangeUserPassPage {...props} />);
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.changePassword).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `changePassword` if form is INVALID on `handleSubmit`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(true),
      },
    };
    const wrapper = shallow(<ChangeUserPassPage {...props} />);
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.changePassword).toHaveBeenCalledTimes(0);
  });

  it('should call `handleConfirmBlur`', () => {
    const wrapper = shallow(<ChangeUserPassPage {...props} />);
    wrapper.instance().handleConfirmBlur({ target: { value: 'test' } });
    expect(wrapper.state().confirmDirty).toBe(true);
  });

  it('should call `validateToNextPassword`', () => {
    const wrapper = shallow(<ChangeUserPassPage {...props} />);
    wrapper.setState({ confirmDirty: 'test' });
    wrapper.instance().validateToNextPassword(null, 'test', jest.fn());
    expect(props.form.validateFields).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `validateToNextPassword`', () => {
    const wrapper = shallow(<ChangeUserPassPage {...props} />);
    wrapper.setState({ confirmDirty: false });
    wrapper.instance().validateToNextPassword(null, 'test', jest.fn());
    expect(props.form.validateFields).toHaveBeenCalledTimes(0);
  });

  it('should call `compareToFirstPassword`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        getFieldValue: jest.fn(),
      },
    };
    const wrapper = shallow(<ChangeUserPassPage {...props} />);
    wrapper.instance().compareToFirstPassword(null, 'test', jest.fn());
    wrapper.instance().compareToFirstPassword(null, 'test password', jest.fn());
    expect(props.form.getFieldValue).toHaveBeenCalledTimes(1);
  });

  describe('mapDispatchToProps', () => {
    describe('changePassword', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.changePassword).toBeDefined();
      });

      it('should dispatch changeUserPass when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const passwords = {};
        result.changePassword(passwords);
        expect(dispatch).toHaveBeenCalledWith(changeUserPass(passwords));
      });
    });
  });
});
