/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { put, takeLatest, take, cancel } from 'redux-saga/effects';
import { createMockTask } from 'redux-saga/utils';
import { LOCATION_CHANGE } from 'react-router-redux';

import { CHANGE_USER_PASS } from '../constants';
import { userPassChanged, changeUserPassErr } from '../actions';
import watchChangeUserPass, { changeUserPassword } from '../saga';

const action = {
  passwords: {
    'current-password': 'secret',
    'new-password': 'secret123',
    'new-confirm-password': 'secret123',
  },
};

const success = {
  message: 'Success!',
};

describe('changeUserPassword Saga', () => {
  let changeUserPasswordGenerator;

  // We have to test twice, once for a successful load and once for an unsuccessful one
  // so we do all the stuff that happens beforehand automatically in the beforeEach
  beforeEach(() => {
    changeUserPasswordGenerator = changeUserPassword(action);

    const requestDescriptor = changeUserPasswordGenerator.next(success).value;
    expect(requestDescriptor).toMatchSnapshot();

    const antdMsgDescriptor = changeUserPasswordGenerator.next(success).value;
    expect(antdMsgDescriptor).toMatchSnapshot();
  });

  it('should dispatch the jobOpeningsFetched action if it requests the data successfully', () => {
    const putDescriptor = changeUserPasswordGenerator.next().value;
    expect(putDescriptor).toEqual(put(userPassChanged()));
  });

  describe('catch wrong current password', () => {
    beforeEach(() => {
      const error = {
        error: 'Error',
      };
      const callDescriptor = changeUserPasswordGenerator.throw(error).value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the changeUserPassErr action if the response errors', () => {
      const putDescriptor = changeUserPasswordGenerator.next().value;
      expect(putDescriptor).toEqual(put(changeUserPassErr()));
    });
  });

  describe('catch wrong new-confirm-password', () => {
    beforeEach(() => {
      const error = {
        error: {
          'new-confirm-password': ['Error'],
        },
      };
      const callDescriptor = changeUserPasswordGenerator.throw(error).value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the changeUserPassErr action if the response errors', () => {
      const putDescriptor = changeUserPasswordGenerator.next().value;
      expect(putDescriptor).toEqual(put(changeUserPassErr()));
    });
  });
});

describe('watchChangeUserPass Saga', () => {
  const generator = watchChangeUserPass();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const expectedYield = takeLatest(CHANGE_USER_PASS, changeUserPassword);
    expect(generator.next().value).toEqual(expectedYield);
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
