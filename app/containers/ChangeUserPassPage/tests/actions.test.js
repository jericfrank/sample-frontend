
import {
  changeUserPass,
  userPassChanged,
  changeUserPassErr,
} from '../actions';
import {
  CHANGE_USER_PASS,
  USER_PASS_CHANGED,
  CHANGE_USER_PASS_ERR,
} from '../constants';

describe('ChangeUserPassPage actions', () => {
  describe('changeUserPass', () => {
    it('has a type of CHANGE_USER_PASS', () => {
      const passwords = {};
      const expected = {
        type: CHANGE_USER_PASS,
        passwords,
      };
      expect(changeUserPass(passwords)).toEqual(expected);
    });
  });

  describe('userPassChanged', () => {
    it('has a type of USER_PASS_CHANGED', () => {
      const expected = {
        type: USER_PASS_CHANGED,
      };
      expect(userPassChanged()).toEqual(expected);
    });
  });

  describe('changeUserPassErr', () => {
    it('has a type of CHANGE_USER_PASS_ERR', () => {
      const expected = {
        type: CHANGE_USER_PASS_ERR,
      };
      expect(changeUserPassErr()).toEqual(expected);
    });
  });
});
