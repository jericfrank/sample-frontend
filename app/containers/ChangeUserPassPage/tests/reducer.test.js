
import { fromJS } from 'immutable';
import {
  changeUserPass,
  userPassChanged,
  changeUserPassErr,
} from '../actions';
import changeUserPassPageReducer from '../reducer';

describe('changeUserPassPageReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({
      saving: false,
      saved: false,
      saveError: false,
    });
  });

  it('returns the initial state', () => {
    expect(changeUserPassPageReducer(undefined, {})).toEqual(state);
  });

  it('should handle the changeUserPass action correctly', () => {
    const expectedResult = state
      .set('saving', true)
      .set('saved', false)
      .set('saveError', false);
    expect(changeUserPassPageReducer(state, changeUserPass()))
      .toEqual(expectedResult);
  });

  it('should handle the userPassChanged action correctly', () => {
    const expectedResult = state
      .set('saving', false)
      .set('saved', true)
      .set('saveError', false);
    expect(changeUserPassPageReducer(state, userPassChanged()))
      .toEqual(expectedResult);
  });

  it('should handle the changeUserPassErr action correctly', () => {
    const expectedResult = state
      .set('saving', false)
      .set('saved', false)
      .set('saveError', true);
    expect(changeUserPassPageReducer(state, changeUserPassErr()))
      .toEqual(expectedResult);
  });
});
