import _ from 'lodash';
import CryptoJS from 'crypto-js';
import { take, call, put, cancel, takeLatest, select, all } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { postRequest, getRequest } from 'utils/request';
import { message } from 'antd';

import { setUserAuth } from 'containers/App/actions';
import { makeSelectUser } from 'containers/App/selectors';
import { makeSelectJobPage } from 'containers/JobPage/selectors';
import { fetchedJobDetails, displayFetchErr } from 'containers/JobPage/actions';
import { applyJobSuccess, applyJobError, applyJobReset, applyJob, applyJobLoading } from './actions';
import { APPLY_JOB, APPLY_JOB_DETAILS } from './constants';

export function* applyJobSaga({ formValues }) {
  try {
    const { jobDetails } = yield select(makeSelectJobPage());
    const { id, job_question } = jobDetails;
    const answers = _.map(formValues, (value, index) => {
      let answer = value;
      if (_.isArray(answer)) {
        answer = answer.join(', ');
      }
      return {
        answer,
        question: _.chain(job_question)
          .flatten()
          .find(['id', _.parseInt(index, 10)])
          .get('question', null)
          .value(),
      };
    });
    const response = yield call(postRequest, '/hr/applicants/apply', {
      job_id: id,
      answers,
    });

    // reset applications in token
    const details = _.omit(yield select(makeSelectUser()), ['applications']);
    const applications = yield call(getRequest, '/hr/applicants/job_applications');
    const userToken = { ...details, applications };

    const encrypt = yield call(CryptoJS.AES.encrypt, JSON.stringify(userToken), localStorage.getItem('jobportalToken'));
    localStorage.setItem('jobportalUser', encrypt);

    yield call(message.success, 'Successfully applied for this job. Please check your email.');
    yield all([
      put(setUserAuth(userToken)),
      put(applyJobSuccess(response)),
      put(applyJobReset()),
    ]);
  } catch (err) {
    yield put(applyJobError(err));
  }
}

export function* applyJobDetails({ id }) {
  try {
    yield put(applyJobLoading());
    const response = yield call(getRequest, `/hr/applicants/job_openings/${id}`);
    if (_.isEmpty(response.job_question)) {
      yield put(applyJob([]));
    } else {
      let questions = _.chunk(_.remove(response.job_question, (q, index) => index !== 0), 5);
      const defaultQuestion = _.pullAt(response.job_question, 0);
      questions = [...questions, defaultQuestion];
      yield put(applyJobReset());
      yield put(fetchedJobDetails({ ...response, job_question: questions }));
    }
  } catch (err) {
    yield put(displayFetchErr(err));
  }
}

export default function* watchApplyJob() {
  const watcher = yield all([
    takeLatest(APPLY_JOB, applyJobSaga),
    takeLatest(APPLY_JOB_DETAILS, applyJobDetails),
  ]);

  yield take(LOCATION_CHANGE);
  yield cancel(...watcher);
}
