/**
 *
 * ApplyPage
 *
 */

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Card, Row, Col, Form, Modal, message } from 'antd';
import FontAwesome from 'react-fontawesome';
import { Redirect } from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import SpinPageLoader from 'components/SpinPageLoader';
import Questionnaire from 'components/Questionnaire';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { makeSelectUser } from 'containers/App/selectors';
import makeSelectJobPage from 'containers/JobPage/selectors';
import jobPageReducer from 'containers/JobPage/reducer';
import jobPageSaga from 'containers/JobPage/saga';
import ProfileAttachments from 'containers/ProfileAttachments';
import profilePageReducer from 'containers/ProfilePage/reducer';
import makeSelectProfilePage from 'containers/ProfilePage/selectors';
import profilePageSaga from 'containers/ProfilePage/saga';
import { saveAttachments } from 'containers/ProfilePage/actions';
import { HAS_FAILED_UPLOAD_MSG } from 'containers/ProfilePage/constants';

import makeSelectApplyPage from './selectors';
import { applyJob, applyJobReset, applyJobDetails } from './actions';
import reducer from './reducer';
import saga from './saga';

export class ApplyPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    toggleUploadView: false,
    defaultPayload: {},
    hasUploadError: false,
  };

  componentDidMount() {
    this.props.getJobDetails(this.props.match.params.id);
  }

  onUploadStatusChange = (hasUploadError) => {
    this.setState({ hasUploadError });
  }

  handleBack = () => {
    this.props.applyJobReset();
    this.props.history.goBack();
  };

  handleOk = () => {
    const { form, onSaveAttachments, currentUser } = this.props;
    form.validateFields((err, values) => {
      const { hasUploadError, defaultPayload } = this.state;

      if (hasUploadError) {
        message.error(HAS_FAILED_UPLOAD_MSG);
      }

      if (!err && !hasUploadError) {
        onSaveAttachments(currentUser, values, defaultPayload);
      }
    });
  }

  handleCancel = () => {
    const { saving } = this.props.profilepage;
    if (!saving) {
      this.setState({ toggleUploadView: false });
    }
  }

  handleSubmitApply = (payload, toggleUploadView) => {
    if (!toggleUploadView) {
      this.props.submitApply(payload);
    } else {
      const defaultPayload = payload;
      this.setState({ defaultPayload, toggleUploadView });
    }
  }

  renderProfileAttachments = () => {
    const { currentUser, form } = this.props;
    const { getFieldDecorator, setFieldsValue } = form;
    return (
      <ProfileAttachments
        user={currentUser}
        getFieldDecorator={getFieldDecorator}
        setFieldsValue={setFieldsValue}
        changeUploadStatus={this.onUploadStatusChange}
      />
    );
  }

  render() {
    const { id } = this.props.match.params;
    const { jobpage, applypage, match, currentUser, profilepage } = this.props;
    const { jobDetails, fetched, fetchErr } = jobpage;
    const { applyJobSubmit, applyJobError, applyJobSuccess, applyJobLoading, messageToUser } = applypage;
    const { saving, saved } = profilepage;

    const applied = _.some(currentUser ? currentUser.applications : [], { job_id: id.toString() });
    const noDocs = _.result(currentUser, 'app_letter_id', null) === null || _.result(currentUser, 'cv_id', null) === null;

    if (fetchErr) {
      return <NotFoundPage />;
    }

    if (applyJobLoading && !_.result(jobDetails, 'job_question', []).length) {
      return <SpinPageLoader label="Submitting..." />;
    }

    if (applyJobSuccess || applied || saved) {
      return <Redirect to={`/jobs/${match.params.id}`} />;
    }

    return (
      <div className="content-wrapper with-bg">
        <Helmet>
          <title>Apply Page</title>
          <meta name="description" content="Description of Apply Page" />
        </Helmet>
        <Row>
          <Col xs={1} sm={1} md={1} lg={2} xl={6}></Col>
          <Col xs={22} sm={22} md={22} lg={20} xl={12} style={{ margin: 'auto', marginTop: '20px' }}>
            <Row>
              <Col span={2}>
                <a className="back-button" role="button" tabIndex="0" onClick={this.handleBack}>
                  <FontAwesome name="angle-left" /> Back
                </a>
              </Col>
              <Col span={20}>
                <h1 className="text-center font-champ-bold dark-navy-blue">
                  {jobDetails.title ? `Application for ${jobDetails.title}` : ''}
                </h1>
              </Col>
              <Col span={2}></Col>
            </Row>
            {fetched ? (
              <Questionnaire
                noDocs={noDocs}
                error={applyJobError}
                success={applyJobSuccess}
                msg={messageToUser}
                loading={applyJobSubmit}
                questions={jobDetails.job_question}
                perPage={5}
                onSubmit={this.handleSubmitApply}
                defaultPayload={this.state.defaultPayload}
              />
            ) : (
              <Card loading />
            )}
          </Col>
          <Col xs={1} sm={1} md={1} lg={2} xl={6}></Col>
        </Row>

        <Modal
          title="Your attachments"
          visible={this.state.toggleUploadView}
          onOk={this.handleOk}
          okText="Save"
          okButtonProps={{ disabled: saving }}
          onCancel={this.handleCancel}
          confirmLoading={saving}
          closable={false}
          maskClosable={false}
          keyboard={false}
        >
          {this.renderProfileAttachments()}
        </Modal>
      </div>
    );
  }
}

ApplyPage.propTypes = {
  match: PropTypes.object.isRequired,
  getJobDetails: PropTypes.func.isRequired,
  applypage: PropTypes.object.isRequired,
  jobpage: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  submitApply: PropTypes.func.isRequired,
  applyJobReset: PropTypes.func.isRequired,
  currentUser: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  onSaveAttachments: PropTypes.func.isRequired,
  profilepage: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  applypage: makeSelectApplyPage(),
  jobpage: makeSelectJobPage(),
  currentUser: makeSelectUser(),
  profilepage: makeSelectProfilePage(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getJobDetails: (id) => dispatch(applyJobDetails(id)),
    submitApply: (payload) => dispatch(applyJob(payload)),
    applyJobReset: () => dispatch(applyJobReset()),
    onSaveAttachments: (user, attachments, answers) => {
      const appLetter = _.result(user, 'app_letter.name', null);
      const cv = _.result(user, 'cv.name', null);

      dispatch(saveAttachments({
        first_name: _.result(user, 'first_name', null),
        last_name: _.result(user, 'last_name', null),
        location: _.result(user, 'address', null),
        email: _.result(user, 'email', null),
        mobile_no: _.result(user, 'mobile_no', null),
        application_letter: _.result(attachments, 'application_letter', appLetter),
        cv_resume: _.result(attachments, 'cv_resume', cv),
      }, answers));
    },
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const jobPageWithReducer = injectReducer({ key: 'jobPage', reducer: jobPageReducer });
const jobPageWithSaga = injectSaga({ key: 'jobPage', saga: jobPageSaga });

const profileWithReducer = injectReducer({ key: 'profilePage', reducer: profilePageReducer });
const profileWithSaga = injectSaga({ key: 'profilePage', saga: profilePageSaga });

const withReducer = injectReducer({ key: 'applyPage', reducer });
const withSaga = injectSaga({ key: 'applyPage', saga });

export default compose(
  jobPageWithReducer,
  jobPageWithSaga,
  profileWithReducer,
  profileWithSaga,
  withReducer,
  withSaga,
  withConnect,
)(Form.create()(ApplyPage));
