/*
 *
 * ApplyPage constants
 *
 */

export const APPLY_JOB = 'app/ApplyPage/APPLY_JOB';
export const APPLY_JOB_SUCCESS = 'app/ApplyPage/APPLY_JOB_SUCCESS';
export const APPLY_JOB_ERROR = 'app/ApplyPage/APPLY_JOB_ERROR';
export const APPLY_JOB_RESET = 'app/ApplyPage/APPLY_JOB_RESET';
export const APPLY_JOB_DETAILS = 'app/ApplyPage/APPLY_JOB_DETAILS';
export const APPLY_JOB_LOADING = 'app/ApplyPage/APPLY_JOB_LOADING';
