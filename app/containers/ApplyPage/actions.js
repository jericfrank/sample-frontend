/*
 *
 * ApplyPage actions
 *
 */

import {
  APPLY_JOB,
  APPLY_JOB_SUCCESS,
  APPLY_JOB_ERROR,
  APPLY_JOB_RESET,
  APPLY_JOB_DETAILS,
  APPLY_JOB_LOADING,
} from './constants';

export function applyJob(formValues) {
  return {
    type: APPLY_JOB,
    formValues,
  };
}

export function applyJobSuccess(success) {
  return {
    type: APPLY_JOB_SUCCESS,
    success,
  };
}

export function applyJobError(error) {
  return {
    type: APPLY_JOB_ERROR,
    error,
  };
}

export function applyJobReset() {
  return {
    type: APPLY_JOB_RESET,
  };
}

export function applyJobDetails(id) {
  return {
    type: APPLY_JOB_DETAILS,
    id,
  };
}

export function applyJobLoading() {
  return {
    type: APPLY_JOB_LOADING,
  };
}
