/*
 *
 * ApplyPage reducer
 *
 */

import _ from 'lodash';
import { fromJS } from 'immutable';
import {
  APPLY_JOB,
  APPLY_JOB_SUCCESS,
  APPLY_JOB_ERROR,
  APPLY_JOB_RESET,
  APPLY_JOB_LOADING,
} from './constants';

const initialState = fromJS({
  applyJobSubmit: false,
  applyJobSuccess: false,
  applyJobError: false,
  applyJobLoading: false,
  messageToUser: '',
});

function applyPageReducer(state = initialState, action) {
  switch (action.type) {
    case APPLY_JOB:
      return state
        .set('applyJobSubmit', true)
        .set('applyJobSuccess', false)
        .set('applyJobError', false)
        .set('applyJobLoading', true)
        .set('messageToUser', '');

    case APPLY_JOB_SUCCESS:
      return state
        .set('applyJobSubmit', false)
        .set('applyJobSuccess', action.success)
        .set('applyJobError', false)
        .set('applyJobLoading', false)
        .set('messageToUser', 'Successfully apply for this job. Please check your email.');

    case APPLY_JOB_ERROR:
      return state
        .set('applyJobSubmit', false)
        .set('applyJobSuccess', false)
        .set('applyJobError', action.error)
        .set('applyJobLoading', false)
        .set('messageToUser', _.result(action, 'error.error.message', 'Something went wrong. Please try again later.'));

    case APPLY_JOB_RESET:
      return state
        .set('applyJobSubmit', false)
        .set('applyJobSuccess', false)
        .set('applyJobError', false)
        .set('applyJobLoading', false)
        .set('messageToUser', '');

    case APPLY_JOB_LOADING:
      return state
        .set('applyJobLoading', true);

    default:
      return state;
  }
}

export default applyPageReducer;
