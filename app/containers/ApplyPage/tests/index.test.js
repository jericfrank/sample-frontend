import React from 'react';
import { Redirect } from 'react-router-dom';
import { shallow } from 'enzyme';
import { Card, message } from 'antd';

import SpinPageLoader from 'components/SpinPageLoader';
import Questionnaire from 'components/Questionnaire';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { saveAttachments } from 'containers/ProfilePage/actions';
import { applyJob, applyJobReset, applyJobDetails } from '../actions';
import { ApplyPage, mapDispatchToProps } from '../index';

describe('<ApplyPage />', () => {
  let props;

  beforeEach(() => {
    props = {
      getJobDetails: jest.fn(),
      applyJobReset: jest.fn(),
      submitApply: jest.fn(),
      onSaveAttachments: jest.fn(),
      match: {
        params: {
          id: 1,
        },
      },
      jobpage: {
        jobDetails: { id: 1 },
        fetched: false,
        fetchErr: false,
      },
      applypage: {
        applyJobSubmit: jest.fn(),
        applyJobError: false,
        applyJobSuccess: false,
        messageToUser: '',
      },
      profilepage: {
        saving: false,
        saved: false,
      },
      history: {
        goBack: jest.fn(),
      },
      currentUser: {
        app_letter_id: 1,
        cv_id: null,
      },
      form: {
        getFieldDecorator: () => jest.fn(),
        validateFields: () => jest.fn(),
      },
    };
  });

  it('should render `ApplyPage` and display `Card`', () => {
    const wrapper = shallow(<ApplyPage {...props} />);
    expect(wrapper.contains(<Card loading />)).toBeTruthy();
  });

  it('should `Redirect` the user to the previous page', () => {
    props = {
      ...props,
      applypage: { ...props.applypage, applyJobSuccess: true },
    };
    const wrapper = shallow(<ApplyPage {...props} />);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

  it('should render `ApplyPage` and display `Job` title', () => {
    props = {
      ...props,
      jobpage: {
        ...props.jobpage,
        jobDetails: { id: 1, title: 'Title' },
      },
    };
    const wrapper = shallow(<ApplyPage {...props} />);
    expect(wrapper.contains(<Card loading />)).toBeTruthy();
  });

  it('should display `Questionnaire` after fetch', () => {
    props = {
      ...props,
      jobpage: {
        ...props.jobpage,
        fetched: true,
      },
    };
    const wrapper = shallow(<ApplyPage {...props} />);
    expect(wrapper.find(Questionnaire)).toHaveLength(1);
  });

  it('should call `getJobDetails` after mount', () => {
    const wrapper = shallow(<ApplyPage {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.getJobDetails).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `getJobDetails` after mount if `jobpage.fetched` is true', () => {
    props = {
      ...props,
      jobpage: {
        ...props.jobpage,
        fetched: true,
      },
    };
    const wrapper = shallow(<ApplyPage {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.getJobDetails).toHaveBeenCalledTimes(1);
  });

  it('should render `NotFoundPage` if `jobpage.fetchErr` is true', () => {
    props = {
      ...props,
      jobpage: {
        ...props.jobpage,
        fetchErr: true,
      },
    };
    const wrapper = shallow(<ApplyPage {...props} />);
    expect(wrapper.contains(<NotFoundPage />)).toBeTruthy();
  });

  it('should call `applyJobReset` and `goBack` on `handleBack`', () => {
    const wrapper = shallow(<ApplyPage {...props} />);
    wrapper.instance().handleBack();
    expect(props.applyJobReset).toHaveBeenCalledTimes(1);
    expect(props.history.goBack).toHaveBeenCalledTimes(1);
  });

  it('should display `SpinPageLoader`', () => {
    props = {
      ...props,
      applypage: {
        ...props.applypage,
        applyJobLoading: true,
      },
    };
    const wrapper = shallow(<ApplyPage {...props} />);
    expect(wrapper.find(SpinPageLoader)).toHaveLength(1);
  });

  it('should `onUploadStatusChange`', () => {
    const wrapper = shallow(<ApplyPage {...props} />);
    expect(wrapper.state('hasUploadError')).toEqual(false);
    wrapper.instance().onUploadStatusChange(true);
    expect(wrapper.state('hasUploadError')).toEqual(true);
  });

  it('should `toggleUploadView` `onUploadStatusChange`', () => {
    const wrapper = shallow(<ApplyPage {...props} />);
    wrapper.setState({ toggleUploadView: true });
    expect(wrapper.state('toggleUploadView')).toEqual(true);
    wrapper.instance().handleCancel();
    expect(wrapper.state('toggleUploadView')).toEqual(false);
  });

  it('should NOT `toggleUploadView` `onUploadStatusChange` when saving', () => {
    props = {
      ...props,
      profilepage: { ...props.profilepage, saving: true },
    };
    delete props.currentUser;
    const wrapper = shallow(<ApplyPage {...props} />);
    wrapper.setState({ toggleUploadView: true });
    expect(wrapper.state('toggleUploadView')).toEqual(true);
    wrapper.instance().handleCancel();
    expect(wrapper.state('toggleUploadView')).toEqual(true);
  });

  it('should invoke `submitApply` on `handleSubmitApply`', () => {
    const wrapper = shallow(<ApplyPage {...props} />);
    wrapper.instance().handleSubmitApply({});
    expect(props.submitApply).toHaveBeenCalledTimes(1);
  });

  it('should set `defaultPayload` and `toggleUploadView`', () => {
    const test = { test: 'test' };
    const wrapper = shallow(<ApplyPage {...props} />);
    wrapper.instance().handleSubmitApply(test, true);
    expect(wrapper.state('defaultPayload')).toEqual(test);
    expect(wrapper.state('toggleUploadView')).toEqual(true);
  });

  it('should call `onSaveAttachments` if upload has NO error', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false, {}),
      },
    };
    const wrapper = shallow(<ApplyPage {...props} />);
    wrapper.instance().handleOk();
    expect(props.onSaveAttachments).toHaveBeenCalledTimes(1);
  });

  it('should call `message.error` if upload has error', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(true),
      },
    };
    const spy = jest.spyOn(message, 'error');
    const wrapper = shallow(<ApplyPage {...props} />);
    wrapper.setState({ hasUploadError: true });
    wrapper.instance().handleOk();
    expect(spy).toHaveBeenCalledTimes(1);
    message.error.mockReset();
    message.error.mockRestore();
  });

  describe('mapDispatchToProps', () => {
    describe('getJobDetails', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.getJobDetails).toBeDefined();
      });

      it('should dispatch fetchJobDetails when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const id = 1;
        result.getJobDetails(id);
        expect(dispatch).toHaveBeenCalledWith(applyJobDetails(id));
      });
    });

    describe('submitApply', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.submitApply).toBeDefined();
      });

      it('should dispatch applyJob when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const payload = {};
        result.submitApply(payload);
        expect(dispatch).toHaveBeenCalledWith(applyJob(payload));
      });
    });

    describe('applyJobReset', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.applyJobReset).toBeDefined();
      });

      it('should dispatch applyJobReset when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.applyJobReset();
        expect(dispatch).toHaveBeenCalledWith(applyJobReset());
      });
    });

    describe('onSaveAttachments', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onSaveAttachments).toBeDefined();
      });

      it('should dispatch onSaveAttachments when called', () => {
        const user = { email: 'email@email.com' };
        const attachments = { application_letter: 1, cv_resume: 2 };
        const answers = { 9: 1 };
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.onSaveAttachments(user, attachments, answers);
        expect(dispatch).toHaveBeenCalledWith(saveAttachments({
          first_name: null,
          last_name: null,
          location: null,
          email: user.email,
          mobile_no: null,
          application_letter: attachments.application_letter,
          cv_resume: attachments.cv_resume,
        }, answers));
      });
    });
  });
});
