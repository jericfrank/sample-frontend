
import { fromJS } from 'immutable';
import {
  applyJob,
  applyJobSuccess,
  applyJobError,
  applyJobReset,
  applyJobLoading,
} from '../actions';
import applyPageReducer from '../reducer';

describe('applyPageReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({
      applyJobSubmit: false,
      applyJobSuccess: false,
      applyJobError: false,
      applyJobLoading: false,
      messageToUser: '',
    });
  });

  it('returns the initial state', () => {
    expect(applyPageReducer(undefined, {})).toEqual(fromJS(state));
  });

  it('should handle the applyJob action correctly', () => {
    const expectedResult = state
      .set('applyJobSubmit', true)
      .set('applyJobSuccess', false)
      .set('applyJobError', false)
      .set('applyJobLoading', true)
      .set('messageToUser', '');
    expect(applyPageReducer(state, applyJob()))
      .toEqual(expectedResult);
  });

  it('should handle the applyJobSuccess action correctly', () => {
    const success = true;
    const expectedResult = state
      .set('applyJobSubmit', false)
      .set('applyJobSuccess', success)
      .set('applyJobError', false)
      .set('messageToUser', 'Successfully apply for this job. Please check your email.');
    expect(applyPageReducer(state, applyJobSuccess(success)))
      .toEqual(expectedResult);
  });

  it('should handle the applyJobError action correctly', () => {
    const errMsg = {
      error: {
        message: 'Error message here',
      },
    };
    const expectedResult = state
      .set('applyJobSubmit', false)
      .set('applyJobSuccess', false)
      .set('applyJobError', errMsg)
      .set('messageToUser', errMsg.error.message);
    expect(applyPageReducer(state, applyJobError(errMsg)))
      .toEqual(expectedResult);
  });

  it('should handle the applyJobReset action correctly', () => {
    const expectedResult = state
      .set('applyJobSubmit', false)
      .set('applyJobSuccess', false)
      .set('applyJobError', false)
      .set('messageToUser', '');
    expect(applyPageReducer(state, applyJobReset()))
      .toEqual(expectedResult);
  });

  it('should handle the applyJobLoading action correctly', () => {
    const expectedResult = state
      .set('applyJobLoading', true);
    expect(applyPageReducer(state, applyJobLoading()))
      .toEqual(expectedResult);
  });
});
