/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { take, takeLatest, put, cancel, all } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';

import { setUserAuth } from 'containers/App/actions';
import { fetchedJobDetails, displayFetchErr } from 'containers/JobPage/actions';
import { applyJobSuccess, applyJobError, applyJobReset, applyJob, applyJobLoading } from '../actions';
import watchApplyJob, { applyJobSaga, applyJobDetails } from '../saga';
import { APPLY_JOB, APPLY_JOB_DETAILS } from '../constants';

describe('applyJob Saga', () => {
  let createGenerator;

  const action = {
    formValues: {
      1: 'Answer!',
      2: ['Answer', 'Answer'],
    },
  };

  const selector = {
    jobDetails: {
      id: 1,
      job_question: [{
        id: 1,
        question: 'Question?',
      }, {
        id: 2,
        question: 'Question?',
      }],
    },
  };

  const userSelect = { id: 1 };
  const applications = [];
  const encrypt = 'ENCRYPT';

  beforeEach(() => {
    class LocalStorageMock {
      constructor() {
        this.store = {};
      }

      clear() {
        this.store = {};
      }

      getItem(key) {
        return this.store[key] || null;
      }

      setItem(key, value) {
        this.store[key] = value.toString();
      }

      removeItem(key) {
        delete this.store[key];
      }
    }
    global.localStorage = new (LocalStorageMock)();

    createGenerator = applyJobSaga(action);

    const selectDescriptor = createGenerator.next(selector).value;
    expect(selectDescriptor).toMatchSnapshot();

    const requestDescriptor = createGenerator.next(selector).value;
    expect(requestDescriptor).toMatchSnapshot();

    const userSelectDescriptor = createGenerator.next(userSelect).value;
    expect(userSelectDescriptor).toMatchSnapshot();

    const applicationsDescriptor = createGenerator.next(applications).value;
    expect(applicationsDescriptor).toMatchSnapshot();

    const jwtTokenCallDescriptor = createGenerator.next(encrypt).value;
    expect(jwtTokenCallDescriptor).toMatchSnapshot();

    const antdMsgDescriptor = createGenerator.next(encrypt).value;
    expect(antdMsgDescriptor).toMatchSnapshot();
  });

  it('should call the setUserAuth, applyJobSuccess, applyJobReset action if requests successfully', () => {
    const response = { ...userSelect, applications };
    const descriptor = createGenerator.next(response).value;
    expect(descriptor).toEqual(all([
      put(setUserAuth({ applications: encrypt })),
      put(applyJobSuccess(userSelect)),
      put(applyJobReset()),
    ]));
  });

  it('should call the applyJobError action if the response errors', () => {
    const response = new Error('Some error');
    const descriptor = createGenerator.throw(response).value;
    expect(descriptor).toEqual(put(applyJobError(response)));
  });
});

describe('applyJobDetails Saga', () => {
  let createGenerator;
  let job;

  beforeEach(() => {
    job = { id: 1 };

    createGenerator = applyJobDetails(job);
  });

  it('should call applyJobLoading action if requests successfully', () => {
    const descriptor = createGenerator.next().value;
    expect(descriptor).toEqual(put(applyJobLoading()));
  });

  describe('after `applyJobLoading` called WITHOUT `job_question`', () => {
    let response;

    beforeEach(() => {
      response = { job_question: {} };

      const selectDescriptor = createGenerator.next(job).value;
      expect(selectDescriptor).toMatchSnapshot();

      const descriptor = createGenerator.next(job).value;
      expect(descriptor).toMatchSnapshot();
    });

    it('should call applyJob action if requests successfully', () => {
      const descriptor = createGenerator.next(response).value;
      expect(descriptor).toEqual(put(applyJob([])));
    });
  });

  describe('after `applyJobLoading` called WITH `job_question`', () => {
    let response;
    const question = { value: 'test' };

    beforeEach(() => {
      response = { job_question: [question] };

      const selectDescriptor = createGenerator.next(job).value;
      expect(selectDescriptor).toMatchSnapshot();

      const descriptor = createGenerator.next(job).value;
      expect(descriptor).toMatchSnapshot();
    });

    it('should call applyJobReset action if requests successfully', () => {
      const descriptor = createGenerator.next(response).value;
      expect(descriptor).toEqual(put(applyJobReset()));
    });

    it('should call fetchedJobDetails action if requests successfully', () => {
      createGenerator.next(response);
      const descriptor = createGenerator.next(response).value;
      expect(descriptor).toEqual(put(fetchedJobDetails({
        ...response,
        job_question: [[question]],
      })));
    });

    it('should call the displayFetchErr action if the response errors', () => {
      response = new Error('Some error');
      const descriptor = createGenerator.throw(response).value;
      expect(descriptor).toEqual(put(displayFetchErr(response)));
    });
  });
});

describe('watchApplyJob Saga', () => {
  const generator = watchApplyJob();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const expectedYield = all([
      takeLatest(APPLY_JOB, applyJobSaga),
      takeLatest(APPLY_JOB_DETAILS, applyJobDetails),
    ]);
    expect(generator.next().value).toEqual(expectedYield);
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(...mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
