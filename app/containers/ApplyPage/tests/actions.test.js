
import {
  applyJob,
  applyJobSuccess,
  applyJobError,
  applyJobReset,
  applyJobLoading,
} from '../actions';
import {
  APPLY_JOB,
  APPLY_JOB_SUCCESS,
  APPLY_JOB_ERROR,
  APPLY_JOB_RESET,
  APPLY_JOB_LOADING,
} from '../constants';

describe('ApplyPage actions', () => {
  describe('applyJob', () => {
    it('has a type of APPLY_JOB', () => {
      const formValues = {};
      const expected = {
        type: APPLY_JOB,
        formValues,
      };
      expect(applyJob(formValues)).toEqual(expected);
    });
  });

  describe('applyJobSuccess', () => {
    it('has a type of APPLY_JOB_SUCCESS', () => {
      const success = true;
      const expected = {
        type: APPLY_JOB_SUCCESS,
        success,
      };
      expect(applyJobSuccess(success)).toEqual(expected);
    });
  });

  describe('applyJobError', () => {
    it('has a type of APPLY_JOB_ERROR', () => {
      const error = true;
      const expected = {
        type: APPLY_JOB_ERROR,
        error,
      };
      expect(applyJobError(error)).toEqual(expected);
    });
  });

  describe('applyJobReset', () => {
    it('has a type of APPLY_JOB_RESET', () => {
      const expected = {
        type: APPLY_JOB_RESET,
      };
      expect(applyJobReset()).toEqual(expected);
    });
  });

  describe('applyJobLoading', () => {
    it('has a type of APPLY_JOB_LOADING', () => {
      const expected = {
        type: APPLY_JOB_LOADING,
      };
      expect(applyJobLoading()).toEqual(expected);
    });
  });
});
