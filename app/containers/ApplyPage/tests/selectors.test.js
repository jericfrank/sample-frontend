import { fromJS } from 'immutable';

import makeSelectApplyPage, { selectApplyPageDomain } from '../selectors';

describe('selectApplyPageDomain', () => {
  it('should select `applyPage`', () => {
    const state = fromJS({
      applyPage: {
        test: 'test',
      },
    });
    expect(selectApplyPageDomain(state))
      .toEqual(state.get('applyPage'));
  });
});

describe('makeSelectApplyPage', () => {
  it('should select `applyPage`', () => {
    const applyPage = fromJS({
      test: 'test',
    });
    const mockedState = fromJS({
      applyPage,
    });
    expect(makeSelectApplyPage()(mockedState))
      .toEqual(applyPage.toJS());
  });
});
