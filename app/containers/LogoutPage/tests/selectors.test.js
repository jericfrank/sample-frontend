import { fromJS } from 'immutable';

import makeSelectLogoutPage, { selectLogoutPageDomain } from '../selectors';

describe('selectLogoutPageDomain', () => {
  it('should select `logoutPage`', () => {
    const state = fromJS({
      logoutPage: {
        test: 'test',
      },
    });
    expect(selectLogoutPageDomain(state))
      .toEqual(state.get('logoutPage'));
  });
});

describe('makeSelectLogoutPage', () => {
  it('should select `logoutPage`', () => {
    const logoutPage = fromJS({
      test: 'test',
    });
    const mockedState = fromJS({
      logoutPage,
    });
    expect(makeSelectLogoutPage()(mockedState))
      .toEqual(logoutPage.toJS());
  });
});
