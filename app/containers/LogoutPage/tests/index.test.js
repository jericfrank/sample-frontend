import React from 'react';
import { shallow } from 'enzyme';
import { Redirect } from 'react-router-dom';

import FullPageLoader from 'components/FullPageLoader';

import { logout, reset } from '../actions';
import { LogoutPage, mapDispatchToProps } from '../index';

describe('<LogoutPage />', () => {
  let props;

  beforeEach(() => {
    props = {
      logoutpage: {
        loggedOut: false,
        logOutErr: false,
      },
      logOutUser: jest.fn(),
      resetLogout: jest.fn(),
    };
  });

  it('should display `FullPageLoader` loader', () => {
    const wrapper = shallow(<LogoutPage {...props} />);
    expect(wrapper.find(FullPageLoader)).toHaveLength(1);
  });

  it('should call `logOutUser` on mount', () => {
    const wrapper = shallow(<LogoutPage {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.logOutUser).toHaveBeenCalledTimes(1);
  });

  it('should call `resetLogout` on mount', () => {
    const wrapper = shallow(<LogoutPage {...props} />);
    wrapper.instance().componentWillUnmount();
    expect(props.resetLogout).toHaveBeenCalledTimes(1);
  });

  it('should display `Redirect`', () => {
    const newProps = {
      ...props,
      logoutpage: {
        ...props.logoutpage,
        logOutErr: true,
      },
    };
    const wrapper = shallow(<LogoutPage {...newProps} />);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

  describe('mapDispatchToProps', () => {
    describe('logOutUser', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.logOutUser).toBeDefined();
      });

      it('should dispatch logout when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.logOutUser();
        expect(dispatch).toHaveBeenCalledWith(logout());
      });
    });

    describe('resetLogout', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.resetLogout).toBeDefined();
      });

      it('should dispatch reset when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.resetLogout();
        expect(dispatch).toHaveBeenCalledWith(reset());
      });
    });
  });
});
