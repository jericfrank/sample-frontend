
import {
  logout,
  loggedOut,
  logoutErr,
  reset,
} from '../actions';

import {
 LOGOUT,
 LOGGED_OUT,
 LOGOUT_ERR,
 LOGOUT_RESET,
} from '../constants';

describe('LogoutPage actions', () => {
  describe('logout', () => {
    it('has a type of LOGOUT', () => {
      const expected = {
        type: LOGOUT,
      };
      expect(logout()).toEqual(expected);
    });
  });

  describe('loggedOut', () => {
    it('has a type of LOGGED_OUT', () => {
      const expected = {
        type: LOGGED_OUT,
      };
      expect(loggedOut()).toEqual(expected);
    });
  });

  describe('logoutErr', () => {
    it('has a type of LOGOUT_ERR', () => {
      const expected = {
        type: LOGOUT_ERR,
      };
      expect(logoutErr()).toEqual(expected);
    });
  });

  describe('reset', () => {
    it('has a type of LOGOUT_RESET', () => {
      const expected = {
        type: LOGOUT_RESET,
      };
      expect(reset()).toEqual(expected);
    });
  });
});
