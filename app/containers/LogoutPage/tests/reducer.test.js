
import { fromJS } from 'immutable';
import logoutPageReducer from '../reducer';
import {
  logout,
  loggedOut,
  logoutErr,
  reset,
} from '../actions';

describe('logoutPageReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({
      loggingOut: false,
      loggedOut: false,
      logOutErr: false,
    });
  });

  it('returns the initial state', () => {
    expect(logoutPageReducer(undefined, {})).toEqual(state);
  });

  it('should handle the logout action correctly', () => {
    const expectedResult = state
      .set('loggingOut', true)
      .set('loggedOut', false)
      .set('logOutErr', false);
    expect(logoutPageReducer(state, logout()))
      .toEqual(expectedResult);
  });

  it('should handle the loggedOut action correctly', () => {
    const expectedResult = state
      .set('loggingOut', false)
      .set('loggedOut', true)
      .set('logOutErr', false);
    expect(logoutPageReducer(state, loggedOut()))
      .toEqual(expectedResult);
  });

  it('should handle the logoutErr action correctly', () => {
    const expectedResult = state
      .set('loggingOut', false)
      .set('loggedOut', false)
      .set('logOutErr', true);
    expect(logoutPageReducer(state, logoutErr()))
      .toEqual(expectedResult);
  });

  it('should handle the reset action correctly', () => {
    const expectedResult = state
      .set('loggingOut', false)
      .set('loggedOut', false)
      .set('logOutErr', false);
    expect(logoutPageReducer(state, reset()))
      .toEqual(expectedResult);
  });
});
