/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { take, put, takeLatest, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';

import { unsetUserAuth } from 'containers/App/actions';

import { loggedOut, logoutErr } from '../actions';
import watchLogoutPage, { logOutUser } from '../saga';
import { LOGOUT } from '../constants';

/* eslint-disable redux-saga/yield-effects */
describe('logOutUser', () => {
  let logOutUserGenerator;

  beforeEach(() => {
    class LocalStorageMock {
      constructor() {
        this.store = {};
      }

      clear() {
        this.store = {};
      }

      getItem(key) {
        return this.store[key] || null;
      }

      setItem(key, value) {
        this.store[key] = value.toString();
      }

      removeItem(key) {
        delete this.store[key];
      }
    }
    global.localStorage = new (LocalStorageMock)();

    logOutUserGenerator = logOutUser();

    const callDescriptor = logOutUserGenerator.next().value;
    expect(callDescriptor).toMatchSnapshot();

    const jwtTokenCallDescriptor = logOutUserGenerator.next().value;
    expect(jwtTokenCallDescriptor).toMatchSnapshot();
  });

  it('should call the unsetUserAuth action', () => {
    const putDescriptor = logOutUserGenerator.next().value;
    expect(putDescriptor).toEqual(put(unsetUserAuth()));
  });

  it('should call the loggedOut action', () => {
    logOutUserGenerator.next();
    const putDescriptor = logOutUserGenerator.next().value;
    expect(putDescriptor).toEqual(put(loggedOut()));
  });

  describe('catch', () => {
    beforeEach(() => {
      const callDescriptor = logOutUserGenerator.throw().value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the unsetUserAuth action', () => {
      const putDescriptor = logOutUserGenerator.next().value;
      expect(putDescriptor).toEqual(put(unsetUserAuth()));
    });

    it('should call the logoutErr action', () => {
      logOutUserGenerator.next();
      const putDescriptor = logOutUserGenerator.next().value;
      expect(putDescriptor).toEqual(put(logoutErr()));
    });
  });
});

describe('watchLogoutPage Saga', () => {
  const generator = watchLogoutPage();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const expectedYield = takeLatest(LOGOUT, logOutUser);
    expect(generator.next().value).toEqual(expectedYield);
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
