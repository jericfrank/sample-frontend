/**
 *
 * LogoutPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Redirect } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import FullPageLoader from 'components/FullPageLoader';

import { logout, reset } from './actions';
import makeSelectLogoutPage from './selectors';
import reducer from './reducer';
import saga from './saga';

export class LogoutPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.logOutUser();
  }

  componentWillUnmount() {
    this.props.resetLogout();
  }

  render() {
    const { logoutpage } = this.props;
    const { loggedOut, logOutErr } = logoutpage;

    if (loggedOut || logOutErr) {
      return <Redirect to="/" />;
    }

    return (
      <div>
        <Helmet>
          <title>Logging out...</title>
          <meta name="description" content="Description of Logging out..." />
        </Helmet>

        <FullPageLoader />
      </div>
    );
  }
}

LogoutPage.propTypes = {
  logoutpage: PropTypes.object.isRequired,
  logOutUser: PropTypes.func.isRequired,
  resetLogout: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  logoutpage: makeSelectLogoutPage(),
});

export function mapDispatchToProps(dispatch) {
  return {
    logOutUser: () => dispatch(logout()),
    resetLogout: () => dispatch(reset()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'logoutPage', reducer });
const withSaga = injectSaga({ key: 'logoutPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(LogoutPage);
