import { take, takeLatest, call, put, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { getRequest } from 'utils/request';
import { expireJwtToken } from 'utils/jwtToken';

import { unsetUserAuth } from 'containers/App/actions';
import { loggedOut, logoutErr } from './actions';
import { LOGOUT } from './constants';

export function* logOutUser() {
  try {
    yield call(getRequest, `/hr/applicants/logout?token=${localStorage.getItem('jobportalToken')}`);
    yield call(expireJwtToken);
    yield put(unsetUserAuth());
    yield put(loggedOut());
  } catch (err) {
    yield call(expireJwtToken);
    yield put(unsetUserAuth());
    yield put(logoutErr());
  }
}

// Individual exports for testing
export default function* watchLogoutPage() {
  const watcher = yield takeLatest(LOGOUT, logOutUser);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
