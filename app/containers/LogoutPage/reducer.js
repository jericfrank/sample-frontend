/*
 *
 * LogoutPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOGOUT,
  LOGGED_OUT,
  LOGOUT_ERR,
  LOGOUT_RESET,
} from './constants';

const initialState = fromJS({
  loggingOut: false,
  loggedOut: false,
  logOutErr: false,
});

function logoutPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return state
        .set('loggingOut', true)
        .set('loggedOut', false)
        .set('logOutErr', false);

    case LOGGED_OUT:
      return state
        .set('loggingOut', false)
        .set('loggedOut', true)
        .set('logOutErr', false);

    case LOGOUT_ERR:
      return state
        .set('loggingOut', false)
        .set('loggedOut', false)
        .set('logOutErr', true);

    case LOGOUT_RESET:
      return state
        .set('loggingOut', false)
        .set('loggedOut', false)
        .set('logOutErr', false);

    default:
      return state;
  }
}

export default logoutPageReducer;
