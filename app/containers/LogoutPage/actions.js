/*
 *
 * LogoutPage actions
 *
 */

import {
 LOGOUT,
 LOGGED_OUT,
 LOGOUT_ERR,
 LOGOUT_RESET,
} from './constants';

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function loggedOut() {
  return {
    type: LOGGED_OUT,
  };
}

export function logoutErr() {
  return {
    type: LOGOUT_ERR,
  };
}

export function reset() {
  return {
    type: LOGOUT_RESET,
  };
}
