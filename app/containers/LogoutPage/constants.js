/*
 *
 * LogoutPage constants
 *
 */

export const LOGOUT = 'app/LogoutPage/LOGOUT';
export const LOGGED_OUT = 'app/LogoutPage/LOGGED_OUT';
export const LOGOUT_ERR = 'app/LogoutPage/LOGOUT_ERR';
export const LOGOUT_RESET = 'app/LogoutPage/LOGOUT_RESET';
