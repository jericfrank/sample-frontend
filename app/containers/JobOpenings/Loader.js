import React from 'react';
import { Row, Col } from 'antd';
import SpinLoader from 'components/SpinLoader';

import Wrapper from './Wrapper';

function Loader() {
  return (
    <Wrapper>
      <Row>
        <Col xs={1} sm={1} md={1} lg={3} xl={5}></Col>
        <Col xs={22} sm={22} md={22} lg={18} xl={14}>
          <div>
            <div className="job-openings-wrapper text-center">
              <SpinLoader />
            </div>
          </div>
        </Col>
        <Col xs={1} sm={1} md={1} lg={3} xl={5}></Col>
      </Row>
    </Wrapper>
  );
}

export default Loader;
