/**
 *
 * JobOpenings
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Row, Col, Card } from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import FilterTags from 'components/FilterTags';
import JobsTable from 'components/JobsTable';

import { makeSelectUser } from 'containers/App/selectors';

import Wrapper from './Wrapper';
import {
  fetchJobOpenings,
  filterJobOpenings,
  removeJobFilters,
} from './actions';
import makeSelectJobOpenings from './selectors';
import reducer from './reducer';
import saga from './saga';

export class JobOpenings extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.getJobOpenings();
  }

  render() {
    const { onJobFilter, onJobRemoveFilters, jobopenings } = this.props;
    const { jobOpenings, filters, fetching } = jobopenings;
    return (
      <Wrapper>
        <Row>
          <Col xs={2} sm={2} md={1} lg={1} xl={5}></Col>
          <Col xs={20} sm={20} md={22} lg={22} xl={14}>
            <div>
              <div className="job-openings-wrapper">
                <div>
                  <h1 className="text-center font-champ-bold dark-navy-blue">
                    Latest Job Openings
                  </h1>

                  {fetching ? (
                    <Card loading></Card>
                  ) : (
                    <div>
                      <div className="filter-wrapper text-center">
                        <span className="filter-label">Filters:</span>
                        <FilterTags
                          loading={fetching}
                          tags={filters}
                          onFilter={onJobFilter}
                          onRemoveFilters={onJobRemoveFilters}
                        />
                      </div>

                      <JobsTable loading={fetching} jobs={jobOpenings.dirty} applied={this.props.currentUser ? this.props.currentUser.applications : null} />
                    </div>
                  )}
                </div>
              </div>
            </div>
          </Col>
          <Col xs={2} sm={2} md={1} lg={1} xl={5}></Col>
        </Row>
      </Wrapper>
    );
  }
}

JobOpenings.propTypes = {
  getJobOpenings: PropTypes.func.isRequired,
  onJobFilter: PropTypes.func.isRequired,
  onJobRemoveFilters: PropTypes.func.isRequired,
  jobopenings: PropTypes.object.isRequired,
  currentUser: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  jobopenings: makeSelectJobOpenings(),
  currentUser: makeSelectUser(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getJobOpenings: () => dispatch(fetchJobOpenings()),
    onJobFilter: (filters) => dispatch(filterJobOpenings(filters)),
    onJobRemoveFilters: () => dispatch(removeJobFilters()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'jobOpenings', reducer });
const withSaga = injectSaga({ key: 'jobOpenings', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(JobOpenings);
