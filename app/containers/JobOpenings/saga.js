import _ from 'lodash';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { getRequest } from 'utils/request';
import { message } from 'antd';

import { FETCH_JOB_OPENINGS, FETCH_JOB_ERR_MSG } from './constants';
import { jobOpeningsFetched, jobsLoadingErr } from './actions';

export function* fetchJobOpenings() {
  try {
    const response = yield call(getRequest, '/hr/applicants/job_openings');
    const divisions = yield call(getRequest, '/hr/applicants/divisions');
    const jobOpenings = _.map(response, (jobOpening) => ({
      key: jobOpening.id,
      ...jobOpening,
      department: _.result(
        _.find(divisions, { id: parseInt(jobOpening.department_id, 10) }),
        'description',
        '------',
      ),
    }));

    const filters = _.uniq(_.flatten([
      _.map(jobOpenings, 'employment_type'),
      _.map(jobOpenings, 'location'),
      _.map(jobOpenings, 'title'),
      _.map(jobOpenings, 'department'),
    ]));

    yield put(jobOpeningsFetched(_.orderBy(jobOpenings, ['title']), filters));
  } catch (err) {
    yield call(message.error, FETCH_JOB_ERR_MSG);
    yield put(jobsLoadingErr());
  }
}

export default function* watchFetchJobOpenings() {
  const watcher = yield takeLatest(FETCH_JOB_OPENINGS, fetchJobOpenings);
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
