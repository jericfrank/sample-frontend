/*
 *
 * JobPage actions
 *
 */

import {
  FETCH_JOB_OPENINGS,
  JOB_OPENINGS_FETCHED,
  JOB_OPENINGS_FETCH_ERR,
  FILTER_JOB_OPENINGS,
  REMOVE_JOB_FILTERS,
} from './constants';

export function fetchJobOpenings() {
  return {
    type: FETCH_JOB_OPENINGS,
  };
}

export function jobOpeningsFetched(jobOpenings, filters) {
  return {
    type: JOB_OPENINGS_FETCHED,
    jobOpenings,
    filters,
  };
}

export function jobsLoadingErr() {
  return {
    type: JOB_OPENINGS_FETCH_ERR,
  };
}

export function filterJobOpenings(filters) {
  return {
    type: FILTER_JOB_OPENINGS,
    filters,
  };
}

export function removeJobFilters() {
  return {
    type: REMOVE_JOB_FILTERS,
  };
}
