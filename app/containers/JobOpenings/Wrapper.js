import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 100px 0px;
  background-color: #FFF;

  .dark-navy-blue {
    color: #001529;
  }

  .job-openings-wrapper {
    width: 100%;
    border-radius: 5px;

    .ant-table-row {
      background-color: white;
    }

    .filter-wrapper {
      margin-bottom: 10px;

      .filter-label {
        padding: 0px 10px;
      }
    }
  }
`;

export default Wrapper;
