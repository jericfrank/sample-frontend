/*
 *
 * JobOpenings constants
 *
 */

 export const FETCH_JOB_OPENINGS = 'app/HomePage/FETCH_JOB_OPENINGS';
 export const JOB_OPENINGS_FETCHED = 'app/HomePage/JOB_OPENINGS_FETCHED';
 export const JOB_OPENINGS_FETCH_ERR = 'app/HomePage/JOB_OPENINGS_FETCH_ERR';
 export const FILTER_JOB_OPENINGS = 'app/HomePage/FILTER_JOB_OPENINGS';
 export const REMOVE_JOB_FILTERS = 'app/HomePage/REMOVE_JOB_FILTERS';

 export const FETCH_JOB_ERR_MSG = 'Something went wrong. Please try again later.';
