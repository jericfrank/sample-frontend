/*
 *
 * JobOpenings reducer
 *
 */

import { fromJS } from 'immutable';
import _ from 'lodash';
import {
  FETCH_JOB_OPENINGS,
  JOB_OPENINGS_FETCHED,
  JOB_OPENINGS_FETCH_ERR,
  FILTER_JOB_OPENINGS,
  REMOVE_JOB_FILTERS,
} from './constants';

const initialState = fromJS({
  jobOpenings: {
    raw: [],
    dirty: [],
  },
  filters: [],
  fetching: false,
  fetched: false,
  fetchErr: false,
});

function jobOpeningsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_JOB_OPENINGS:
      return state
        .setIn(['jobOpenings', 'raw'], [])
        .setIn(['jobOpenings', 'dirty'], [])
        .set('filters', [])
        .set('fetching', true)
        .set('fetched', false)
        .set('fetchErr', false);

    case JOB_OPENINGS_FETCHED:
      return state
        .setIn(['jobOpenings', 'raw'], action.jobOpenings)
        .setIn(['jobOpenings', 'dirty'], action.jobOpenings)
        .set('filters', action.filters)
        .set('fetching', false)
        .set('fetched', true)
        .set('fetchErr', false);

    case JOB_OPENINGS_FETCH_ERR:
      return state
        .setIn(['jobOpenings', 'raw'], [])
        .setIn(['jobOpenings', 'dirty'], [])
        .set('filters', [])
        .set('fetching', false)
        .set('fetched', false)
        .set('fetchErr', true);

    case FILTER_JOB_OPENINGS:
      return state
        .setIn(
          ['jobOpenings', 'dirty'],
          state.get('jobOpenings').get('raw').filter((opening) => {
            const filterData = _.pick(opening, [
              'title',
              'location',
              'department',
              'employment_type',
            ]);

            return _.some(Object.values(filterData), (data) => (
              _.includes(action.filters, data)
            )) || !action.filters.length;
          })
        );

    case REMOVE_JOB_FILTERS:
      return state
        .setIn(['jobOpenings', 'dirty'], state.get('jobOpenings').get('raw'));

    default:
      return state;
  }
}

export default jobOpeningsReducer;
