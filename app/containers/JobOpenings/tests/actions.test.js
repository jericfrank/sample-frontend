
import {
  fetchJobOpenings,
  jobOpeningsFetched,
  jobsLoadingErr,
  filterJobOpenings,
  removeJobFilters,
} from '../actions';
import {
  FETCH_JOB_OPENINGS,
  JOB_OPENINGS_FETCHED,
  JOB_OPENINGS_FETCH_ERR,
  FILTER_JOB_OPENINGS,
  REMOVE_JOB_FILTERS,
} from '../constants';

describe('JobOpenings actions', () => {
  describe('fetchJobOpenings', () => {
    it('has a type of FETCH_JOB_OPENINGS', () => {
      const expected = {
        type: FETCH_JOB_OPENINGS,
      };
      expect(fetchJobOpenings()).toEqual(expected);
    });
  });

  describe('jobOpeningsFetched', () => {
    it('has a type of JOB_OPENINGS_FETCHED', () => {
      const jobOpenings = [];
      const filters = [];
      const expected = {
        type: JOB_OPENINGS_FETCHED,
        jobOpenings,
        filters,
      };
      expect(jobOpeningsFetched(jobOpenings, filters)).toEqual(expected);
    });
  });

  describe('jobsLoadingErr', () => {
    it('has a type of JOB_OPENINGS_FETCH_ERR', () => {
      const expected = {
        type: JOB_OPENINGS_FETCH_ERR,
      };
      expect(jobsLoadingErr()).toEqual(expected);
    });
  });

  describe('filterJobOpenings', () => {
    it('has a type of FILTER_JOB_OPENINGS', () => {
      const filters = [];
      const expected = {
        type: FILTER_JOB_OPENINGS,
        filters,
      };
      expect(filterJobOpenings(filters)).toEqual(expected);
    });
  });

  describe('removeJobFilters', () => {
    it('has a type of REMOVE_JOB_FILTERS', () => {
      const expected = {
        type: REMOVE_JOB_FILTERS,
      };
      expect(removeJobFilters()).toEqual(expected);
    });
  });
});
