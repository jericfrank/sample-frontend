
import { fromJS } from 'immutable';
import jobOpeningsReducer from '../reducer';
import {
  fetchJobOpenings,
  jobOpeningsFetched,
  jobsLoadingErr,
  filterJobOpenings,
  removeJobFilters,
} from '../actions';

describe('jobOpeningsReducer', () => {
  let state;
  beforeEach(() => {
    state = fromJS({
      jobOpenings: {
        raw: [],
        dirty: [],
      },
      filters: [],
      fetching: false,
      fetched: false,
      fetchErr: false,
    });
  });

  it('returns the initial state', () => {
    const expectedResult = state;
    expect(jobOpeningsReducer(undefined, {})).toEqual(expectedResult);
  });

  it('should handle the fetchJobOpenings action correctly', () => {
    const expectedResult = state
      .setIn(['jobOpenings', 'raw'], [])
      .setIn(['jobOpenings', 'dirty'], [])
      .set('filters', [])
      .set('fetching', true)
      .set('fetched', false)
      .set('fetchErr', false);
    expect(jobOpeningsReducer(state, fetchJobOpenings()))
      .toEqual(expectedResult);
  });

  it('should handle the jobOpeningsFetched action correctly', () => {
    const jobOpenings = ['jobs', 'goes', 'here'];
    const filters = ['filters', 'goes', 'here'];
    const expectedResult = state
      .setIn(['jobOpenings', 'raw'], jobOpenings)
      .setIn(['jobOpenings', 'dirty'], jobOpenings)
      .set('filters', filters)
      .set('fetching', false)
      .set('fetched', true)
      .set('fetchErr', false);
    expect(jobOpeningsReducer(state, jobOpeningsFetched(jobOpenings, filters)))
      .toEqual(expectedResult);
  });

  it('should handle the jobsLoadingErr action correctly', () => {
    const expectedResult = state
      .setIn(['jobOpenings', 'raw'], [])
      .setIn(['jobOpenings', 'dirty'], [])
      .set('filters', [])
      .set('fetching', false)
      .set('fetched', false)
      .set('fetchErr', true);
    expect(jobOpeningsReducer(state, jobsLoadingErr()))
      .toEqual(expectedResult);
  });

  it('should handle the filterJobOpenings action correctly', () => {
    const filters = ['Job 1'];
    const dirty = [{ title: 'Job 1' }];
    const raw = [{ title: 'Job' }, ...dirty];

    const mockedState = state
      .setIn(['jobOpenings', 'raw'], raw)
      .setIn(['jobOpenings', 'dirty'], dirty)
      .set('filters', filters);

    const expectedResult = state
      .setIn(['jobOpenings', 'raw'], raw)
      .setIn(['jobOpenings', 'dirty'], dirty)
      .set('filters', filters);

    expect(jobOpeningsReducer(mockedState, filterJobOpenings(filters)))
      .toEqual(expectedResult);
  });

  it('should handle the removeJobFilters action correctly', () => {
    const expectedResult = state
      .setIn(['jobOpenings', 'raw'], fromJS([]))
      .setIn(['jobOpenings', 'dirty'], fromJS([]))
      .set('filters', fromJS([]))
      .set('fetching', false)
      .set('fetched', false)
      .set('fetchErr', false);
    expect(jobOpeningsReducer(state, removeJobFilters()))
      .toEqual(expectedResult);
  });
});
