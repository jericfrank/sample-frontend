/**
 * Tests for JobOpenings sagas
 */

import { put, takeLatest, take, cancel } from 'redux-saga/effects';
import { createMockTask } from 'redux-saga/utils';
import { LOCATION_CHANGE } from 'react-router-redux';

import { FETCH_JOB_OPENINGS } from '../constants';
import { jobOpeningsFetched, jobsLoadingErr } from '../actions';
import watchFetchJobOpenings, { fetchJobOpenings } from '../saga';

const divisions = [{ id: 1, description: 'Department' }];
const job = {
  id: 1,
  title: 'Job Title',
  department_id: 1,
  location: 'Cebu City',
  employment_type: 'Full-time',
};
const jobOpenings = [job];

/* eslint-disable redux-saga/yield-effects */
describe('fetchJobOpenings Saga', () => {
  let fetchJobOpeningsGenerator;

  // We have to test twice, once for a successful load and once for an unsuccessful one
  // so we do all the stuff that happens beforehand automatically in the beforeEach
  beforeEach(() => {
    fetchJobOpeningsGenerator = fetchJobOpenings();

    const jobOpeningsDescriptor = fetchJobOpeningsGenerator.next(divisions).value;
    expect(jobOpeningsDescriptor).toMatchSnapshot();

    const divisionsDescriptor = fetchJobOpeningsGenerator.next(jobOpenings).value;
    expect(divisionsDescriptor).toMatchSnapshot();
  });

  it('should dispatch the jobOpeningsFetched action if it requests the data successfully', () => {
    const putDescriptor = fetchJobOpeningsGenerator.next(divisions).value;
    const filters = ['Full-time', 'Cebu City', 'Job Title', 'Department'];
    expect(putDescriptor).toEqual(put(jobOpeningsFetched([{
      ...job,
      key: 1,
      department: 'Department',
    }], filters)));
  });

  describe('catch', () => {
    beforeEach(() => {
      const callDescriptor = fetchJobOpeningsGenerator.throw().value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the jobsLoadingErr action if the response errors', () => {
      const putDescriptor = fetchJobOpeningsGenerator.next().value;
      expect(putDescriptor).toEqual(put(jobsLoadingErr()));
    });
  });
});

describe('watchFetchJobOpenings Saga', () => {
  const generator = watchFetchJobOpenings();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const expectedYield = takeLatest(FETCH_JOB_OPENINGS, fetchJobOpenings);
    expect(generator.next().value).toEqual(expectedYield);
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
