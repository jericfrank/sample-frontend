import React from 'react';
import { shallow } from 'enzyme';
import { Card } from 'antd';

import JobsTable from 'components/JobsTable';

import {
  fetchJobOpenings,
  filterJobOpenings,
  removeJobFilters,
} from '../actions';
import { JobOpenings, mapDispatchToProps } from '../index';

describe('<JobOpenings />', () => {
  let props;
  beforeEach(() => {
    props = {
      getJobOpenings: jest.fn(),
      onJobFilter: jest.fn(),
      onJobRemoveFilters: jest.fn(),
      jobopenings: {
        jobOpenings: {
          raw: [],
          dirty: [],
        },
        filters: [],
        fetching: false,
        fetchErr: false,
        errMsg: '',
      },
    };
  });

  it('should invoke `getJobOpenings` when `componentDidMount`', () => {
    const wrapper = shallow(<JobOpenings {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.getJobOpenings).toHaveBeenCalledTimes(1);
  });

  it('should display loading `Card`', () => {
    props = {
      ...props,
      jobopenings: {
        ...props.jobopenings,
        fetching: true,
      },
    };

    const wrapper = shallow(<JobOpenings {...props} />);
    expect(wrapper.contains(<Card loading></Card>)).toEqual(true);
  });

  it('should display `JobsTable` without `applications`', () => {
    props = {
      ...props,
      currentUser: {
        applications: [],
      },
    };
    const wrapper = shallow(<JobOpenings {...props} />);
    expect(wrapper.contains(
      <JobsTable
        loading={false}
        jobs={[]}
        applied={props.currentUser.applications}
      />
    )).toBeTruthy();
  });

  it('should display `JobsTable` with `applications`', () => {
    const wrapper = shallow(<JobOpenings {...props} />);
    expect(wrapper.contains(
      <JobsTable
        loading={false}
        jobs={[]}
        applied={null}
      />
    )).toBeTruthy();
  });

  describe('mapDispatchToProps', () => {
    describe('getJobOpenings', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.getJobOpenings).toBeDefined();
      });

      it('should dispatch fetchJobOpenings when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.getJobOpenings();
        expect(dispatch).toHaveBeenCalledWith(fetchJobOpenings());
      });
    });

    describe('onJobFilter', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onJobFilter).toBeDefined();
      });

      it('should dispatch filterJobOpenings when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const filters = ['Detailer'];
        result.onJobFilter(filters);
        expect(dispatch).toHaveBeenCalledWith(filterJobOpenings(filters));
      });
    });

    describe('onJobRemoveFilters', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onJobRemoveFilters).toBeDefined();
      });

      it('should dispatch removeJobFilters when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.onJobRemoveFilters();
        expect(dispatch).toHaveBeenCalledWith(removeJobFilters());
      });
    });
  });
});
