import { fromJS } from 'immutable';

import makeSelectJobOpenings, { selectJobOpeningsDomain } from '../selectors';

describe('selectJobOpeningsDomain', () => {
  it('should select `jobOpenings`', () => {
    const state = fromJS({
      jobOpenings: {
        test: 'test',
      },
    });
    expect(selectJobOpeningsDomain(state))
      .toEqual(state.get('jobOpenings'));
  });
});

describe('makeSelectJobOpenings', () => {
  it('should select `jobOpenings`', () => {
    const jobOpenings = fromJS({
      test: 'test',
    });
    const mockedState = fromJS({
      jobOpenings,
    });
    expect(makeSelectJobOpenings()(mockedState))
      .toEqual(jobOpenings.toJS());
  });
});
