import { createSelector } from 'reselect';

/**
 * Direct selector to the jobOpenings state domain
 */
const selectJobOpeningsDomain = (state) => state.get('jobOpenings');

/**
 * Other specific selectors
 */


/**
 * Default selector used by JobOpenings
 */

const makeSelectJobOpenings = () => createSelector(
  selectJobOpeningsDomain,
  (substate) => substate.toJS()
);

export default makeSelectJobOpenings;
export {
  selectJobOpeningsDomain,
};
