/*
 *
 * VerifyPage actions
 *
 */

import {
  VALIDATE_CODE,
  CODE_VALIDATED,
  VALIDATE_ERR,
} from './constants';

export function validateCode(code) {
  return {
    type: VALIDATE_CODE,
    code,
  };
}

export function codeValidated() {
  return {
    type: CODE_VALIDATED,
  };
}

export function validateCodeErr() {
  return {
    type: VALIDATE_ERR,
  };
}
