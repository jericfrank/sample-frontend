/**
 *
 * VerifyPage
 *
 */

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import urlParams from 'utils/urlParams';

import SpinPageLoader from 'components/SpinPageLoader';
import { validateCode } from './actions';
import makeSelectVerifyPage from './selectors';
import reducer from './reducer';
import saga from './saga';

export class VerifyPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const urlParamsObj = urlParams(this.props.location.search);
    const code = _.result(urlParamsObj, 'code', null);
    this.props.validateCode(code);
  }

  render() {
    const { validated, validateErr } = this.props.verifypage;

    if (validated || validateErr) {
      return <Redirect to="/" />;
    }

    return <SpinPageLoader label="Validating..." />;
  }
}

VerifyPage.propTypes = {
  location: PropTypes.object.isRequired,
  validateCode: PropTypes.func.isRequired,
  verifypage: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  verifypage: makeSelectVerifyPage(),
});

export function mapDispatchToProps(dispatch) {
  return {
    validateCode: (code) => dispatch(validateCode(code)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'verifyPage', reducer });
const withSaga = injectSaga({ key: 'verifyPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(VerifyPage);
