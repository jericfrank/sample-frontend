import CryptoJS from 'crypto-js';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { postRequest, getRequest } from 'utils/request';
import { handleJwtToken } from 'utils/jwtToken';
import { message } from 'antd';

import { setUserAuth } from 'containers/App/actions';
import { codeValidated, validateCodeErr } from './actions';
import { VALIDATE_CODE } from './constants';

export function* validateCode({ code }) {
  try {
    const { applicant } = yield call(postRequest, '/hr/applicants/verifyToken', { token: code });
    const { details, token } = applicant;

    yield call(handleJwtToken, token);
    const applications = yield call(getRequest, '/hr/applicants/job_applications');

    const user = { ...details, applications };

    const encrypt = yield call(CryptoJS.AES.encrypt, JSON.stringify(user), token);
    localStorage.setItem('jobportalUser', encrypt);

    yield call(message.success, 'Email verified! Welcome to Job Board!', 5);
    yield put(setUserAuth(user));
    yield put(codeValidated());
  } catch (err) {
    yield call(message.error, 'Email verification expired or invalid.', 5);
    yield put(validateCodeErr());
  }
}

export default function* watchVerifyPage() {
  const watcher = yield takeLatest(VALIDATE_CODE, validateCode);

  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}
