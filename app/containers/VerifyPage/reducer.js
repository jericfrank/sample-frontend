/*
 *
 * VerifyPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  VALIDATE_CODE,
  CODE_VALIDATED,
  VALIDATE_ERR,
} from './constants';

const initialState = fromJS({
  validating: false,
  validated: false,
  validateErr: false,
});

function verifyPageReducer(state = initialState, action) {
  switch (action.type) {
    case VALIDATE_CODE:
      return state
        .set('validating', true)
        .set('validated', false)
        .set('validateErr', false);

    case CODE_VALIDATED:
      return state
        .set('validating', false)
        .set('validated', true)
        .set('validateErr', false);

    case VALIDATE_ERR:
      return state
        .set('validating', false)
        .set('validated', false)
        .set('validateErr', true);

    default:
      return state;
  }
}

export default verifyPageReducer;
