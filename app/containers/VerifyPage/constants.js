/*
 *
 * VerifyPage constants
 *
 */

export const VALIDATE_CODE = 'app/VerifyPage/VALIDATE_CODE';
export const CODE_VALIDATED = 'app/VerifyPage/CODE_VALIDATED';
export const VALIDATE_ERR = 'app/VerifyPage/VALIDATE_ERR';
