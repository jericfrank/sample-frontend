/**
 * Tests for SignUpPage sagas
 */

import { put, takeLatest, take, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';

import { setUserAuth } from 'containers/App/actions';
import { VALIDATE_CODE } from '../constants';
import watchVerifyPage, { validateCode } from '../saga';
import { codeValidated, validateCodeErr } from '../actions';

/* eslint-disable redux-saga/yield-effects */
describe('validateCode Saga', () => {
  let validateCodeGenerator;
  let payload;
  let response;
  let encrypt;

  beforeEach(() => {
    class LocalStorageMock {
      constructor() {
        this.store = {};
      }

      clear() {
        this.store = {};
      }

      getItem(key) {
        return this.store[key] || null;
      }

      setItem(key, value) {
        this.store[key] = value.toString();
      }

      removeItem(key) {
        delete this.store[key];
      }
    }
    global.localStorage = new (LocalStorageMock)();

    payload = {
      code: 'CODE',
    };
    response = {
      applicant: {
        details: {},
        token: 'Token goes here',
      },
    };
    encrypt = 'Encrypted the flipping out of the user details';

    validateCodeGenerator = validateCode(payload);

    const registerDescriptor = validateCodeGenerator.next(response).value;
    expect(registerDescriptor).toMatchSnapshot();

    const getReqDescriptor = validateCodeGenerator.next(response).value;
    expect(getReqDescriptor).toMatchSnapshot();

    const jwtCallDescriptor = validateCodeGenerator.next(response).value;
    expect(jwtCallDescriptor).toMatchSnapshot();

    const messageDescriptor = validateCodeGenerator.next().value;
    expect(messageDescriptor).toMatchSnapshot();

    const encryptDescriptor = validateCodeGenerator.next(encrypt).value;
    expect(encryptDescriptor).toMatchSnapshot();
  });

  it('should dispatch the setUserAuth action if it requests the data successfully', () => {
    const putDescriptor = validateCodeGenerator.next(response.applicant.details).value;
    expect(putDescriptor).toEqual(put(setUserAuth(response.applicant.details)));
  });

  it('should dispatch the codeValidated action if it requests the data successfully', () => {
    validateCodeGenerator.next(response.applicant.details);
    const putDescriptor = validateCodeGenerator.next().value;
    expect(putDescriptor).toEqual(put(codeValidated()));
  });

  describe('catch', () => {
    beforeEach(() => {
      const callDescriptor = validateCodeGenerator.throw().value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the validateCodeErr action if the response errors', () => {
      const putDescriptor = validateCodeGenerator.next().value;
      expect(putDescriptor).toEqual(put(validateCodeErr()));
    });
  });
});

describe('watchVerifyPage Saga', () => {
  const generator = watchVerifyPage();
  const mockTask = createMockTask();

  it('waits for start action VALIDATE_CODE', () => {
    const expectedYield = takeLatest(VALIDATE_CODE, validateCode);
    expect(generator.next().value).toEqual(expectedYield);
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
