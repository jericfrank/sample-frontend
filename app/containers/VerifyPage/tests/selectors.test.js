import { fromJS } from 'immutable';
import makeSelectVerifyPage, { selectVerifyPageDomain } from '../selectors';

describe('selectVerifyPageDomain', () => {
  it('should select `verifyPage` state', () => {
    const verifyPage = fromJS({
      validating: false,
      validated: false,
      validateErr: false,
    });
    const mockedState = fromJS({
      verifyPage,
    });
    expect(selectVerifyPageDomain(mockedState))
      .toEqual(verifyPage);
  });
});

describe('makeSelectVerifyPage', () => {
  it('should select `SignUpPage` state', () => {
    const state = {
      validating: false,
      validated: false,
      validateErr: false,
    };
    const verifyPage = fromJS(state);
    const mockedState = fromJS({
      verifyPage,
    });
    expect(makeSelectVerifyPage()(mockedState))
      .toEqual(state);
  });
});
