
import {
  validateCode,
  codeValidated,
  validateCodeErr,
} from '../actions';
import {
  VALIDATE_CODE,
  CODE_VALIDATED,
  VALIDATE_ERR,
} from '../constants';

describe('SignUpPage actions', () => {
  describe('validateCode', () => {
    it('has a type of VALIDATE_CODE', () => {
      const code = 'CODE';
      const expected = {
        type: VALIDATE_CODE,
        code,
      };
      expect(validateCode(code)).toEqual(expected);
    });
  });

  describe('codeValidated', () => {
    it('has a type of CODE_VALIDATED', () => {
      const expected = {
        type: CODE_VALIDATED,
      };
      expect(codeValidated()).toEqual(expected);
    });
  });

  describe('validateCodeErr', () => {
    it('has a type of VALIDATE_ERR', () => {
      const expected = {
        type: VALIDATE_ERR,
      };
      expect(validateCodeErr()).toEqual(expected);
    });
  });
});
