import { fromJS } from 'immutable';
import verifyPageReducer from '../reducer';

import {
  validateCode,
  codeValidated,
  validateCodeErr,
} from '../actions';

describe('verifyPageReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({
      validating: false,
      validated: false,
      validateErr: false,
    });
  });

  it('returns the initial state', () => {
    expect(verifyPageReducer(undefined, {})).toEqual(state);
  });

  it('should handle the validateCode action correctly', () => {
    const code = 'CODE';
    const expectedResult = state
      .set('validating', true)
      .set('validated', false)
      .set('validateErr', false);
    expect(verifyPageReducer(state, validateCode(code)))
      .toEqual(expectedResult);
  });

  it('should handle the codeValidated action correctly', () => {
    const expectedResult = state
      .set('validating', false)
      .set('validated', true)
      .set('validateErr', false);
    expect(verifyPageReducer(state, codeValidated()))
      .toEqual(expectedResult);
  });

  it('should handle the validateCodeErr action correctly', () => {
    const expectedResult = state
      .set('validating', false)
      .set('validated', false)
      .set('validateErr', true);
    expect(verifyPageReducer(state, validateCodeErr()))
      .toEqual(expectedResult);
  });
});
