import React from 'react';
import { shallow } from 'enzyme';
import { Redirect } from 'react-router-dom';

import SpinPageLoader from 'components/SpinPageLoader';
import { validateCode } from '../actions';
import { VerifyPage, mapDispatchToProps } from '../index';

describe('<VerifyPage />', () => {
  let props;

  beforeEach(() => {
    props = {
      validateCode: jest.fn(),
      location: {
        search: '?code=CODE',
      },
      verifypage: {
        validated: false,
        validateErr: false,
      },
    };
  });

  it('should render `VerifyPage`', () => {
    const wrapper = shallow(<VerifyPage {...props} />);
    expect(wrapper.contains(<SpinPageLoader label="Validating..." />)).toEqual(true);
  });

  it('should `validateCode` on mount', () => {
    const wrapper = shallow(<VerifyPage {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.validateCode).toHaveBeenCalledTimes(1);
  });

  it('should `Redirect` after verifying', () => {
    props = {
      ...props,
      verifypage: {
        validated: false,
        validateErr: true,
      },
    };
    const wrapper = shallow(<VerifyPage {...props} />);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

  describe('mapDispatchToProps', () => {
    describe('validateCode', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.validateCode).toBeDefined();
      });

      it('should dispatch validateCode when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const code = 'CODE';
        result.validateCode(code);
        expect(dispatch).toHaveBeenCalledWith(validateCode(code));
      });
    });
  });
});
