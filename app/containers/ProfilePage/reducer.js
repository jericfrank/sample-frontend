/*
 *
 * ProfilePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  FETCH_USER_INFO,
  USER_INFO_FETCHED,
  FETCH_USER_INFO_ERR,
  SAVE_PROFILE,
  PROFILE_SAVED,
  SAVE_PROFILE_ERR,
  SAVE_ATTACHMENTS,
  ATTACHMENTS_SAVED,
  SAVE_ATTACHMENTS_ERR,
  DEACTIVATE_ACCOUNT,
  ACCOUNT_DEACTIVATED,
  DEACTIVATE_ACCOUNT_ERR,
  RESET_PROFILE_FORM,
} from './constants';

const initialState = fromJS({
  fetching: true,
  fetched: false,
  fetchErr: false,
  saving: false,
  saved: false,
  saveErr: false,
  deactivating: false,
  deactivated: false,
  deactivateErr: false,
  user: null,
});

function profilePageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_USER_INFO:
      return state
        .set('fetching', true)
        .set('fetched', false)
        .set('fetchErr', false)
        .set('user', null);

    case USER_INFO_FETCHED:
      return state
        .set('fetching', false)
        .set('fetched', true)
        .set('fetchErr', false)
        .set('user', action.user);

    case FETCH_USER_INFO_ERR:
      return state
        .set('fetching', false)
        .set('fetched', false)
        .set('fetchErr', true)
        .set('user', null);

    case SAVE_PROFILE:
      return state
        .set('saving', true)
        .set('saved', false)
        .set('saveErr', false);

    case PROFILE_SAVED:
      return state
        .set('saving', false)
        .set('saved', true)
        .set('saveErr', false);

    case SAVE_PROFILE_ERR:
      return state
        .set('saving', false)
        .set('saved', false)
        .set('saveErr', true);

    case SAVE_ATTACHMENTS:
      return state
        .set('saving', true)
        .set('saved', false)
        .set('saveErr', false);

    case ATTACHMENTS_SAVED:
      return state
        .set('saving', false)
        .set('saved', true)
        .set('saveErr', false);

    case SAVE_ATTACHMENTS_ERR:
      return state
        .set('saving', false)
        .set('saved', false)
        .set('saveErr', true);

    case DEACTIVATE_ACCOUNT:
      return state
        .set('deactivating', true)
        .set('deactivated', false)
        .set('deactivateErr', false);

    case ACCOUNT_DEACTIVATED:
      return state
        .set('deactivating', false)
        .set('deactivated', true)
        .set('deactivateErr', false);

    case DEACTIVATE_ACCOUNT_ERR:
      return state
        .set('deactivating', false)
        .set('deactivated', false)
        .set('deactivateErr', true);

    case RESET_PROFILE_FORM:
      return state
        .set('fetching', true)
        .set('fetched', false)
        .set('fetchErr', false)
        .set('saving', false)
        .set('saved', false)
        .set('saveErr', false)
        .set('deactivating', false)
        .set('deactivated', false)
        .set('deactivateErr', false);

    default:
      return state;
  }
}

export default profilePageReducer;
