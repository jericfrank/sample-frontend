/**
 *
 * ProfilePage
 *
 */

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Row, Col, Card, Form, Button, Popconfirm, message } from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import urlParams from 'utils/urlParams';

import ProfileBasicInfo from 'components/ProfileBasicInfo';
import ProfileContactInfo from 'components/ProfileContactInfo';
import ProfileAttachments from 'containers/ProfileAttachments';

import Wrapper from './Wrapper';
import makeSelectProfilePage from './selectors';
import { HAS_FAILED_UPLOAD_MSG } from './constants';
import {
  fetchUserInfo,
  saveProfile,
  deactivateAccount,
  resetProfileForm,
} from './actions';
import reducer from './reducer';
import saga from './saga';

export class ProfilePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    hasUploadError: false,
  }

  componentDidMount() {
    this.props.getUserInfo();
  }

  componentWillUnmount() {
    this.props.resetForm();
  }

  onUploadStatusChange = (hasUploadError) => {
    this.setState({ hasUploadError });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      const { hasUploadError } = this.state;

      if (hasUploadError) {
        message.error(HAS_FAILED_UPLOAD_MSG);
      }

      if (!err && !hasUploadError) {
        this.props.onSaveProfile(values);
      }
    });
  }

  render() {
    const { profilepage, form, onDeactivateAccount } = this.props;
    const { getFieldDecorator, setFieldsValue } = form;
    const {
      fetching, user, fetchErr,
      saving, saved,
      deactivating, deactivated,
    } = profilepage;

    if (saved) {
      const urlParamsObj = urlParams(this.props.location.search);
      const redirectRoute = _.result(urlParamsObj, 'redirect', '/');
      return <Redirect to={decodeURIComponent(redirectRoute)} />;
    }

    if (deactivated || fetchErr) {
      return <Redirect to="/logout" />;
    }

    return (
      <div className="content-wrapper with-bg">
        <Helmet>
          <title>My Profile</title>
          <meta name="description" content="Description of My Profile" />
        </Helmet>

        <Row style={{ color: '#484859 !important' }}>
          <Col xs={2} sm={2} md={1} lg={1} xl={5}></Col>
          <Col xs={20} sm={20} md={22} lg={22} xl={14}>
            <h1
              className="text-center font-champ-bold dark-navy-blue"
              style={{ marginTop: '20px' }}
            >
              My Profile
            </h1>

            <Wrapper>
              <Card loading={fetching} className="placeholder-dark">
                <Form onSubmit={this.handleSubmit} className="login-form">
                  <h4 className="label-divider">BASIC INFORMATION</h4>
                  <ProfileBasicInfo
                    user={user}
                    getFieldDecorator={getFieldDecorator}
                    setFieldsValue={setFieldsValue}
                  />

                  <h4 className="label-divider">CONTACT INFORMATION</h4>
                  <ProfileContactInfo
                    user={user}
                    getFieldDecorator={getFieldDecorator}
                    setFieldsValue={setFieldsValue}
                  />

                  <h4 className="label-divider">ATTACHMENTS (optional)</h4>
                  <ProfileAttachments
                    user={user}
                    getFieldDecorator={getFieldDecorator}
                    setFieldsValue={setFieldsValue}
                    changeUploadStatus={this.onUploadStatusChange}
                  />

                  <div className="clear"></div>
                  <Button
                    htmlType="submit"
                    type="primary"
                    size="large"
                    className="pull-right"
                    loading={saving}
                    style={{ marginTop: '20px' }}
                  >
                    Save
                  </Button>
                  <Popconfirm
                    title="Are you sure you want to deactivate your account?"
                    onConfirm={onDeactivateAccount}
                    okText="Yes"
                    okType="danger"
                    cancelText="No"
                  >
                    <Button
                      htmlType="button"
                      type="danger"
                      size="large"
                      className="pull-right"
                      loading={deactivating}
                      style={{ marginTop: '20px', marginRight: '10px' }}
                    >
                      Deactivate Account
                    </Button>
                  </Popconfirm>
                </Form>
              </Card>
            </Wrapper>
          </Col>
          <Col xs={2} sm={2} md={1} lg={1} xl={5}></Col>
        </Row>
      </div>
    );
  }
}

ProfilePage.propTypes = {
  getUserInfo: PropTypes.func.isRequired,
  profilepage: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  onSaveProfile: PropTypes.func.isRequired,
  onDeactivateAccount: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  profilepage: makeSelectProfilePage(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getUserInfo: () => dispatch(fetchUserInfo()),
    onSaveProfile: (user) => dispatch(saveProfile(user)),
    onDeactivateAccount: () => dispatch(deactivateAccount()),
    resetForm: () => dispatch(resetProfileForm()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'profilePage', reducer });
const withSaga = injectSaga({ key: 'profilePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Form.create()(ProfilePage));
