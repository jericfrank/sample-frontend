import styled from 'styled-components';
const Wrapper = styled.div`
  margin-bottom: 60px;

  .label-divider {
    font-size: 12px;
    font-weight: bold;
    color: #8b8b8b;
    margin: 20px 0px 20px 0px;

    &:first-child {
      margin-top: 0px;
    }
  }

  .ant-form-item-label {
    line-height: normal;

    label {
      font-size: 13px !important;
      color: #525252;
    }
  }

  .card-table .ant-card-body {
    padding: 0px;
  }

  .ant-pagination.ant-table-pagination {
    margin-right: 20px;
  }
`;

export default Wrapper;
