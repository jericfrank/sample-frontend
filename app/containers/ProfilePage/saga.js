import _ from 'lodash';
import CryptoJS from 'crypto-js';
import {
  all, take, call, cancel, put, takeLatest, select,
} from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { getRequest, postRequest } from 'utils/request';
import { message as antdMessage } from 'antd';

import { makeSelectJobPage } from 'containers/JobPage/selectors';
import { applyJobSuccess, applyJobError, applyJobReset } from 'containers/ApplyPage/actions';
import { setUserAuth } from 'containers/App/actions';
import {
  userInfoFetched, fetchUserInfoErr,
  profileSaved, saveProfileErr, resetProfileForm,
  accountDeactivated, deactivateAccountErr,
} from './actions';
import {
  FETCH_USER_INFO, SAVE_PROFILE, SAVE_ATTACHMENTS, DEACTIVATE_ACCOUNT, UPDATE_SUCCESS,
} from './constants';

export function* fetchUserInfo() {
  try {
    const user = yield call(getRequest, '/hr/applicants/info');
    yield put(userInfoFetched({
      ...user,
      location: user.address,
    }));
  } catch (err) {
    yield call(antdMessage.error, 'Something went wrong when fetching genders. Please try again.');
    yield put(fetchUserInfoErr());
  }
}

export function* saveProfile({ user }) {
  try {
    let payload = { ...user };

    if (payload.application_letter === undefined) {
      payload = _.omit(payload, 'application_letter');
    }

    if (payload.cv_resume === undefined) {
      payload = _.omit(payload, 'cv_resume');
    }

    const res = yield call(postRequest, '/applicants/profile', payload);

    const applications = yield call(getRequest, '/hr/applicants/job_applications');
    const userInfo = { ...res, applications };

    const token = localStorage.getItem('jobportalToken');
    const encrypt = yield call(CryptoJS.AES.encrypt, JSON.stringify(userInfo), token);
    localStorage.setItem('jobportalUser', encrypt);

    yield call(antdMessage.success, UPDATE_SUCCESS);
    yield all([
      put(setUserAuth({ ...userInfo, location: res.address })),
      put(profileSaved()),
    ]);
  } catch (err) {
    yield call(antdMessage.error, 'Something went wrong saving profile. Please try again.');
    yield put(saveProfileErr());
  }
}

export function* saveAttachments({ user, qAnswers }) {
  try {
    const { jobDetails } = yield select(makeSelectJobPage());
    let payload = { ...user };

    if (payload.application_letter === undefined) {
      payload = _.omit(payload, 'application_letter');
    }

    if (payload.cv_resume === undefined) {
      payload = _.omit(payload, 'cv_resume');
    }

    const res = yield call(postRequest, '/applicants/profile', payload);
    const { id, job_question } = jobDetails;
    const answers = _.map(qAnswers, (value, index) => {
      let answer = value;
      if (_.isArray(answer)) {
        answer = answer.join(', ');
      }
      return {
        answer,
        question: _.chain(job_question)
          .flatten()
          .find(['id', _.parseInt(index, 10)])
          .get('question', null)
          .value(),
      };
    });
    const response = yield call(postRequest, '/hr/applicants/apply', {
      job_id: id,
      answers,
    });

    const applications = yield call(getRequest, '/hr/applicants/job_applications');
    const userInfo = { ...res, applications };

    const token = localStorage.getItem('jobportalToken');
    const encrypt = yield call(CryptoJS.AES.encrypt, JSON.stringify(userInfo), token);
    localStorage.setItem('jobportalUser', encrypt);

    yield call(antdMessage.success, UPDATE_SUCCESS);
    yield all([
      put(setUserAuth({ ...userInfo, location: res.address })),
      put(profileSaved()),
      put(applyJobSuccess(response)),
      put(resetProfileForm()),
      put(applyJobReset()),
    ]);
  } catch (err) {
    yield call(antdMessage.error, 'Something went wrong saving profile. Please try again.');
    yield all([
      put(saveProfileErr()),
      put(applyJobError(err)),
    ]);
  }
}

export function* deactivateAccount() {
  try {
    yield call(postRequest, '/applicants/auth/deactivate');
    yield call(antdMessage.success, 'Account successfully deactivated!');
    yield put(accountDeactivated());
  } catch (err) {
    yield call(antdMessage.error, 'Something went wrong deactivating account. Please try again.');
    yield put(deactivateAccountErr());
  }
}

// Individual exports for testing
export default function* watchProfilePage() {
  const watchers = yield all([
    takeLatest(FETCH_USER_INFO, fetchUserInfo),
    takeLatest(SAVE_PROFILE, saveProfile),
    takeLatest(SAVE_ATTACHMENTS, saveAttachments),
    takeLatest(DEACTIVATE_ACCOUNT, deactivateAccount),
  ]);

  yield take(LOCATION_CHANGE);
  yield cancel(...watchers);
}
