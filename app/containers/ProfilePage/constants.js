/*
 *
 * ProfilePage constants
 *
 */

export const FETCH_USER_INFO = 'app/ProfilePage/FETCH_USER_INFO';
export const USER_INFO_FETCHED = 'app/ProfilePage/USER_INFO_FETCHED';
export const FETCH_USER_INFO_ERR = 'app/ProfilePage/FETCH_USER_INFO_ERR';

export const SAVE_PROFILE = 'app/ProfilePage/SAVE_PROFILE';
export const PROFILE_SAVED = 'app/ProfilePage/PROFILE_SAVED';
export const RESET_PROFILE_FORM = 'app/ProfilePage/RESET_PROFILE_FORM';
export const SAVE_PROFILE_ERR = 'app/ProfilePage/SAVE_PROFILE_ERR';
export const SAVE_ATTACHMENTS = 'app/ProfilePage/SAVE_ATTACHMENTS';
export const ATTACHMENTS_SAVED = 'app/ProfilePage/ATTACHMENTS_SAVED';
export const SAVE_ATTACHMENTS_ERR = 'app/ProfilePage/SAVE_ATTACHMENTS_ERR';
export const DEACTIVATE_ACCOUNT = 'app/ProfilePage/DEACTIVATE_ACCOUNT';
export const ACCOUNT_DEACTIVATED = 'app/ProfilePage/ACCOUNT_DEACTIVATED';
export const DEACTIVATE_ACCOUNT_ERR = 'app/ProfilePage/DEACTIVATE_ACCOUNT_ERR';

export const UPDATE_SUCCESS = 'Successfully saved!';
export const HAS_FAILED_UPLOAD_MSG = 'You have failed file upload(s). Please try removing/uploading it again.';
export const FIELDS = [
  'first_name',
  'middle_name',
  'last_name',
  'gender_id',
  'email',
  'mobile_no',
];
