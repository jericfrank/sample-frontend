
import { fromJS } from 'immutable';
import {
  fetchUserInfo, userInfoFetched, fetchUserInfoErr,
  saveProfile, profileSaved, saveProfileErr,
  saveAttachments, attachmentsSaved, saveAttachmentsErr,
  deactivateAccount, accountDeactivated, deactivateAccountErr,
  resetProfileForm,
} from '../actions';

import profilePageReducer from '../reducer';

describe('profilePageReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({
      fetching: true,
      fetched: false,
      fetchErr: false,
      saving: false,
      saved: false,
      saveErr: false,
      deactivating: false,
      deactivated: false,
      deactivateErr: false,
      user: null,
    });
  });

  it('returns the initial state', () => {
    expect(profilePageReducer(undefined, {})).toEqual(state);
  });

  it('should handle the fetchUserInfo action correctly', () => {
    const expectedResult = state
      .set('fetching', true)
      .set('fetched', false)
      .set('fetchErr', false)
      .set('user', null);
    expect(profilePageReducer(state, fetchUserInfo())).toEqual(expectedResult);
  });

  it('should handle the userInfoFetched action correctly', () => {
    const user = [{ id: 1, description: 'Male' }];
    const expectedResult = state
      .set('fetching', false)
      .set('fetched', true)
      .set('fetchErr', false)
      .set('user', user);
    expect(profilePageReducer(state, userInfoFetched(user)))
      .toEqual(expectedResult);
  });

  it('should handle the fetchUserInfoErr action correctly', () => {
    const expectedResult = state
      .set('fetching', false)
      .set('fetched', false)
      .set('fetchErr', true)
      .set('user', null);
    expect(profilePageReducer(state, fetchUserInfoErr()))
      .toEqual(expectedResult);
  });

  it('should handle the saveProfile action correctly', () => {
    const expectedResult = state
      .set('saving', true)
      .set('saved', false)
      .set('saveErr', false);
    expect(profilePageReducer(state, saveProfile()))
      .toEqual(expectedResult);
  });

  it('should handle the profileSaved action correctly', () => {
    const expectedResult = state
      .set('saving', false)
      .set('saved', true)
      .set('saveErr', false);
    expect(profilePageReducer(state, profileSaved()))
      .toEqual(expectedResult);
  });

  it('should handle the saveProfileErr action correctly', () => {
    const expectedResult = state
      .set('saving', false)
      .set('saved', false)
      .set('saveErr', true);
    expect(profilePageReducer(state, saveProfileErr()))
      .toEqual(expectedResult);
  });

  it('should handle the saveAttachments action correctly', () => {
    const expectedResult = state
      .set('saving', true)
      .set('saved', false)
      .set('saveErr', false);
    expect(profilePageReducer(state, saveAttachments()))
      .toEqual(expectedResult);
  });

  it('should handle the attachmentsSaved action correctly', () => {
    const expectedResult = state
      .set('saving', false)
      .set('saved', true)
      .set('saveErr', false);
    expect(profilePageReducer(state, attachmentsSaved()))
      .toEqual(expectedResult);
  });

  it('should handle the saveAttachmentsErr action correctly', () => {
    const expectedResult = state
      .set('saving', false)
      .set('saved', false)
      .set('saveErr', true);
    expect(profilePageReducer(state, saveAttachmentsErr()))
      .toEqual(expectedResult);
  });

  it('should handle the deactivateAccount action correctly', () => {
    const expectedResult = state
      .set('deactivating', true)
      .set('deactivated', false)
      .set('deactivateErr', false);
    expect(profilePageReducer(state, deactivateAccount()))
      .toEqual(expectedResult);
  });

  it('should handle the accountDeactivated action correctly', () => {
    const expectedResult = state
      .set('deactivating', false)
      .set('deactivated', true)
      .set('deactivateErr', false);
    expect(profilePageReducer(state, accountDeactivated()))
      .toEqual(expectedResult);
  });

  it('should handle the deactivateAccountErr action correctly', () => {
    const expectedResult = state
      .set('deactivating', false)
      .set('deactivated', false)
      .set('deactivateErr', true);
    expect(profilePageReducer(state, deactivateAccountErr()))
      .toEqual(expectedResult);
  });

  it('should handle the resetProfileForm action correctly', () => {
    const expectedResult = state
      .set('fetching', true)
      .set('fetched', false)
      .set('fetchErr', false)
      .set('saving', false)
      .set('saved', false)
      .set('saveErr', false)
      .set('deactivating', false)
      .set('deactivated', false)
      .set('deactivateErr', false);
    expect(profilePageReducer(state, resetProfileForm()))
      .toEqual(expectedResult);
  });
});
