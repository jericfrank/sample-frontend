import { fromJS } from 'immutable';

import makeSelectProfilePage, { selectProfilePageDomain } from '../selectors';

describe('selectProfilePageDomain', () => {
  it('should select `profilePage`', () => {
    const state = fromJS({
      profilePage: {
        test: 'test',
      },
    });
    expect(selectProfilePageDomain(state))
      .toEqual(state.get('profilePage'));
  });
});

describe('makeSelectProfilePage', () => {
  it('should select `profilePage`', () => {
    const profilePage = fromJS({
      test: 'test',
    });
    const mockedState = fromJS({
      profilePage,
    });
    expect(makeSelectProfilePage()(mockedState))
      .toEqual(profilePage.toJS());
  });
});
