/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { all, take, put, takeLatest, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';

import { setUserAuth } from 'containers/App/actions';
import { applyJobSuccess, applyJobError, applyJobReset } from 'containers/ApplyPage/actions';

import {
  userInfoFetched, fetchUserInfoErr,
  profileSaved, saveProfileErr, resetProfileForm,
  accountDeactivated, deactivateAccountErr,
} from '../actions';
import watchProfilePage, {
  fetchUserInfo, saveProfile, saveAttachments, deactivateAccount,
} from '../saga';
import {
  FETCH_USER_INFO, SAVE_PROFILE, SAVE_ATTACHMENTS, DEACTIVATE_ACCOUNT,
} from '../constants';

describe('fetchUserInfo Saga', () => {
  let fetchUserInfoCredsGenerator;
  let response;

  beforeEach(() => {
    fetchUserInfoCredsGenerator = fetchUserInfo();
    response = { address: 'Address' };

    const callDescriptor = fetchUserInfoCredsGenerator.next(response).value;
    expect(callDescriptor).toMatchSnapshot();
  });

  it('should dispatch the userInfoFetched action if it requests the data successfully', () => {
    const putDescriptor = fetchUserInfoCredsGenerator.next(response).value;
    expect(putDescriptor).toEqual(put(userInfoFetched({
      ...response,
      location: response.address,
    })));
  });

  describe('catch', () => {
    beforeEach(() => {
      const callDescriptor = fetchUserInfoCredsGenerator.throw().value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the fetchUserInfoErr action if the response errors', () => {
      const putDescriptor = fetchUserInfoCredsGenerator.next().value;
      expect(putDescriptor).toEqual(put(fetchUserInfoErr()));
    });
  });
});

describe('saveProfile Saga', () => {
  let saveProfileCredsGenerator;
  let user;
  let applications;
  let encrypt;

  describe('without files', () => {
    beforeEach(() => {
      class LocalStorageMock {
        constructor() {
          this.store = {
            jobportalToken: 'TOKEN HERE',
          };
        }

        clear() {
          this.store = {};
        }

        getItem(key) {
          return this.store[key] || null;
        }

        setItem(key, value) {
          this.store[key] = value.toString();
        }

        removeItem(key) {
          delete this.store[key];
        }
      }
      global.localStorage = new (LocalStorageMock)();

      user = { address: 'Address' };
      applications = [];
      encrypt = 'ENCRYPT HERE';
      saveProfileCredsGenerator = saveProfile({ user });

      const callDescriptor = saveProfileCredsGenerator.next().value;
      expect(callDescriptor).toMatchSnapshot();

      const postReqDescriptor = saveProfileCredsGenerator.next(user).value;
      expect(postReqDescriptor).toMatchSnapshot();

      const getReqDescriptor = saveProfileCredsGenerator.next(applications).value;
      expect(getReqDescriptor).toMatchSnapshot();

      const cryptoDescriptor = saveProfileCredsGenerator.next(encrypt).value;
      expect(cryptoDescriptor).toMatchSnapshot();
    });

    it('should dispatch the setUserAuth action if it requests the data successfully', () => {
      const putDescriptor = saveProfileCredsGenerator.next().value;
      expect(putDescriptor).toEqual(all([
        put(setUserAuth({ ...user, location: user.address, applications })),
        put(profileSaved()),
      ]));
    });
  });

  describe('with files', () => {
    beforeEach(() => {
      class LocalStorageMock {
        constructor() {
          this.store = {
            jobportalToken: 'TOKEN HERE',
          };
        }

        clear() {
          this.store = {};
        }

        getItem(key) {
          return this.store[key] || null;
        }

        setItem(key, value) {
          this.store[key] = value.toString();
        }

        removeItem(key) {
          delete this.store[key];
        }
      }
      global.localStorage = new (LocalStorageMock)();

      user = {
        address: 'Address',
        application_letter: 'Application Letter',
        cv_resume: 'CV Resume',
      };
      applications = [];
      encrypt = 'ENCRYPT HERE';
      saveProfileCredsGenerator = saveProfile({ user });

      const callDescriptor = saveProfileCredsGenerator.next().value;
      expect(callDescriptor).toMatchSnapshot();

      const postReqDescriptor = saveProfileCredsGenerator.next(user).value;
      expect(postReqDescriptor).toMatchSnapshot();

      const getReqDescriptor = saveProfileCredsGenerator.next(applications).value;
      expect(getReqDescriptor).toMatchSnapshot();

      const cryptoDescriptor = saveProfileCredsGenerator.next(encrypt).value;
      expect(cryptoDescriptor).toMatchSnapshot();
    });

    it('should dispatch the setUserAuth action if it requests the data successfully', () => {
      const putDescriptor = saveProfileCredsGenerator.next().value;
      expect(putDescriptor).toEqual(all([
        put(setUserAuth({ ...user, location: user.address, applications })),
        put(profileSaved()),
      ]));
    });
  });

  describe('catch', () => {
    beforeEach(() => {
      const callDescriptor = saveProfileCredsGenerator.throw().value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the saveProfileErr action if the  errors', () => {
      const putDescriptor = saveProfileCredsGenerator.next().value;
      expect(putDescriptor).toEqual(put(saveProfileErr()));
    });
  });
});

describe('saveAttachments Saga', () => {
  let saveAttachmentsCredsGenerator;
  let user;
  let jobDetails;
  let qAnswers;
  let applications;
  let encrypt;

  describe('without files', () => {
    beforeEach(() => {
      class LocalStorageMock {
        constructor() {
          this.store = {
            jobportalToken: 'TOKEN HERE',
          };
        }

        clear() {
          this.store = {};
        }

        getItem(key) {
          return this.store[key] || null;
        }

        setItem(key, value) {
          this.store[key] = value.toString();
        }

        removeItem(key) {
          delete this.store[key];
        }
      }
      global.localStorage = new (LocalStorageMock)();

      jobDetails = { id: 1, job_question: [{ id: 9, question: 'Question?' }] };
      user = { address: 'Address' };
      qAnswers = { 9: '1' };
      applications = [];
      encrypt = 'ENCRYPT HERE';
      saveAttachmentsCredsGenerator = saveAttachments({ user, qAnswers });

      const callDescriptor = saveAttachmentsCredsGenerator.next().value;
      expect(callDescriptor).toMatchSnapshot();

      const jobDetailsDescriptor = saveAttachmentsCredsGenerator.next({ jobDetails }).value;
      expect(jobDetailsDescriptor).toMatchSnapshot();

      const postReqDescriptor = saveAttachmentsCredsGenerator.next(user).value;
      expect(postReqDescriptor).toMatchSnapshot();

      const qAnswersDescriptor = saveAttachmentsCredsGenerator.next(qAnswers).value;
      expect(qAnswersDescriptor).toMatchSnapshot();

      const getReqDescriptor = saveAttachmentsCredsGenerator.next(applications).value;
      expect(getReqDescriptor).toMatchSnapshot();

      const cryptoDescriptor = saveAttachmentsCredsGenerator.next(encrypt).value;
      expect(cryptoDescriptor).toMatchSnapshot();
    });

    it('should dispatch the setUserAuth action if it requests the data successfully', () => {
      const putDescriptor = saveAttachmentsCredsGenerator.next(qAnswers).value;
      expect(putDescriptor).toEqual(all([
        put(setUserAuth({ ...user, location: user.address, applications })),
        put(profileSaved()),
        put(applyJobSuccess(qAnswers)),
        put(resetProfileForm()),
        put(applyJobReset()),
      ]));
    });
  });

  describe('with files', () => {
    beforeEach(() => {
      class LocalStorageMock {
        constructor() {
          this.store = {
            jobportalToken: 'TOKEN HERE',
          };
        }

        clear() {
          this.store = {};
        }

        getItem(key) {
          return this.store[key] || null;
        }

        setItem(key, value) {
          this.store[key] = value.toString();
        }

        removeItem(key) {
          delete this.store[key];
        }
      }
      global.localStorage = new (LocalStorageMock)();

      jobDetails = { id: 1, job_question: [{ id: 9, question: 'Question?' }] };
      user = {
        address: 'Address',
        application_letter: 'Application Letter',
        cv_resume: 'CV Resume',
      };
      qAnswers = { 9: [1, 2] };
      applications = [];
      encrypt = 'ENCRYPT HERE';
      saveAttachmentsCredsGenerator = saveAttachments({ user, qAnswers });

      const callDescriptor = saveAttachmentsCredsGenerator.next().value;
      expect(callDescriptor).toMatchSnapshot();

      const jobDetailsDescriptor = saveAttachmentsCredsGenerator.next({ jobDetails }).value;
      expect(jobDetailsDescriptor).toMatchSnapshot();

      const postReqDescriptor = saveAttachmentsCredsGenerator.next(user).value;
      expect(postReqDescriptor).toMatchSnapshot();

      const qAnswersDescriptor = saveAttachmentsCredsGenerator.next(qAnswers).value;
      expect(qAnswersDescriptor).toMatchSnapshot();

      const getReqDescriptor = saveAttachmentsCredsGenerator.next(applications).value;
      expect(getReqDescriptor).toMatchSnapshot();

      const cryptoDescriptor = saveAttachmentsCredsGenerator.next(encrypt).value;
      expect(cryptoDescriptor).toMatchSnapshot();
    });

    it('should dispatch the setUserAuth action if it requests the data successfully', () => {
      const putDescriptor = saveAttachmentsCredsGenerator.next(qAnswers).value;
      expect(putDescriptor).toEqual(all([
        put(setUserAuth({ ...user, location: user.address, applications })),
        put(profileSaved()),
        put(applyJobSuccess(qAnswers)),
        put(resetProfileForm()),
        put(applyJobReset()),
      ]));
    });
  });

  describe('catch', () => {
    let err;

    beforeEach(() => {
      err = new Error();
      const callDescriptor = saveAttachmentsCredsGenerator.throw(err).value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the saveAttachmentsErr action if the  errors', () => {
      const putDescriptor = saveAttachmentsCredsGenerator.next(err).value;
      expect(putDescriptor).toEqual(all([
        put(saveProfileErr()),
        put(applyJobError(err)),
      ]));
    });
  });
});

describe('deactivateAccount Saga', () => {
  let deactivateAccountCredsGenerator;

  beforeEach(() => {
    deactivateAccountCredsGenerator = deactivateAccount();

    const callDescriptor = deactivateAccountCredsGenerator.next().value;
    expect(callDescriptor).toMatchSnapshot();

    const antdMsgCallDescriptor = deactivateAccountCredsGenerator.next().value;
    expect(antdMsgCallDescriptor).toMatchSnapshot();
  });

  it('should dispatch the accountDeactivated action if it requests the data successfully', () => {
    const putDescriptor = deactivateAccountCredsGenerator.next().value;
    expect(putDescriptor).toEqual(put(accountDeactivated()));
  });

  describe('catch', () => {
    beforeEach(() => {
      const callDescriptor = deactivateAccountCredsGenerator.throw().value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should call the deactivateAccountErr action if the  errors', () => {
      const putDescriptor = deactivateAccountCredsGenerator.next().value;
      expect(putDescriptor).toEqual(put(deactivateAccountErr()));
    });
  });
});

describe('watchProfilePage Saga', () => {
  const generator = watchProfilePage();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const effect = generator.next().value;
    expect(effect).toEqual(all([
      takeLatest(FETCH_USER_INFO, fetchUserInfo),
      takeLatest(SAVE_PROFILE, saveProfile),
      takeLatest(SAVE_ATTACHMENTS, saveAttachments),
      takeLatest(DEACTIVATE_ACCOUNT, deactivateAccount),
    ]));
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(...mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
