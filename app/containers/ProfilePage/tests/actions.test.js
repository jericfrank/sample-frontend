import {
  fetchUserInfo, userInfoFetched, fetchUserInfoErr,
  saveProfile, profileSaved, saveProfileErr,
  saveAttachments, attachmentsSaved, saveAttachmentsErr,
  deactivateAccount, accountDeactivated, deactivateAccountErr,
  resetProfileForm,
} from '../actions';
import {
  FETCH_USER_INFO, USER_INFO_FETCHED, FETCH_USER_INFO_ERR,
  SAVE_PROFILE, PROFILE_SAVED, SAVE_PROFILE_ERR,
  SAVE_ATTACHMENTS, ATTACHMENTS_SAVED, SAVE_ATTACHMENTS_ERR,
  DEACTIVATE_ACCOUNT, ACCOUNT_DEACTIVATED, DEACTIVATE_ACCOUNT_ERR,
  RESET_PROFILE_FORM,
} from '../constants';

describe('ProfilePage actions', () => {
  describe('fetchUserInfo', () => {
    it('has a type of FETCH_USER_INFO', () => {
      const expected = {
        type: FETCH_USER_INFO,
      };
      expect(fetchUserInfo()).toEqual(expected);
    });
  });

  describe('userInfoFetched', () => {
    it('has a type of USER_INFO_FETCHED', () => {
      const user = {};
      const expected = {
        type: USER_INFO_FETCHED,
        user,
      };
      expect(userInfoFetched(user)).toEqual(expected);
    });
  });

  describe('fetchUserInfoErr', () => {
    it('has a type of FETCH_USER_INFO_ERR', () => {
      const expected = {
        type: FETCH_USER_INFO_ERR,
      };
      expect(fetchUserInfoErr()).toEqual(expected);
    });
  });

  describe('saveProfile', () => {
    it('has a type of SAVE_PROFILE', () => {
      const user = {};
      const expected = {
        type: SAVE_PROFILE,
        user,
      };
      expect(saveProfile(user)).toEqual(expected);
    });
  });

  describe('profileSaved', () => {
    it('has a type of PROFILE_SAVED', () => {
      const expected = {
        type: PROFILE_SAVED,
      };
      expect(profileSaved()).toEqual(expected);
    });
  });

  describe('saveProfileErr', () => {
    it('has a type of SAVE_PROFILE_ERR', () => {
      const expected = {
        type: SAVE_PROFILE_ERR,
      };
      expect(saveProfileErr()).toEqual(expected);
    });
  });

  describe('saveAttachments', () => {
    it('has a type of SAVE_ATTACHMENTS', () => {
      const user = {};
      const qAnswers = {};
      const expected = {
        type: SAVE_ATTACHMENTS,
        user,
        qAnswers,
      };
      expect(saveAttachments(user, qAnswers)).toEqual(expected);
    });
  });

  describe('attachmentsSaved', () => {
    it('has a type of ATTACHMENTS_SAVED', () => {
      const expected = {
        type: ATTACHMENTS_SAVED,
      };
      expect(attachmentsSaved()).toEqual(expected);
    });
  });

  describe('saveAttachmentsErr', () => {
    it('has a type of SAVE_ATTACHMENTS_ERR', () => {
      const expected = {
        type: SAVE_ATTACHMENTS_ERR,
      };
      expect(saveAttachmentsErr()).toEqual(expected);
    });
  });

  describe('deactivateAccount', () => {
    it('has a type of DEACTIVATE_ACCOUNT', () => {
      const expected = {
        type: DEACTIVATE_ACCOUNT,
      };
      expect(deactivateAccount()).toEqual(expected);
    });
  });

  describe('accountDeactivated', () => {
    it('has a type of ACCOUNT_DEACTIVATED', () => {
      const expected = {
        type: ACCOUNT_DEACTIVATED,
      };
      expect(accountDeactivated()).toEqual(expected);
    });
  });

  describe('deactivateAccountErr', () => {
    it('has a type of DEACTIVATE_ACCOUNT_ERR', () => {
      const expected = {
        type: DEACTIVATE_ACCOUNT_ERR,
      };
      expect(deactivateAccountErr()).toEqual(expected);
    });
  });

  describe('resetProfileForm', () => {
    it('has a type of RESET_PROFILE_FORM', () => {
      const expected = {
        type: RESET_PROFILE_FORM,
      };
      expect(resetProfileForm()).toEqual(expected);
    });
  });
});
