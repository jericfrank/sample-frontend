import React from 'react';
import { Redirect } from 'react-router-dom';
import { shallow } from 'enzyme';
import { message } from 'antd';

import ProfileBasicInfo from 'components/ProfileBasicInfo';
import ProfileContactInfo from 'components/ProfileContactInfo';
import ProfileAttachments from 'containers/ProfileAttachments';
import { fetchUserInfo, saveProfile, deactivateAccount, resetProfileForm } from '../actions';
import { ProfilePage, mapDispatchToProps } from '../index';

describe('<ProfilePage />', () => {
  let props;

  beforeEach(() => {
    props = {
      getUserInfo: jest.fn(),
      onSaveProfile: jest.fn(),
      onDeactivateAccount: jest.fn(),
      resetForm: jest.fn(),
      location: { search: '?redirect=%2Fjobs%2F5%2Fapply' },
      profilepage: {
        user: {},
        fetching: false,
        saving: false,
        saved: false,
        deactivating: false,
      },
      form: {
        getFieldDecorator: () => jest.fn(),
        setFieldsValue: () => jest.fn(),
      },
    };

    message.error = jest.fn();
  });

  afterEach(() => {
    message.error.mockReset();
    message.error.mockRestore();
  });

  it('should render `ProfileBasicInfo`, `ProfileContactInfo` and `ProfileAttachments`', () => {
    const { user } = props.profilepage;
    const { getFieldDecorator, setFieldsValue } = props.form;
    const wrapper = shallow(<ProfilePage {...props} />);
    expect(wrapper.contains(
      <ProfileBasicInfo
        user={user}
        getFieldDecorator={getFieldDecorator}
        setFieldsValue={setFieldsValue}
      />
    )).toEqual(true);
    expect(wrapper.contains(
      <ProfileContactInfo
        user={user}
        getFieldDecorator={getFieldDecorator}
        setFieldsValue={setFieldsValue}
      />
    )).toEqual(true);
    expect(wrapper.contains(
      <ProfileAttachments
        user={user}
        getFieldDecorator={getFieldDecorator}
        setFieldsValue={setFieldsValue}
        changeUploadStatus={wrapper.instance().onUploadStatusChange}
      />
    )).toEqual(true);
  });

  it('should fetch userInfo after mount', () => {
    const wrapper = shallow(<ProfilePage {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.getUserInfo).toHaveBeenCalledTimes(1);
  });

  it('should fetch userInfo after mount', () => {
    const wrapper = shallow(<ProfilePage {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.getUserInfo).toHaveBeenCalledTimes(1);
  });

  it('should `resetForm` before `unmount`', () => {
    const wrapper = shallow(<ProfilePage {...props} />);
    wrapper.instance().componentWillUnmount();
    expect(props.resetForm).toHaveBeenCalledTimes(1);
  });

  it('should change `hasUploadError` state', () => {
    const wrapper = shallow(<ProfilePage {...props} />);
    expect(wrapper.state('hasUploadError')).toEqual(false);
    wrapper.instance().onUploadStatusChange(true);
    expect(wrapper.state('hasUploadError')).toEqual(true);
  });

  it('should call `onSaveProfile` when saving', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false),
      },
    };
    const wrapper = shallow(<ProfilePage {...props} />);
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.onSaveProfile).toHaveBeenCalledTimes(1);
  });

  it('should trigger `message.error`', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(false),
      },
    };
    const wrapper = shallow(<ProfilePage {...props} />);
    wrapper.setState({ hasUploadError: true });
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(message.error).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `onSaveProfile` when having an error on saving', () => {
    props = {
      ...props,
      form: {
        ...props.form,
        validateFields: (cb) => cb(true),
      },
    };
    const wrapper = shallow(<ProfilePage {...props} />);
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() });
    expect(props.onSaveProfile).toHaveBeenCalledTimes(0);
  });

  it('should `Redirect` the user to the previous page', () => {
    props = {
      ...props,
      profilepage: { ...props.profilepage, saved: true },
    };
    const wrapper = shallow(<ProfilePage {...props} />);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

  it('should `Redirect` the user to the previous page', () => {
    props = {
      ...props,
      profilepage: { ...props.profilepage, fetchErr: true },
    };
    const wrapper = shallow(<ProfilePage {...props} />);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

  describe('mapDispatchToProps', () => {
    describe('getUserInfo', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.getUserInfo).toBeDefined();
      });

      it('should dispatch fetchUserInfo when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.getUserInfo();
        expect(dispatch).toHaveBeenCalledWith(fetchUserInfo());
      });
    });

    describe('onSaveProfile', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onSaveProfile).toBeDefined();
      });

      it('should dispatch saveProfile when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const user = {};
        result.onSaveProfile(user);
        expect(dispatch).toHaveBeenCalledWith(saveProfile(user));
      });
    });

    describe('onDeactivateAccount', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onDeactivateAccount).toBeDefined();
      });

      it('should dispatch deactivateAccount when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.onDeactivateAccount();
        expect(dispatch).toHaveBeenCalledWith(deactivateAccount());
      });
    });

    describe('resetForm', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.resetForm).toBeDefined();
      });

      it('should dispatch resetProfileForm when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.resetForm();
        expect(dispatch).toHaveBeenCalledWith(resetProfileForm());
      });
    });
  });
});
