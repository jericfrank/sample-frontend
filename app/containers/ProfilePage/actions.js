/*
 *
 * ProfilePage actions
 *
 */

import {
  FETCH_USER_INFO,
  USER_INFO_FETCHED,
  FETCH_USER_INFO_ERR,
  SAVE_PROFILE,
  PROFILE_SAVED,
  SAVE_PROFILE_ERR,
  SAVE_ATTACHMENTS,
  ATTACHMENTS_SAVED,
  SAVE_ATTACHMENTS_ERR,
  DEACTIVATE_ACCOUNT,
  ACCOUNT_DEACTIVATED,
  DEACTIVATE_ACCOUNT_ERR,
  RESET_PROFILE_FORM,
} from './constants';

export function fetchUserInfo() {
  return {
    type: FETCH_USER_INFO,
  };
}

export function userInfoFetched(user) {
  return {
    type: USER_INFO_FETCHED,
    user,
  };
}

export function fetchUserInfoErr() {
  return {
    type: FETCH_USER_INFO_ERR,
  };
}

export function saveProfile(user) {
  return {
    type: SAVE_PROFILE,
    user,
  };
}

export function profileSaved() {
  return {
    type: PROFILE_SAVED,
  };
}

export function saveProfileErr() {
  return {
    type: SAVE_PROFILE_ERR,
  };
}

export function saveAttachments(user, qAnswers) {
  return {
    type: SAVE_ATTACHMENTS,
    user,
    qAnswers,
  };
}

export function attachmentsSaved() {
  return {
    type: ATTACHMENTS_SAVED,
  };
}

export function saveAttachmentsErr() {
  return {
    type: SAVE_ATTACHMENTS_ERR,
  };
}

export function deactivateAccount() {
  return {
    type: DEACTIVATE_ACCOUNT,
  };
}

export function accountDeactivated() {
  return {
    type: ACCOUNT_DEACTIVATED,
  };
}

export function deactivateAccountErr() {
  return {
    type: DEACTIVATE_ACCOUNT_ERR,
  };
}

export function resetProfileForm() {
  return {
    type: RESET_PROFILE_FORM,
  };
}
