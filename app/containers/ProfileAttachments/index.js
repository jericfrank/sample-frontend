/**
 *
 * ProfileAttachments
 *
 */

import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Spin, Row, Col, Form } from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import ProfileFileUpload from 'components/ProfileFileUpload';

import { fetchAttachments, removeAttachment } from './actions';
import makeSelectProfileAttachments from './selectors';
import reducer from './reducer';
import saga from './saga';

export class ProfileAttachments extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { user } = this.props;

    if (user) {
      this.props.getAttachments(user);
    }
  }

  handleUpload = (file, formItemName, hasUploadError) => {
    this.props.changeUploadStatus(hasUploadError);
    this.props.setFieldsValue(
      _.zipObject([formItemName], [_.result(file, 'response.filePath', null)])
    );
  }

  handleRemoveFile = (file, formItemName, hasUploadError) => {
    this.handleUpload(file, formItemName, hasUploadError);
    this.props.detachFile(file);
  }

  render() {
    const { getFieldDecorator, profileattachments } = this.props;
    const { fetching, appLetters, cvResumes } = profileattachments;

    const fileUploadProps = {
      onUploadSuccess: this.handleUpload,
      onUploadError: this.handleUpload,
      onFileRemove: this.handleRemoveFile,
    };

    return (
      <Spin spinning={fetching} tip="Fetching...">
        <Row gutter={16}>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <h3 className="text-center">
              Upload your Cover Letter
            </h3>
          </Col>
          <Col xs={0} sm={12} md={12} lg={12} xl={12}>
            <h3 className="text-center">Upload your CV/Resume</h3>
          </Col>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <Form.Item className="form-item">
              {getFieldDecorator('application_letter', { rules: [] })(
                <ProfileFileUpload
                  {...fileUploadProps}
                  formItemName="application_letter"
                  defaultFileList={appLetters}
                />
              )}
            </Form.Item>
          </Col>
          <Col xs={24} sm={0} md={0} lg={0} xl={0}>
            <h3 className="text-center">Upload your CV/Resume</h3>
          </Col>
          <Col xs={24} sm={12} md={12} lg={12} xl={12}>
            <Form.Item className="form-item">
              {getFieldDecorator('cv_resume', { rules: [] })(
                <ProfileFileUpload
                  {...fileUploadProps}
                  formItemName="cv_resume"
                  defaultFileList={cvResumes}
                />
              )}
            </Form.Item>
          </Col>
        </Row>
      </Spin>
    );
  }
}

ProfileAttachments.propTypes = {
  user: PropTypes.object.isRequired,
  getFieldDecorator: PropTypes.func.isRequired,
  setFieldsValue: PropTypes.func.isRequired,
  changeUploadStatus: PropTypes.func.isRequired,
  profileattachments: PropTypes.object.isRequired,
  getAttachments: PropTypes.func.isRequired,
  detachFile: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  profileattachments: makeSelectProfileAttachments(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getAttachments: (user) => dispatch(fetchAttachments(user)),
    detachFile: (file) => dispatch(removeAttachment(file)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'profileAttachments', reducer });
const withSaga = injectSaga({ key: 'profileAttachments', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ProfileAttachments);
