/*
 *
 * ProfileAttachments actions
 *
 */

import {
  FETCH_ATTACHMENTS,
  ATTACHMENTS_FETCHED,
  FETCH_ATTACHMENTS_ERR,
  REMOVE_ATTACHMENT,
} from './constants';

export function fetchAttachments(user) {
  return {
    type: FETCH_ATTACHMENTS,
    user,
  };
}

export function attachmentsFetched(appLetters, cvResumes) {
  return {
    type: ATTACHMENTS_FETCHED,
    appLetters,
    cvResumes,
  };
}

export function fetchAttachmentsErr() {
  return {
    type: FETCH_ATTACHMENTS_ERR,
  };
}

export function removeAttachment(file) {
  return {
    type: REMOVE_ATTACHMENT,
    file,
  };
}
