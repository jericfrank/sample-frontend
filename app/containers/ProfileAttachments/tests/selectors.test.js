import { fromJS } from 'immutable';

import makeSelectProfileAttachments, { selectProfileAttachmentsDomain } from '../selectors';

describe('selectProfileAttachmentsDomain', () => {
  it('should select `profileAttachments`', () => {
    const state = fromJS({
      profileAttachments: {
        test: 'test',
      },
    });
    expect(selectProfileAttachmentsDomain(state))
      .toEqual(state.get('profileAttachments'));
  });
});

describe('makeSelectProfileAttachments', () => {
  it('should select `profileAttachments`', () => {
    const profileAttachments = fromJS({
      test: 'test',
    });
    const mockedState = fromJS({
      profileAttachments,
    });
    expect(makeSelectProfileAttachments()(mockedState))
      .toEqual(profileAttachments.toJS());
  });
});
