
import {
  fetchAttachments,
  attachmentsFetched,
  fetchAttachmentsErr,
  removeAttachment,
} from '../actions';
import {
  FETCH_ATTACHMENTS,
  ATTACHMENTS_FETCHED,
  FETCH_ATTACHMENTS_ERR,
  REMOVE_ATTACHMENT,
} from '../constants';

describe('SignUpPage actions', () => {
  describe('fetchAttachments', () => {
    it('has a type of FETCH_ATTACHMENTS', () => {
      const user = {};
      const expected = {
        type: FETCH_ATTACHMENTS,
        user,
      };
      expect(fetchAttachments(user)).toEqual(expected);
    });
  });

  describe('attachmentsFetched', () => {
    it('has a type of ATTACHMENTS_FETCHED', () => {
      const appLetters = [];
      const cvResumes = [];
      const expected = {
        type: ATTACHMENTS_FETCHED,
        appLetters,
        cvResumes,
      };
      expect(attachmentsFetched(appLetters, cvResumes)).toEqual(expected);
    });
  });

  describe('fetchAttachmentsErr', () => {
    it('has a type of FETCH_ATTACHMENTS_ERR', () => {
      const expected = {
        type: FETCH_ATTACHMENTS_ERR,
      };
      expect(fetchAttachmentsErr()).toEqual(expected);
    });
  });

  describe('removeAttachment', () => {
    it('has a type of REMOVE_ATTACHMENT', () => {
      const file = {};
      const expected = {
        type: REMOVE_ATTACHMENT,
        file,
      };
      expect(removeAttachment(file)).toEqual(expected);
    });
  });
});
