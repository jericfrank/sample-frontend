import React from 'react';
import { shallow } from 'enzyme';
import { Spin } from 'antd';

import { fetchAttachments, removeAttachment } from '../actions';
import { ProfileAttachments, mapDispatchToProps } from '../index';

describe('<ProfileAttachments />', () => {
  let props;

  beforeEach(() => {
    props = {
      user: {},
      getFieldDecorator: () => jest.fn(),
      setFieldsValue: () => jest.fn(),
      getAttachments: jest.fn(),
      detachFile: jest.fn(),
      changeUploadStatus: jest.fn(),
      profileattachments: {
        fetching: false,
        appLetters: [],
        cvResumes: [],
      },
    };
  });

  it('should render `ProfileAttachments`', () => {
    const wrapper = shallow(<ProfileAttachments {...props} />);
    expect(wrapper.find(Spin)).toHaveLength(1);
  });

  it('should call `props.getAttachments`', () => {
    const wrapper = shallow(<ProfileAttachments {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.getAttachments).toHaveBeenCalledTimes(1);
  });

  it('should NOT call `props.getAttachments`', () => {
    props = { ...props, user: null };
    const wrapper = shallow(<ProfileAttachments {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.getAttachments).toHaveBeenCalledTimes(0);
  });

  it('should call `changeUploadStatus` and `setFieldsValue` props', () => {
    const spy = jest.spyOn(props, 'setFieldsValue');
    const wrapper = shallow(<ProfileAttachments {...props} />);
    wrapper.instance().handleUpload({}, 'cv_resume', false);
    expect(props.changeUploadStatus).toHaveBeenCalledTimes(1);
    expect(props.setFieldsValue).toHaveBeenCalledTimes(1);
    spy.mockReset();
    spy.mockRestore();
  });

  it('should call `handleRemoveFile` props', () => {
    const spy = jest.spyOn(props, 'setFieldsValue');
    const wrapper = shallow(<ProfileAttachments {...props} />);
    wrapper.instance().handleRemoveFile({}, 'cv_resume', false);
    expect(props.changeUploadStatus).toHaveBeenCalledTimes(1);
    expect(props.setFieldsValue).toHaveBeenCalledTimes(1);
    expect(props.detachFile).toHaveBeenCalledTimes(1);
    spy.mockReset();
    spy.mockRestore();
  });

  describe('mapDispatchToProps', () => {
    describe('getAttachments', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.getAttachments).toBeDefined();
      });

      it('should dispatch fetchAttachments when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const user = {};
        result.getAttachments(user);
        expect(dispatch).toHaveBeenCalledWith(fetchAttachments(user));
      });
    });

    describe('detachFile', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.detachFile).toBeDefined();
      });

      it('should dispatch removeAttachment when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const file = {};
        result.detachFile(file);
        expect(dispatch).toHaveBeenCalledWith(removeAttachment(file));
      });
    });
  });
});
