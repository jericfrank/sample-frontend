
import { fromJS } from 'immutable';
import {
  fetchAttachments,
  attachmentsFetched,
  fetchAttachmentsErr,
  removeAttachment,
} from '../actions';

import profileAttachmentsReducer from '../reducer';

describe('profileAttachmentsReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({
      fetching: false,
      fetched: false,
      fetchErr: false,
      appLetters: [],
      cvResumes: [],
    });
  });

  it('returns the initial state', () => {
    expect(profileAttachmentsReducer(undefined, {})).toEqual(state);
  });

  it('should handle the fetchAttachments action correctly', () => {
    const expectedResult = state
      .set('fetching', true)
      .set('fetched', false)
      .set('fetchErr', false)
      .set('appLetters', [])
      .set('cvResumes', []);
    expect(profileAttachmentsReducer(state, fetchAttachments()))
      .toEqual(expectedResult);
  });

  it('should handle the attachmentsFetched action correctly', () => {
    const appLetters = [];
    const cvResumes = [];
    const expectedResult = state
      .set('fetching', false)
      .set('fetched', true)
      .set('fetchErr', false)
      .set('appLetters', appLetters)
      .set('cvResumes', cvResumes);
    expect(profileAttachmentsReducer(state, attachmentsFetched(appLetters, cvResumes)))
      .toEqual(expectedResult);
  });

  it('should handle the fetchAttachmentsErr action correctly', () => {
    const expectedResult = state
      .set('fetching', false)
      .set('fetched', false)
      .set('fetchErr', true)
      .set('appLetters', [])
      .set('cvResumes', []);
    expect(profileAttachmentsReducer(state, fetchAttachmentsErr()))
      .toEqual(expectedResult);
  });

  it('should handle the removeAttachment action correctly', () => {
    const file = { uid: 1 };
    const stateWithFiles = state
      .set('appLetters', [file])
      .set('cvResumes', [file]);

    const expectedResult = state
      .set('fetching', false)
      .set('fetched', false)
      .set('fetchErr', true)
      .set('appLetters', [])
      .set('cvResumes', []);
    expect(profileAttachmentsReducer(stateWithFiles, removeAttachment(file)))
      .toEqual(expectedResult);
  });
});
