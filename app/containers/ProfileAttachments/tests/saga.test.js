/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { take, put, takeLatest, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { createMockTask } from 'redux-saga/utils';

import { attachmentsFetched, fetchAttachmentsErr } from '../actions';
import watchProfileAttachments, { fetchAttachments } from '../saga';
import { FETCH_ATTACHMENTS } from '../constants';

describe('fetchUserInfo Saga', () => {
  describe('Application Letter', () => {
    let fetchAttachmentsGenerator;
    let response;
    let user;

    beforeEach(() => {
      user = {
        app_letter: {
          id: 1,
          name: 'NAME',
          path: 'PATH',
        },
      };
      fetchAttachmentsGenerator = fetchAttachments({ user });
      response = { uri: 'URI HERE' };

      const callDescriptor = fetchAttachmentsGenerator.next(response).value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should dispatch the attachmentsFetched action if it requests the data successfully', () => {
      const { id, name } = user.app_letter;
      const fileList = [{
        uid: id,
        name,
        status: 'done',
        url: response.uri,
      }];
      const putDescriptor = fetchAttachmentsGenerator.next(response).value;
      expect(putDescriptor).toEqual(put(attachmentsFetched(fileList, [])));
    });

    describe('catch', () => {
      beforeEach(() => {
        const callDescriptor = fetchAttachmentsGenerator.throw().value;
        expect(callDescriptor).toMatchSnapshot();
      });

      it('should call the fetchUserInfoErr action if the response errors', () => {
        const putDescriptor = fetchAttachmentsGenerator.next().value;
        expect(putDescriptor).toEqual(put(fetchAttachmentsErr()));
      });
    });
  });

  describe('CV Reusme', () => {
    let fetchAttachmentsGenerator;
    let response;
    let user;

    beforeEach(() => {
      user = {
        cv: {
          id: 1,
          name: 'NAME',
          path: 'PATH',
        },
      };
      fetchAttachmentsGenerator = fetchAttachments({ user });
      response = { uri: 'URI HERE' };

      const callDescriptor = fetchAttachmentsGenerator.next(response).value;
      expect(callDescriptor).toMatchSnapshot();
    });

    it('should dispatch the attachmentsFetched action if it requests the data successfully', () => {
      const { id, name } = user.cv;
      const fileList = [{
        uid: id,
        name,
        status: 'done',
        url: response.uri,
      }];
      const putDescriptor = fetchAttachmentsGenerator.next(response).value;
      expect(putDescriptor).toEqual(put(attachmentsFetched([], fileList)));
    });
  });
});

describe('watchProfileAttachments Saga', () => {
  const generator = watchProfileAttachments();
  const mockTask = createMockTask();

  it('waits for start action', () => {
    const effect = generator.next().value;
    expect(effect).toEqual(takeLatest(FETCH_ATTACHMENTS, fetchAttachments));
  });

  it('take LOCATION_CHANGE', () => {
    const expectedYield = take(LOCATION_CHANGE);
    expect(generator.next(mockTask).value).toEqual(expectedYield);
  });

  it('cancel generator', () => {
    const expectedCancelYield = cancel(mockTask);
    expect(generator.next().value).toEqual(expectedCancelYield);
  });
});
