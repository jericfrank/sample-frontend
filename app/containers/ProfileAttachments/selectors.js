import { createSelector } from 'reselect';

/**
 * Direct selector to the profileAttachments state domain
 */
const selectProfileAttachmentsDomain = (state) => state.get('profileAttachments');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ProfileAttachments
 */

const makeSelectProfileAttachments = () => createSelector(
  selectProfileAttachmentsDomain,
  (substate) => substate.toJS()
);

export default makeSelectProfileAttachments;
export {
  selectProfileAttachmentsDomain,
};
