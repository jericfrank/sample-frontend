import { take, call, cancel, put, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { postRequest } from 'utils/request';
import { message } from 'antd';

import { attachmentsFetched, fetchAttachmentsErr } from './actions';
import { FETCH_ATTACHMENTS } from './constants';

export function* fetchAttachments({ user }) {
  try {
    let appLetters = [];
    let cvResumes = [];

    if (user.app_letter) {
      const { path, id, name } = user.app_letter;
      const { uri } = yield call(postRequest, '/applicants/file', { path });
      appLetters = [{
        uid: id,
        name,
        status: 'done',
        url: uri,
      }];
    }

    if (user.cv) {
      const { path, id, name } = user.cv;
      const { uri } = yield call(postRequest, '/applicants/file', { path });
      cvResumes = [{
        uid: id,
        name,
        status: 'done',
        url: uri,
      }];
    }
    yield put(attachmentsFetched(appLetters, cvResumes));
  } catch (err) {
    yield call(message.error, 'Something went wrong when fetching genders. Please try again.');
    yield put(fetchAttachmentsErr());
  }
}

// Individual exports for testing
export default function* watchProfileAttachments() {
  const watchers = yield takeLatest(FETCH_ATTACHMENTS, fetchAttachments);

  yield take(LOCATION_CHANGE);
  yield cancel(watchers);
}
