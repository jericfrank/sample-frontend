/*
 *
 * ProfileAttachments reducer
 *
 */

import { fromJS } from 'immutable';
import {
  FETCH_ATTACHMENTS,
  ATTACHMENTS_FETCHED,
  REMOVE_ATTACHMENT,
  FETCH_ATTACHMENTS_ERR,
} from './constants';

const initialState = fromJS({
  fetching: false,
  fetched: false,
  fetchErr: false,
  appLetters: [],
  cvResumes: [],
});

function profileAttachmentsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_ATTACHMENTS:
      return state
        .set('fetching', true)
        .set('fetched', false)
        .set('fetchErr', false)
        .set('appLetters', [])
        .set('cvResumes', []);

    case ATTACHMENTS_FETCHED:
      return state
        .set('fetching', false)
        .set('fetched', true)
        .set('fetchErr', false)
        .set('appLetters', action.appLetters)
        .set('cvResumes', action.cvResumes);

    case FETCH_ATTACHMENTS_ERR:
      return state
        .set('fetching', false)
        .set('fetched', false)
        .set('fetchErr', true)
        .set('appLetters', [])
        .set('cvResumes', []);

    case REMOVE_ATTACHMENT:
      return state
        .set('fetching', false)
        .set('fetched', false)
        .set('fetchErr', true)
        .set('appLetters', state.get('appLetters')
          .filter((file) => file.uid !== action.file.uid))
        .set('cvResumes', state.get('cvResumes')
          .filter((file) => file.uid !== action.file.uid));

    default:
      return state;
  }
}

export default profileAttachmentsReducer;
