/*
 *
 * ProfileAttachments constants
 *
 */

export const FETCH_ATTACHMENTS = 'app/ProfileAttachments/FETCH_ATTACHMENTS';
export const ATTACHMENTS_FETCHED = 'app/ProfileAttachments/ATTACHMENTS_FETCHED';
export const FETCH_ATTACHMENTS_ERR = 'app/ProfileAttachments/FETCH_ATTACHMENTS_ERR';

export const REMOVE_ATTACHMENT = 'app/ProfileAttachments/REMOVE_ATTACHMENT';
